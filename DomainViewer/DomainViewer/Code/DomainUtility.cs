﻿using Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Globalization;

namespace DomainViewer.Code
{
    public class DomainUtility
    {
        public static Dictionary<string, string> DeserializeResponseRaw(List<string> parms, string textResponse, string[] charRemove = null)
        {
            try
            {
                if (String.IsNullOrEmpty(textResponse))
                    return null;

                if (charRemove != null)
                {
                    foreach (var i in charRemove)
                    {
                        textResponse = textResponse.Replace(i, String.Empty);
                    }
                }

                bool getByNextParam = false;
                Dictionary<string, string> dicParms = new Dictionary<string, string>();

                if (parms.Count <= 0)
                    return null;

                string after = parms[0];
                //parms.RemoveAt(0);

                if (after.Equals("NextParam"))
                    getByNextParam = true;

                string before = "";
                string value = "";
                string valueTmp = "";
                for (int i = 1; i < parms.Count; i++)
                {
					try
					{
						before = parms[i];

						if (i == (parms.Count - 1))
						{
							value = textResponse.Substring(textResponse.IndexOf(before) + before.Length);
							value = value.Replace("\r", "");
							value = value.Replace("\n", "");
							value = value.Replace("\t", "");
							dicParms.Add(before, value.Trim());
							break;
						}

						if (getByNextParam)
							after = parms[i + 1];

						valueTmp = textResponse.Substring(textResponse.IndexOf(before) + before.Length);
						value = valueTmp.Substring(0, valueTmp.IndexOf(after)).Trim();
						dicParms.Add(before, value);
					}
					catch
					{
						before = parms[i];
						after = parms[i + 1];
						dicParms.Add(before, "");
					}
                }

                return dicParms;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static string RemoveHtmlTags(string textResponse)
        {
            try
            {
                return Regex.Replace(textResponse, "<.*?>", String.Empty);
            }
            catch(Exception)
            {
                return textResponse;
            }
        }

        public static DomainObjectBase DeserializeResponse(Dictionary<string, string> dicParamsDomain, List<string> listParamsData)
        {
            try
            {
                if (dicParamsDomain == null || dicParamsDomain.Count <= 0)
                    return null;

                DomainObjectBase item = new DomainObjectBase();

                for (int i = 0; i < dicParamsDomain.Count; i++)
                {
                    var property = item.GetType().GetProperty(listParamsData[i]);
                    if (property == null)
                        continue;

                    var typeMember = property.PropertyType;
                    var typeConverter = TypeDescriptor.GetConverter(typeMember);
                    object value = null;

					//if (typeMember.Equals(typeof(DateTime)))
					//{
					//	dicParamsDomain[dicParamsDomain.ElementAt(i).Key] = dicParamsDomain.ElementAt(i).Value.Trim();
					//	dicParamsDomain[dicParamsDomain.ElementAt(i).Key] = dicParamsDomain.ElementAt(i).Value.ToString().Substring(0, dicParamsDomain.ElementAt(i).Value.IndexOf(" "));
					//}

                    value = typeConverter.ConvertFromString(null, CultureInfo.GetCultureInfoByIetfLanguageTag("vi"), dicParamsDomain.ElementAt(i).Value);
					if (typeMember.Equals(typeof(DateTime)))
					{
						if(string.IsNullOrEmpty(value.ToString()))
						{
							property.SetValue(item, null, null);
							continue;
						}
					}
                    property.SetValue(item, value, null);
                }

                item.ShortExpireDate = int.Parse(((DateTime)item.ExpireDate).ToString("yyyyMMdd"));
                return item;
            }
            catch (Exception)
            {
                return null;
            }
        }

        //Bỏ phần SubDomain
        public static string RemoveSubDomain(string domain, string[] ext2Allow = null)
        {
            try
            {
                var uriTmp = new UriBuilder(domain);
                domain = uriTmp.Host;

                string extTmp = domain.Substring(domain.LastIndexOf("."));
                if (domain.Count(p => p.Equals('.')) > 1)
                {
                    if (ext2Allow == null)
                        return String.Empty;
                    else
                    {
                        foreach (var i in ext2Allow)
                        {
                            if (domain.EndsWith(i))
                            {
                                extTmp = i;
                                break;
                            }
                        }
                    }
                }

                domain = domain.Remove(domain.IndexOf(extTmp));

                if (domain.Contains("."))
                    domain = domain.Substring(domain.LastIndexOf(".") + 1) + extTmp;
                else
                    domain = domain + extTmp;

                return domain;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }
    }
}
