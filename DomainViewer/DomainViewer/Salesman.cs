//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainViewer
{
    using System;
    using System.Collections.Generic;
    
    public partial class Salesman
    {
        public int SaleId { get; set; }
        public string SaleName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public Nullable<short> AccessType { get; set; }
        public Nullable<short> Status { get; set; }
    }
}
