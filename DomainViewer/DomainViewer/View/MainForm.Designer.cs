﻿namespace DomainViewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.importBtn = new System.Windows.Forms.Button();
			this.import = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.btnLogOut = new System.Windows.Forms.Button();
			this.lblAccessType = new System.Windows.Forms.Label();
			this.btnChangePassword = new System.Windows.Forms.Button();
			this.lblAccoutName = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.resultLbl = new System.Windows.Forms.Label();
			this.divineTask = new System.Windows.Forms.Button();
			this.numbarCheckLb = new System.Windows.Forms.Label();
			this.addSaleBtn = new System.Windows.Forms.Button();
			this.salesmanClb = new System.Windows.Forms.CheckedListBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.statusImportLbl = new System.Windows.Forms.Label();
			this.urlFileImortLbl = new System.Windows.Forms.Label();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.lblCountRecord = new System.Windows.Forms.Label();
			this.todateLbl = new System.Windows.Forms.Label();
			this.fromdateLbl = new System.Windows.Forms.Label();
			this.searchValueBtn = new System.Windows.Forms.Button();
			this.dateToDtb = new System.Windows.Forms.DateTimePicker();
			this.dateFromDtb = new System.Windows.Forms.DateTimePicker();
			this.valueSearchByDateCbb = new System.Windows.Forms.ComboBox();
			this.searchTextTxt = new System.Windows.Forms.TextBox();
			this.valueSearchByTextCbb = new System.Windows.Forms.ComboBox();
			this.dgvDomain = new System.Windows.Forms.DataGridView();
			this.STT = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tpageBookDomain = new System.Windows.Forms.TabPage();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.chkUseCaptureDay = new System.Windows.Forms.CheckBox();
			this.chkUseExpireDay = new System.Windows.Forms.CheckBox();
			this.btnDomainBookedSearchAll = new System.Windows.Forms.Button();
			this.btn_DomainBooked_Search = new System.Windows.Forms.Button();
			this.dtpSearchDomainBookedCaptureDay = new System.Windows.Forms.DateTimePicker();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.txt_SearchDomainBookedName = new System.Windows.Forms.TextBox();
			this.txtSearchDomainBookedInfo = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtSearchDomainBookedPrice = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.dtpSearchDomanBookedExpireDay = new System.Windows.Forms.DateTimePicker();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.btnUpdateDomainBooked = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.rtxt_DomainBooked_CustomerInfo = new System.Windows.Forms.RichTextBox();
			this.txt_DomainBooked_DepositPrice = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txt_DomainBooked_DomainName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btn_DomainBooked_Add = new System.Windows.Forms.Button();
			this.dgvDomainBooked = new System.Windows.Forms.DataGridView();
			this.cmsGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.miShowWhois = new System.Windows.Forms.ToolStripMenuItem();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.label10 = new System.Windows.Forms.Label();
			this.dtp_DomainBooked_CaptureDate = new System.Windows.Forms.DateTimePicker();
			this.chkInput_CaptureDate = new System.Windows.Forms.CheckBox();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvDomain)).BeginInit();
			this.tpageBookDomain.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.groupBox5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvDomainBooked)).BeginInit();
			this.cmsGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
			// 
			// importBtn
			// 
			this.importBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.importBtn.Location = new System.Drawing.Point(877, 23);
			this.importBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.importBtn.Name = "importBtn";
			this.importBtn.Size = new System.Drawing.Size(87, 28);
			this.importBtn.TabIndex = 2;
			this.importBtn.Text = "Import";
			this.importBtn.UseVisualStyleBackColor = true;
			this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
			// 
			// import
			// 
			this.import.Location = new System.Drawing.Point(9, 18);
			this.import.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.import.Name = "import";
			this.import.Size = new System.Drawing.Size(87, 28);
			this.import.TabIndex = 3;
			this.import.Text = "Chọn File";
			this.import.UseVisualStyleBackColor = true;
			this.import.Click += new System.EventHandler(this.import_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tpageBookDomain);
			this.tabControl1.Location = new System.Drawing.Point(0, -1);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1038, 591);
			this.tabControl1.TabIndex = 4;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.groupBox4);
			this.tabPage1.Controls.Add(this.groupBox2);
			this.tabPage1.Controls.Add(this.groupBox1);
			this.tabPage1.Location = new System.Drawing.Point(4, 25);
			this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tabPage1.Size = new System.Drawing.Size(1030, 562);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Tag = "";
			this.tabPage1.Text = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.btnLogOut);
			this.groupBox4.Controls.Add(this.lblAccessType);
			this.groupBox4.Controls.Add(this.btnChangePassword);
			this.groupBox4.Controls.Add(this.lblAccoutName);
			this.groupBox4.Controls.Add(this.label1);
			this.groupBox4.Location = new System.Drawing.Point(9, 7);
			this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Padding = new System.Windows.Forms.Padding(5);
			this.groupBox4.Size = new System.Drawing.Size(277, 91);
			this.groupBox4.TabIndex = 9;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Tài khoản";
			// 
			// btnLogOut
			// 
			this.btnLogOut.Location = new System.Drawing.Point(137, 55);
			this.btnLogOut.Name = "btnLogOut";
			this.btnLogOut.Size = new System.Drawing.Size(88, 28);
			this.btnLogOut.TabIndex = 3;
			this.btnLogOut.Text = "Đăng Xuất";
			this.btnLogOut.UseVisualStyleBackColor = true;
			this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
			// 
			// lblAccessType
			// 
			this.lblAccessType.AutoSize = true;
			this.lblAccessType.Font = new System.Drawing.Font("Tahoma", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(163)));
			this.lblAccessType.Location = new System.Drawing.Point(213, 27);
			this.lblAccessType.Name = "lblAccessType";
			this.lblAccessType.Size = new System.Drawing.Size(35, 16);
			this.lblAccessType.TabIndex = 2;
			this.lblAccessType.Text = "Sale";
			// 
			// btnChangePassword
			// 
			this.btnChangePassword.Location = new System.Drawing.Point(9, 55);
			this.btnChangePassword.Name = "btnChangePassword";
			this.btnChangePassword.Size = new System.Drawing.Size(109, 28);
			this.btnChangePassword.TabIndex = 1;
			this.btnChangePassword.Text = "Đổi mật khẩu";
			this.btnChangePassword.UseVisualStyleBackColor = true;
			this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
			// 
			// lblAccoutName
			// 
			this.lblAccoutName.AutoSize = true;
			this.lblAccoutName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
			this.lblAccoutName.Location = new System.Drawing.Point(43, 27);
			this.lblAccoutName.Name = "lblAccoutName";
			this.lblAccoutName.Size = new System.Drawing.Size(25, 16);
			this.lblAccoutName.TabIndex = 0;
			this.lblAccoutName.Text = "Du";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(18, 27);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(23, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Hi,";
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.groupBox3);
			this.groupBox2.Controls.Add(this.addSaleBtn);
			this.groupBox2.Controls.Add(this.salesmanClb);
			this.groupBox2.Location = new System.Drawing.Point(9, 300);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox2.Size = new System.Drawing.Size(1013, 254);
			this.groupBox2.TabIndex = 8;
			this.groupBox2.TabStop = false;
			this.groupBox2.Tag = "1";
			this.groupBox2.Text = "Thêm sale và Chia tasks";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.resultLbl);
			this.groupBox3.Controls.Add(this.divineTask);
			this.groupBox3.Controls.Add(this.numbarCheckLb);
			this.groupBox3.Location = new System.Drawing.Point(578, 46);
			this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox3.Size = new System.Drawing.Size(428, 148);
			this.groupBox3.TabIndex = 4;
			this.groupBox3.TabStop = false;
			this.groupBox3.Tag = "";
			this.groupBox3.Text = "Chia tasks";
			// 
			// resultLbl
			// 
			this.resultLbl.AutoSize = true;
			this.resultLbl.Location = new System.Drawing.Point(7, 116);
			this.resultLbl.Name = "resultLbl";
			this.resultLbl.Size = new System.Drawing.Size(0, 16);
			this.resultLbl.TabIndex = 5;
			// 
			// divineTask
			// 
			this.divineTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.divineTask.Location = new System.Drawing.Point(334, 113);
			this.divineTask.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.divineTask.Name = "divineTask";
			this.divineTask.Size = new System.Drawing.Size(87, 28);
			this.divineTask.TabIndex = 4;
			this.divineTask.Text = "Chia tasks";
			this.divineTask.UseVisualStyleBackColor = true;
			this.divineTask.Click += new System.EventHandler(this.divineTask_Click);
			// 
			// numbarCheckLb
			// 
			this.numbarCheckLb.AutoSize = true;
			this.numbarCheckLb.Location = new System.Drawing.Point(3, 20);
			this.numbarCheckLb.Name = "numbarCheckLb";
			this.numbarCheckLb.Size = new System.Drawing.Size(0, 16);
			this.numbarCheckLb.TabIndex = 0;
			// 
			// addSaleBtn
			// 
			this.addSaleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.addSaleBtn.Location = new System.Drawing.Point(9, 209);
			this.addSaleBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.addSaleBtn.Name = "addSaleBtn";
			this.addSaleBtn.Size = new System.Drawing.Size(150, 28);
			this.addSaleBtn.TabIndex = 3;
			this.addSaleBtn.Text = "Quản lý tài khoản";
			this.addSaleBtn.UseVisualStyleBackColor = true;
			this.addSaleBtn.Click += new System.EventHandler(this.addSaleBtn_Click);
			// 
			// salesmanClb
			// 
			this.salesmanClb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.salesmanClb.FormattingEnabled = true;
			this.salesmanClb.Location = new System.Drawing.Point(9, 46);
			this.salesmanClb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.salesmanClb.Name = "salesmanClb";
			this.salesmanClb.Size = new System.Drawing.Size(252, 130);
			this.salesmanClb.TabIndex = 0;
			this.salesmanClb.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.salesmanClb_ItemCheck);
			this.salesmanClb.SelectedIndexChanged += new System.EventHandler(this.salesmanClb_SelectedIndexChanged);
			this.salesmanClb.SelectedValueChanged += new System.EventHandler(this.salesmanClb_SelectedValueChanged);
			this.salesmanClb.EnabledChanged += new System.EventHandler(this.salesmanClb_EnabledChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.importBtn);
			this.groupBox1.Controls.Add(this.statusImportLbl);
			this.groupBox1.Controls.Add(this.import);
			this.groupBox1.Controls.Add(this.urlFileImortLbl);
			this.groupBox1.Location = new System.Drawing.Point(9, 106);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox1.Size = new System.Drawing.Size(1013, 186);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Tag = "1";
			this.groupBox1.Text = "Import excel";
			// 
			// statusImportLbl
			// 
			this.statusImportLbl.AutoSize = true;
			this.statusImportLbl.Location = new System.Drawing.Point(6, 65);
			this.statusImportLbl.Name = "statusImportLbl";
			this.statusImportLbl.Size = new System.Drawing.Size(0, 16);
			this.statusImportLbl.TabIndex = 6;
			// 
			// urlFileImortLbl
			// 
			this.urlFileImortLbl.AutoSize = true;
			this.urlFileImortLbl.Location = new System.Drawing.Point(118, 25);
			this.urlFileImortLbl.Name = "urlFileImortLbl";
			this.urlFileImortLbl.Size = new System.Drawing.Size(0, 16);
			this.urlFileImortLbl.TabIndex = 4;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.lblCountRecord);
			this.tabPage2.Controls.Add(this.todateLbl);
			this.tabPage2.Controls.Add(this.fromdateLbl);
			this.tabPage2.Controls.Add(this.searchValueBtn);
			this.tabPage2.Controls.Add(this.dateToDtb);
			this.tabPage2.Controls.Add(this.dateFromDtb);
			this.tabPage2.Controls.Add(this.valueSearchByDateCbb);
			this.tabPage2.Controls.Add(this.searchTextTxt);
			this.tabPage2.Controls.Add(this.valueSearchByTextCbb);
			this.tabPage2.Controls.Add(this.dgvDomain);
			this.tabPage2.Location = new System.Drawing.Point(4, 25);
			this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tabPage2.Size = new System.Drawing.Size(1030, 562);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// lblCountRecord
			// 
			this.lblCountRecord.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblCountRecord.AutoSize = true;
			this.lblCountRecord.Location = new System.Drawing.Point(601, 75);
			this.lblCountRecord.Name = "lblCountRecord";
			this.lblCountRecord.Size = new System.Drawing.Size(0, 16);
			this.lblCountRecord.TabIndex = 12;
			this.lblCountRecord.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// todateLbl
			// 
			this.todateLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.todateLbl.AutoSize = true;
			this.todateLbl.Location = new System.Drawing.Point(420, 30);
			this.todateLbl.Name = "todateLbl";
			this.todateLbl.Size = new System.Drawing.Size(62, 16);
			this.todateLbl.TabIndex = 11;
			this.todateLbl.Text = "Đến ngày";
			// 
			// fromdateLbl
			// 
			this.fromdateLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fromdateLbl.AutoSize = true;
			this.fromdateLbl.Location = new System.Drawing.Point(178, 30);
			this.fromdateLbl.Name = "fromdateLbl";
			this.fromdateLbl.Size = new System.Drawing.Size(62, 16);
			this.fromdateLbl.TabIndex = 10;
			this.fromdateLbl.Text = "Đến ngày";
			// 
			// searchValueBtn
			// 
			this.searchValueBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.searchValueBtn.Location = new System.Drawing.Point(487, 69);
			this.searchValueBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.searchValueBtn.Name = "searchValueBtn";
			this.searchValueBtn.Size = new System.Drawing.Size(87, 28);
			this.searchValueBtn.TabIndex = 8;
			this.searchValueBtn.Text = "Tìm Kiếm";
			this.searchValueBtn.UseVisualStyleBackColor = true;
			this.searchValueBtn.Click += new System.EventHandler(this.searchValueBtn_Click);
			// 
			// dateToDtb
			// 
			this.dateToDtb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dateToDtb.CustomFormat = " dd/MM/yyyy";
			this.dateToDtb.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateToDtb.Location = new System.Drawing.Point(487, 27);
			this.dateToDtb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.dateToDtb.Name = "dateToDtb";
			this.dateToDtb.Size = new System.Drawing.Size(233, 23);
			this.dateToDtb.TabIndex = 7;
			// 
			// dateFromDtb
			// 
			this.dateFromDtb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dateFromDtb.CustomFormat = " dd/MM/yyyy";
			this.dateFromDtb.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateFromDtb.Location = new System.Drawing.Point(247, 27);
			this.dateFromDtb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.dateFromDtb.Name = "dateFromDtb";
			this.dateFromDtb.Size = new System.Drawing.Size(165, 23);
			this.dateFromDtb.TabIndex = 6;
			// 
			// valueSearchByDateCbb
			// 
			this.valueSearchByDateCbb.FormattingEnabled = true;
			this.valueSearchByDateCbb.Location = new System.Drawing.Point(30, 26);
			this.valueSearchByDateCbb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.valueSearchByDateCbb.Name = "valueSearchByDateCbb";
			this.valueSearchByDateCbb.Size = new System.Drawing.Size(140, 24);
			this.valueSearchByDateCbb.TabIndex = 5;
			// 
			// searchTextTxt
			// 
			this.searchTextTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.searchTextTxt.Location = new System.Drawing.Point(247, 69);
			this.searchTextTxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.searchTextTxt.Name = "searchTextTxt";
			this.searchTextTxt.Size = new System.Drawing.Size(165, 23);
			this.searchTextTxt.TabIndex = 4;
			// 
			// valueSearchByTextCbb
			// 
			this.valueSearchByTextCbb.FormattingEnabled = true;
			this.valueSearchByTextCbb.Location = new System.Drawing.Point(30, 68);
			this.valueSearchByTextCbb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.valueSearchByTextCbb.Name = "valueSearchByTextCbb";
			this.valueSearchByTextCbb.Size = new System.Drawing.Size(140, 24);
			this.valueSearchByTextCbb.TabIndex = 3;
			this.valueSearchByTextCbb.SelectedIndexChanged += new System.EventHandler(this.valueSearchByTextCbb_SelectedIndexChanged);
			// 
			// dgvDomain
			// 
			this.dgvDomain.AllowUserToAddRows = false;
			this.dgvDomain.AllowUserToDeleteRows = false;
			this.dgvDomain.AllowUserToOrderColumns = true;
			this.dgvDomain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dgvDomain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvDomain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT});
			this.dgvDomain.Location = new System.Drawing.Point(8, 119);
			this.dgvDomain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.dgvDomain.Name = "dgvDomain";
			this.dgvDomain.ReadOnly = true;
			this.dgvDomain.Size = new System.Drawing.Size(1014, 435);
			this.dgvDomain.TabIndex = 2;
			this.dgvDomain.VirtualMode = true;
			this.dgvDomain.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDomain_CellContentClick);
			this.dgvDomain.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDomain_CellDoubleClick);
			this.dgvDomain.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDomain_CellMouseClick);
			this.dgvDomain.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dgvDomain_CellValueNeeded);
			this.dgvDomain.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDomain_ColumnHeaderMouseClick);
			this.dgvDomain.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvDomain_RowHeaderMouseClick);
			// 
			// STT
			// 
			this.STT.DataPropertyName = "STT";
			this.STT.HeaderText = "STT";
			this.STT.Name = "STT";
			this.STT.ReadOnly = true;
			// 
			// tpageBookDomain
			// 
			this.tpageBookDomain.Controls.Add(this.groupBox6);
			this.tpageBookDomain.Controls.Add(this.groupBox5);
			this.tpageBookDomain.Controls.Add(this.dgvDomainBooked);
			this.tpageBookDomain.Location = new System.Drawing.Point(4, 25);
			this.tpageBookDomain.Name = "tpageBookDomain";
			this.tpageBookDomain.Padding = new System.Windows.Forms.Padding(3);
			this.tpageBookDomain.Size = new System.Drawing.Size(1030, 562);
			this.tpageBookDomain.TabIndex = 2;
			this.tpageBookDomain.Text = "Domain đặt trước";
			this.tpageBookDomain.UseVisualStyleBackColor = true;
			// 
			// groupBox6
			// 
			this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox6.Controls.Add(this.chkUseCaptureDay);
			this.groupBox6.Controls.Add(this.chkUseExpireDay);
			this.groupBox6.Controls.Add(this.btnDomainBookedSearchAll);
			this.groupBox6.Controls.Add(this.btn_DomainBooked_Search);
			this.groupBox6.Controls.Add(this.dtpSearchDomainBookedCaptureDay);
			this.groupBox6.Controls.Add(this.label9);
			this.groupBox6.Controls.Add(this.label8);
			this.groupBox6.Controls.Add(this.txt_SearchDomainBookedName);
			this.groupBox6.Controls.Add(this.txtSearchDomainBookedInfo);
			this.groupBox6.Controls.Add(this.label7);
			this.groupBox6.Controls.Add(this.txtSearchDomainBookedPrice);
			this.groupBox6.Controls.Add(this.label6);
			this.groupBox6.Controls.Add(this.label5);
			this.groupBox6.Controls.Add(this.dtpSearchDomanBookedExpireDay);
			this.groupBox6.Location = new System.Drawing.Point(631, 6);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(396, 206);
			this.groupBox6.TabIndex = 7;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Tìm kiếm";
			// 
			// chkUseCaptureDay
			// 
			this.chkUseCaptureDay.AutoSize = true;
			this.chkUseCaptureDay.Checked = true;
			this.chkUseCaptureDay.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkUseCaptureDay.Location = new System.Drawing.Point(323, 154);
			this.chkUseCaptureDay.Name = "chkUseCaptureDay";
			this.chkUseCaptureDay.Size = new System.Drawing.Size(48, 20);
			this.chkUseCaptureDay.TabIndex = 21;
			this.chkUseCaptureDay.Text = "Use";
			this.chkUseCaptureDay.UseVisualStyleBackColor = true;
			this.chkUseCaptureDay.CheckedChanged += new System.EventHandler(this.chkUseCaptureDay_CheckedChanged);
			// 
			// chkUseExpireDay
			// 
			this.chkUseExpireDay.AutoSize = true;
			this.chkUseExpireDay.Checked = true;
			this.chkUseExpireDay.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkUseExpireDay.Location = new System.Drawing.Point(324, 121);
			this.chkUseExpireDay.Name = "chkUseExpireDay";
			this.chkUseExpireDay.Size = new System.Drawing.Size(48, 20);
			this.chkUseExpireDay.TabIndex = 20;
			this.chkUseExpireDay.Text = "Use";
			this.chkUseExpireDay.UseVisualStyleBackColor = true;
			this.chkUseExpireDay.CheckedChanged += new System.EventHandler(this.chkUseExpireDay_CheckedChanged);
			// 
			// btnDomainBookedSearchAll
			// 
			this.btnDomainBookedSearchAll.Location = new System.Drawing.Point(9, 180);
			this.btnDomainBookedSearchAll.Name = "btnDomainBookedSearchAll";
			this.btnDomainBookedSearchAll.Size = new System.Drawing.Size(75, 23);
			this.btnDomainBookedSearchAll.TabIndex = 19;
			this.btnDomainBookedSearchAll.Text = "Tìm tất cả";
			this.btnDomainBookedSearchAll.UseVisualStyleBackColor = true;
			this.btnDomainBookedSearchAll.Click += new System.EventHandler(this.btnDomainBookedSearchAll_Click);
			// 
			// btn_DomainBooked_Search
			// 
			this.btn_DomainBooked_Search.Location = new System.Drawing.Point(117, 180);
			this.btn_DomainBooked_Search.Name = "btn_DomainBooked_Search";
			this.btn_DomainBooked_Search.Size = new System.Drawing.Size(75, 23);
			this.btn_DomainBooked_Search.TabIndex = 11;
			this.btn_DomainBooked_Search.Text = "Tìm kiếm";
			this.btn_DomainBooked_Search.UseVisualStyleBackColor = true;
			this.btn_DomainBooked_Search.Click += new System.EventHandler(this.btn_DomainBooked_Search_Click);
			// 
			// dtpSearchDomainBookedCaptureDay
			// 
			this.dtpSearchDomainBookedCaptureDay.CustomFormat = "dd-MM-yyyy";
			this.dtpSearchDomainBookedCaptureDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpSearchDomainBookedCaptureDay.Location = new System.Drawing.Point(117, 151);
			this.dtpSearchDomainBookedCaptureDay.Name = "dtpSearchDomainBookedCaptureDay";
			this.dtpSearchDomainBookedCaptureDay.Size = new System.Drawing.Size(200, 23);
			this.dtpSearchDomainBookedCaptureDay.TabIndex = 18;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(6, 154);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(83, 16);
			this.label9.TabIndex = 17;
			this.label9.Text = "Capture Day:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 123);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(88, 16);
			this.label8.TabIndex = 16;
			this.label8.Text = "Ngày hết hạn:";
			// 
			// txt_SearchDomainBookedName
			// 
			this.txt_SearchDomainBookedName.Location = new System.Drawing.Point(117, 22);
			this.txt_SearchDomainBookedName.Name = "txt_SearchDomainBookedName";
			this.txt_SearchDomainBookedName.Size = new System.Drawing.Size(273, 23);
			this.txt_SearchDomainBookedName.TabIndex = 15;
			// 
			// txtSearchDomainBookedInfo
			// 
			this.txtSearchDomainBookedInfo.Location = new System.Drawing.Point(117, 86);
			this.txtSearchDomainBookedInfo.Name = "txtSearchDomainBookedInfo";
			this.txtSearchDomainBookedInfo.Size = new System.Drawing.Size(273, 23);
			this.txtSearchDomainBookedInfo.TabIndex = 14;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 89);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(67, 16);
			this.label7.TabIndex = 11;
			this.label7.Text = "Thông tin:";
			// 
			// txtSearchDomainBookedPrice
			// 
			this.txtSearchDomainBookedPrice.Location = new System.Drawing.Point(117, 54);
			this.txtSearchDomainBookedPrice.Name = "txtSearchDomainBookedPrice";
			this.txtSearchDomainBookedPrice.Size = new System.Drawing.Size(134, 23);
			this.txtSearchDomainBookedPrice.TabIndex = 12;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 57);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(98, 16);
			this.label6.TabIndex = 11;
			this.label6.Text = "Số tiền đặt cọc:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 25);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(93, 16);
			this.label5.TabIndex = 11;
			this.label5.Text = "Domain Name:";
			// 
			// dtpSearchDomanBookedExpireDay
			// 
			this.dtpSearchDomanBookedExpireDay.CustomFormat = "dd-MM-yyyy";
			this.dtpSearchDomanBookedExpireDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtpSearchDomanBookedExpireDay.Location = new System.Drawing.Point(117, 120);
			this.dtpSearchDomanBookedExpireDay.Name = "dtpSearchDomanBookedExpireDay";
			this.dtpSearchDomanBookedExpireDay.Size = new System.Drawing.Size(200, 23);
			this.dtpSearchDomanBookedExpireDay.TabIndex = 2;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.chkInput_CaptureDate);
			this.groupBox5.Controls.Add(this.label10);
			this.groupBox5.Controls.Add(this.dtp_DomainBooked_CaptureDate);
			this.groupBox5.Controls.Add(this.btnUpdateDomainBooked);
			this.groupBox5.Controls.Add(this.label4);
			this.groupBox5.Controls.Add(this.rtxt_DomainBooked_CustomerInfo);
			this.groupBox5.Controls.Add(this.txt_DomainBooked_DepositPrice);
			this.groupBox5.Controls.Add(this.label3);
			this.groupBox5.Controls.Add(this.txt_DomainBooked_DomainName);
			this.groupBox5.Controls.Add(this.label2);
			this.groupBox5.Controls.Add(this.btn_DomainBooked_Add);
			this.groupBox5.Location = new System.Drawing.Point(3, 6);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(622, 206);
			this.groupBox5.TabIndex = 5;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Đặt trước domain";
			// 
			// btnUpdateDomainBooked
			// 
			this.btnUpdateDomainBooked.Location = new System.Drawing.Point(532, 180);
			this.btnUpdateDomainBooked.Name = "btnUpdateDomainBooked";
			this.btnUpdateDomainBooked.Size = new System.Drawing.Size(75, 23);
			this.btnUpdateDomainBooked.TabIndex = 11;
			this.btnUpdateDomainBooked.Text = "Update";
			this.btnUpdateDomainBooked.UseVisualStyleBackColor = true;
			this.btnUpdateDomainBooked.Click += new System.EventHandler(this.btnUpdateDomainBooked_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 54);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(67, 16);
			this.label4.TabIndex = 10;
			this.label4.Text = "Thông tin:";
			// 
			// rtxt_DomainBooked_CustomerInfo
			// 
			this.rtxt_DomainBooked_CustomerInfo.Location = new System.Drawing.Point(105, 51);
			this.rtxt_DomainBooked_CustomerInfo.Name = "rtxt_DomainBooked_CustomerInfo";
			this.rtxt_DomainBooked_CustomerInfo.Size = new System.Drawing.Size(502, 90);
			this.rtxt_DomainBooked_CustomerInfo.TabIndex = 9;
			this.rtxt_DomainBooked_CustomerInfo.Text = "";
			// 
			// txt_DomainBooked_DepositPrice
			// 
			this.txt_DomainBooked_DepositPrice.Location = new System.Drawing.Point(482, 22);
			this.txt_DomainBooked_DepositPrice.Name = "txt_DomainBooked_DepositPrice";
			this.txt_DomainBooked_DepositPrice.Size = new System.Drawing.Size(125, 23);
			this.txt_DomainBooked_DepositPrice.TabIndex = 8;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(375, 25);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(98, 16);
			this.label3.TabIndex = 7;
			this.label3.Text = "Số tiền đặt cọc:";
			// 
			// txt_DomainBooked_DomainName
			// 
			this.txt_DomainBooked_DomainName.Location = new System.Drawing.Point(105, 22);
			this.txt_DomainBooked_DomainName.Name = "txt_DomainBooked_DomainName";
			this.txt_DomainBooked_DomainName.Size = new System.Drawing.Size(227, 23);
			this.txt_DomainBooked_DomainName.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 25);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "Domain Name:";
			// 
			// btn_DomainBooked_Add
			// 
			this.btn_DomainBooked_Add.Location = new System.Drawing.Point(105, 180);
			this.btn_DomainBooked_Add.Name = "btn_DomainBooked_Add";
			this.btn_DomainBooked_Add.Size = new System.Drawing.Size(75, 23);
			this.btn_DomainBooked_Add.TabIndex = 0;
			this.btn_DomainBooked_Add.Text = "Thêm";
			this.btn_DomainBooked_Add.UseVisualStyleBackColor = true;
			this.btn_DomainBooked_Add.Click += new System.EventHandler(this.btn_DomainBooked_Add_Click);
			// 
			// dgvDomainBooked
			// 
			this.dgvDomainBooked.AllowUserToAddRows = false;
			this.dgvDomainBooked.AllowUserToDeleteRows = false;
			this.dgvDomainBooked.AllowUserToOrderColumns = true;
			this.dgvDomainBooked.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dgvDomainBooked.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvDomainBooked.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.dgvDomainBooked.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDomainBooked.Location = new System.Drawing.Point(3, 218);
			this.dgvDomainBooked.Name = "dgvDomainBooked";
			this.dgvDomainBooked.ReadOnly = true;
			this.dgvDomainBooked.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvDomainBooked.Size = new System.Drawing.Size(1024, 341);
			this.dgvDomainBooked.TabIndex = 4;
			this.dgvDomainBooked.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDomainBooked_CellDoubleClick);
			// 
			// cmsGrid
			// 
			this.cmsGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miShowWhois});
			this.cmsGrid.Name = "cmsGrid";
			this.cmsGrid.Size = new System.Drawing.Size(153, 26);
			this.cmsGrid.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.cmsGrid_ItemClicked);
			// 
			// miShowWhois
			// 
			this.miShowWhois.Name = "miShowWhois";
			this.miShowWhois.Size = new System.Drawing.Size(152, 22);
			this.miShowWhois.Text = "Whois Domain";
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(6, 150);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(88, 16);
			this.label10.TabIndex = 18;
			this.label10.Text = "Capture Date:";
			// 
			// dtp_DomainBooked_CaptureDate
			// 
			this.dtp_DomainBooked_CaptureDate.CustomFormat = "dd-MM-yyyy HH:mm:ss";
			this.dtp_DomainBooked_CaptureDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtp_DomainBooked_CaptureDate.Location = new System.Drawing.Point(105, 147);
			this.dtp_DomainBooked_CaptureDate.Name = "dtp_DomainBooked_CaptureDate";
			this.dtp_DomainBooked_CaptureDate.Size = new System.Drawing.Size(161, 23);
			this.dtp_DomainBooked_CaptureDate.TabIndex = 17;
			// 
			// chkInput_CaptureDate
			// 
			this.chkInput_CaptureDate.AutoSize = true;
			this.chkInput_CaptureDate.Location = new System.Drawing.Point(284, 149);
			this.chkInput_CaptureDate.Name = "chkInput_CaptureDate";
			this.chkInput_CaptureDate.Size = new System.Drawing.Size(48, 20);
			this.chkInput_CaptureDate.TabIndex = 22;
			this.chkInput_CaptureDate.Text = "Use";
			this.chkInput_CaptureDate.UseVisualStyleBackColor = true;
			this.chkInput_CaptureDate.CheckedChanged += new System.EventHandler(this.chkInput_CaptureDate_CheckedChanged);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1038, 591);
			this.Controls.Add(this.tabControl1);
			this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.MinimumSize = new System.Drawing.Size(1054, 630);
			this.Name = "Form1";
			this.Text = "Soft DomainViewer";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvDomain)).EndInit();
			this.tpageBookDomain.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.groupBox6.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvDomainBooked)).EndInit();
			this.cmsGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button importBtn;
        private System.Windows.Forms.Button import;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label urlFileImortLbl;
        private System.Windows.Forms.Label statusImportLbl;
        private System.Windows.Forms.DataGridView dgvDomain;
        private System.Windows.Forms.DateTimePicker dateToDtb;
        private System.Windows.Forms.DateTimePicker dateFromDtb;
        private System.Windows.Forms.ComboBox valueSearchByDateCbb;
        private System.Windows.Forms.TextBox searchTextTxt;
        private System.Windows.Forms.ComboBox valueSearchByTextCbb;
        private System.Windows.Forms.Button searchValueBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button addSaleBtn;
        private System.Windows.Forms.CheckedListBox salesmanClb;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label numbarCheckLb;
        private System.Windows.Forms.Button divineTask;
        private System.Windows.Forms.Label resultLbl;
        private System.Windows.Forms.Label todateLbl;
        private System.Windows.Forms.Label fromdateLbl;
        private System.Windows.Forms.Label lblCountRecord;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblAccessType;
        public System.Windows.Forms.Label lblAccoutName;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.ContextMenuStrip cmsGrid;
        private System.Windows.Forms.ToolStripMenuItem miShowWhois;
		private System.Windows.Forms.TabPage tpageBookDomain;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Button btn_DomainBooked_Search;
		private System.Windows.Forms.DateTimePicker dtpSearchDomainBookedCaptureDay;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txt_SearchDomainBookedName;
		private System.Windows.Forms.TextBox txtSearchDomainBookedInfo;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtSearchDomainBookedPrice;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.DateTimePicker dtpSearchDomanBookedExpireDay;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.RichTextBox rtxt_DomainBooked_CustomerInfo;
		private System.Windows.Forms.TextBox txt_DomainBooked_DepositPrice;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txt_DomainBooked_DomainName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btn_DomainBooked_Add;
		private System.Windows.Forms.DataGridView dgvDomainBooked;
		private System.Windows.Forms.Button btnDomainBookedSearchAll;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.CheckBox chkUseCaptureDay;
		private System.Windows.Forms.CheckBox chkUseExpireDay;
		private System.Windows.Forms.Button btnUpdateDomainBooked;
		private System.Windows.Forms.CheckBox chkInput_CaptureDate;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.DateTimePicker dtp_DomainBooked_CaptureDate;
    }
}

