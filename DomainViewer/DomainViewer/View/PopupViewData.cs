﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DomainViewer.View
{
    public partial class PopupViewData : Form
    {
        object listData;

        public PopupViewData(object listData)
        {
            InitializeComponent();
            this.listData = listData;
            dgvViewData.DataSource = listData;
        }
    }
}
