﻿using DomainViewer.Controller;
using DomainViewer.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DomainViewer.View
{
    public partial class FrmLogin : Form
    {
        Form1 frmMain;

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if(String.IsNullOrEmpty(txtUserName.Text) || String.IsNullOrEmpty(txtPassowrd.Text))
                {
                    MessageBox.Show("Nhập đầy đủ thông tin");
                    return;
                }

                var checkLogin = Tool.checkLogin(txtUserName.Text, txtPassowrd.Text);
                if(checkLogin == null)
                {
                    MessageBox.Show("Sai thông tin đăng nhập");
                    return;
                }

                if (checkLogin.Status == null || checkLogin.Status.Value != (short)SaleStatus.Active)
                {
                    MessageBox.Show("Tài khoản chưa được kích hoạt");
                    return;
                }

                this.Hide();
                frmMain = new Form1(this);
                frmMain.accountLogin = checkLogin;
                frmMain.lblAccessType.Text = Enum.Parse(typeof(AccessType), checkLogin.AccessType.Value.ToString()).ToString();
                frmMain.lblAccoutName.Text = checkLogin.SaleName;
                frmMain.dgvDomainData();

                if (!frmMain.LoadControlPermission())
                    this.Close();

                frmMain.FormClosed += frmMain_FormClosed;
                frmMain.Show();
            }
            catch(Exception)
            {

            }
        }

        void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void FrmLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
