﻿using DomainViewer.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DomainViewer.View
{
    public partial class PopupWhois : Form
    {
        public PopupWhois(string domainNameDotVn, string domainDotComDotVN,string domainQT)
        {
            InitializeComponent();
			LoadControl(domainNameDotVn, domainDotComDotVN, domainQT);
        }

		void LoadControl(string domainNameDotVn, string domainDotComDotVN, string domainQT)
        {
			if (!String.IsNullOrEmpty(domainQT))
			{

				ProcessMatBao process = new ProcessMatBao(domainQT);
				WebBrowser webBrowser = process.WhoisDomainToView();

				if (webBrowser != null)
				{
					GroupBox groupBox = new GroupBox();
					groupBox.Text = "Domain Quốc tế";
					groupBox.Parent = splMain.Panel1;
					groupBox.Dock = DockStyle.Fill;

					webBrowser.Parent = groupBox;
					webBrowser.Dock = DockStyle.Fill;
				}
				//process.WhoisDomain();
			}
			else
			{
				splMain.Panel1Collapsed = true;
			}

            if (!String.IsNullOrEmpty(domainNameDotVn))
            {
                ProcessVnnic process = new ProcessVnnic(domainNameDotVn);
                WebBrowser webBrowser = process.WhoisDomain();

                if (webBrowser != null)
                {
                    GroupBox groupBox = new GroupBox();
                    groupBox.Text = "Domain .vn";
                    groupBox.Parent = splMain.Panel1;
                    groupBox.Dock = DockStyle.Fill;

                    webBrowser.Parent = groupBox;
                    webBrowser.Dock = DockStyle.Fill;
                }
            }
            else
            {
                splMain.Panel1Collapsed = true;
            }

            if (!String.IsNullOrEmpty(domainDotComDotVN))
            {
                ProcessVnnic process = new ProcessVnnic(domainDotComDotVN);
                WebBrowser webBrowser = process.WhoisDomain();

                if (webBrowser != null)
                {
                    GroupBox groupBox = new GroupBox();
                    groupBox.Text = "Domain .com.vn";
                    groupBox.Parent = splMain.Panel2;
                    groupBox.Dock = DockStyle.Fill;

                    webBrowser.Parent = groupBox;
                    webBrowser.Dock = DockStyle.Fill;
                }
            }
            else
            {
                splMain.Panel2Collapsed = true;
            }
        }

        private void PopupWhois_Load(object sender, EventArgs e)
        {

        }
    }
}
