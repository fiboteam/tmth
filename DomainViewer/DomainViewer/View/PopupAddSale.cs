﻿using DomainViewer.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DomainViewer.Controller;

namespace DomainViewer.View
{
    public partial class PopupAddSale : Form
    {
        FrmLogin frmLogin;
        List<Salesman> listSaleMan = null;
        Salesman currentItem = null;
        bool isLoad = false;
        int rowCurrent = -1;

        public PopupAddSale(FrmLogin frm)
        {
            frmLogin = frm;
            InitializeComponent();

            cboAccessType.DataSource = Tool.EnumToObject(typeof(AccessType));
            cboAccessType.DisplayMember = "Name";
            cboAccessType.ValueMember = "Value";

            cboStatus.DataSource = Tool.EnumToObject(typeof(SaleStatus));
            cboStatus.DisplayMember = "Name";
            cboStatus.ValueMember = "Value";

            LoadData();
        }

        public void LoadData()
        {
            try
            {
                listSaleMan = SaleControler.GetAll();
                if (listSaleMan == null)
                {
                    listSaleMan = new List<Salesman>();
                    MessageBox.Show("Dữ liệu rỗng!!!");
                }

                dgvSale.DataSource = null;
                dgvSale.Rows.Clear();
                dgvSale.Columns.Clear();

                DataGridViewComboBoxColumn columnStatus = new DataGridViewComboBoxColumn();
                {
                    columnStatus.Name = "Status";
                    columnStatus.DataPropertyName = "Status";
                    columnStatus.HeaderText = "Status";
                    columnStatus.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                }

                columnStatus.DataSource = Tool.EnumToObject(typeof(SaleStatus));
                columnStatus.DisplayMember = "Name";
                columnStatus.ValueMember = "Value";
                dgvSale.Columns.Add(columnStatus);


                DataGridViewComboBoxColumn columnAccessType = new DataGridViewComboBoxColumn();
                {
                    columnAccessType.Name = "AccessType";
                    columnAccessType.DataPropertyName = "AccessType";
                    columnAccessType.HeaderText = "AccessType";
                    columnAccessType.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                }

                columnAccessType.DataSource = Tool.EnumToObject(typeof(AccessType));
                columnAccessType.DisplayMember = "Name";
                columnAccessType.ValueMember = "Value";
                dgvSale.Columns.Add(columnAccessType);

                dgvSale.DataSource = listSaleMan;
                dgvSale.Columns["Password"].Visible = false;
                dgvSale.Columns["SaleId"].Visible = false;
                dgvSale.Columns["Status"].DisplayIndex = dgvSale.Columns.Count - 2;
                dgvSale.Columns["AccessType"].DisplayIndex = dgvSale.Columns.Count - 2;
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi Load Data");
            }
        }

        private void addSaleBtn_Click(object sender, EventArgs e)
        {
            if (salenameTxt.Text != "" || salenameTxt.Text != null) {
                Salesman saleMan = new Salesman();
                saleMan.SaleName = salenameTxt.Text;
                saleMan.Phone = phoneTxt.Text;
                saleMan.Address = addressTxt.Text;
                saleMan.Password = String.IsNullOrEmpty(txtPassword.Text) ? Tool.GetMD5("12345") : Tool.GetMD5(txtPassword.Text);
                saleMan.AccessType = (short)cboAccessType.SelectedValue;
                saleMan.Status = (short)cboStatus.SelectedValue;
                if (SaleControler.InsertSaleman(saleMan))
                {
                    listSaleMan.Add(saleMan);
                    dgvSale.DataSource = null;
                    dgvSale.DataSource = listSaleMan;

                    dgvSale.ClearSelection();
                    foreach(DataGridViewRow i in dgvSale.Rows)
                    {
                        if (saleMan.SaleId == (int)i.Cells["SaleId"].Value)
                        {
                            dgvSale.Rows[i.Index].Cells["SaleId"].Selected = true;
                            break;
                        }
                    }

                    MessageBox.Show("Thêm thành công");
                }
                else
                    MessageBox.Show("Thêm thất bại");



                //if (status == "success")
                //{
                //    this.Close();
                //    Form1 form = new Form1(frmLogin);
                //    ((Form1)this.Owner).setSalesToCheckbox();
                //    //form.addSaleToCheckbox();
                //    //form.setSalesToCheckbox();
                //}
            }
            
        }

        private void PopupAddSale_Load(object sender, EventArgs e)
        {
            isLoad = true;
        }

        private void dgvSale_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (!isLoad)
                return;

            currentItem = listSaleMan.FirstOrDefault(p => p.SaleId == (int)dgvSale.Rows[e.RowIndex].Cells["SaleId"].Value);
            rowCurrent = e.RowIndex;
            SetValueToControl();
        }

        void SetValueToControl()
        {
            try
            {
                if(currentItem != null)
                {
                    salenameTxt.Text = currentItem.SaleName;
                    txtPassword.Text = "";
                    phoneTxt.Text = currentItem.Phone;
                    addressTxt.Text = currentItem.Address;
                    cboStatus.SelectedValue = currentItem.Status.Value;
                    cboAccessType.SelectedValue = currentItem.AccessType.Value;
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Không gắn được dữ liệu");
            }
        }

        bool SetValueToCurrentItem()
        {
            try
            {
                if (currentItem != null)
                {
                    dgvSale.Rows[rowCurrent].Cells["SaleName"].Value = salenameTxt.Text;
                    dgvSale.Rows[rowCurrent].Cells["Phone"].Value = phoneTxt.Text;
                    dgvSale.Rows[rowCurrent].Cells["Address"].Value = addressTxt.Text;
                    dgvSale.Rows[rowCurrent].Cells["Status"].Value = (short)cboStatus.SelectedValue;
                    dgvSale.Rows[rowCurrent].Cells["AccessType"].Value = (short)cboAccessType.SelectedValue;

                    if (!String.IsNullOrEmpty(txtPassword.Text))
                        dgvSale.Rows[rowCurrent].Cells["Password"].Value = Tool.GetMD5(txtPassword.Text);
                }

                return true;
            }
            catch (Exception)
            {
                
            }

            MessageBox.Show("Không gắn được dữ liệu");
            return false;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (SetValueToCurrentItem())
            {
                if (SaleControler.UpdateSaleMan(currentItem))
                    MessageBox.Show("Cập nhật thành công!!!");
                else
                    MessageBox.Show("Cập nhật thất bại");
            }
        }

        private void PopupAddSale_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 form = new Form1(frmLogin);
            ((Form1)this.Owner).setSalesToCheckbox();
        }
    }
}
