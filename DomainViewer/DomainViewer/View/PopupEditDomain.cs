﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DomainViewer.Controller;
using DomainViewer.Utility;

namespace DomainViewer.View
{
    public partial class PopupEditDomain : Form
    {
        Salesman accountLogin;
        private DomainData domainData;
        private int currentRowIndex;
        public PopupEditDomain(DomainData domainData,int rowIndex, Salesman accountLogin)
        {
            
            InitializeComponent();

            this.accountLogin = accountLogin;

            this.domainData = domainData;
            domainValueLbl.Text = domainData.Domain.ToString();
            sourceValueLbl.Text = domainData.SourceString;
            captureDayValueLbl.Text = domainData.CaptureDay.ToString();
            dotComValueLbl.Text = domainData.DotCom;
            dotVNValueLbl.Text = domainData.DotVN;
            saleValueLbl.Text = domainData.Sales;
            note1RTB.Text = domainData.Note1.Replace("@nl","/n");
            note2RTB.Text = domainData.Note2.Replace("@nl", "/n");
            note3RTB.Text = domainData.Note3.Replace("@nl", "/n");
            this.currentRowIndex = rowIndex;

            var sourceSale = ExcelController.getContext()
                .Salesmen.Where(p => p.AccessType != null
                && p.AccessType == (short)AccessType.Sale
                && p.Status == (short)SaleStatus.Active).ToList();

            sourceSale.Insert(0, new Salesman() { SaleName = "" });
            cboSale.DataSource = sourceSale;
            cboSale.DisplayMember = "SaleName";
            cboSale.ValueMember = "SaleName";
            cboSale.SelectedValue = this.domainData.Sales;

            if(!LoadControlPermission())
            {
                MessageBox.Show("Lỗi không phân quyền được");
                this.Dispose();
            }
        }

        private void PopupEditDomain_Shown(object sender, EventArgs e)
        {
            
        }

        private void updateDomainBtn_Click(object sender, EventArgs e)
        {
            if (accountLogin.AccessType != null && accountLogin.AccessType.Value == (short)AccessType.Admin && !String.IsNullOrEmpty(saleValueLbl.Text))
            {
                if (!cboSale.Text.Equals(saleValueLbl.Text))
                {
                    var entity = ExcelController.getContext().Salesmen.FirstOrDefault(p => p.SaleName.Equals(saleValueLbl.Text));
                    if (entity != null)
                    {
                        if (entity.Status == null || entity.Status.Value == (short)SaleStatus.DeActive)
                        {
                            if (MessageBox.Show("Domain này thuộc một Sale có trạng thái DeActive.\r\nBạn có chắc muốn đổi không?", "Hỏi", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                                return;
                        }
                    }

                    if (String.IsNullOrEmpty(cboSale.Text))
                    {
                        if (MessageBox.Show("Bạn có chắc không chọn Sale cho Domain này?", "Hỏi", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                            return;
                    }

                    this.domainData.Sales = cboSale.Text;
                }
            }

            this.domainData.Note1= note1RTB.Text; //.Replace("/n","@nl");
            this.domainData.Note2 = note2RTB.Text; //.Replace("/n", "@nl");
            this.domainData.Note3 = note3RTB.Text; //.Replace("/n", "@nl");
            this.domainData.LastUpdate = DateTime.Now;
            //ExcelController.UpdateDomainData(this.domainData,this.domainData);
            ExcelController.AddOrUpdateEntity(this.domainData, this.domainData);
            ((Form1)this.Owner).updateRecordDomain(this.domainData,this.currentRowIndex);
            this.Hide();
        }

        private void cancelUpdateDomainBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopupEditDomain_Load(object sender, EventArgs e)
        {

        }

        public bool LoadControlPermission()
        {
            try
            {
                var listControl = Tool.GetControls(this.Controls)
                    .Where(p =>
                        p.Tag != null &&
                        !String.IsNullOrEmpty(p.Tag.ToString())
                        )
                    .ToList();

                var listPermission = listControl
                    .Where(p => p.Tag.ToString() != accountLogin.AccessType.Value.ToString())
                    .ToList();

                if (listPermission == null)
                    return false;

                foreach (var i in listPermission)
                {
                    i.Visible = false;
                }

                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi phần quyền. Chương trình sẽ tắt!!!");
            }

            return false;
        }
    }
}
