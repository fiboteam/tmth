﻿namespace DomainViewer.View
{
    partial class PopupEditDomain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.domainTitleLbl = new System.Windows.Forms.Label();
            this.domainValueLbl = new System.Windows.Forms.Label();
            this.sourceValueLbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.captureDayValueLbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.saleValueLbl = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dotComValueLbl = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dotVNValueLbl = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.updateDomainBtn = new System.Windows.Forms.Button();
            this.cancelUpdateDomainBtn = new System.Windows.Forms.Button();
            this.note2RTB = new System.Windows.Forms.RichTextBox();
            this.note1RTB = new System.Windows.Forms.RichTextBox();
            this.note3RTB = new System.Windows.Forms.RichTextBox();
            this.cboSale = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // domainTitleLbl
            // 
            this.domainTitleLbl.AutoSize = true;
            this.domainTitleLbl.Location = new System.Drawing.Point(28, 9);
            this.domainTitleLbl.Name = "domainTitleLbl";
            this.domainTitleLbl.Size = new System.Drawing.Size(51, 14);
            this.domainTitleLbl.TabIndex = 0;
            this.domainTitleLbl.Text = "Domain:";
            // 
            // domainValueLbl
            // 
            this.domainValueLbl.AutoSize = true;
            this.domainValueLbl.Location = new System.Drawing.Point(83, 9);
            this.domainValueLbl.Name = "domainValueLbl";
            this.domainValueLbl.Size = new System.Drawing.Size(38, 14);
            this.domainValueLbl.TabIndex = 1;
            this.domainValueLbl.Text = "label2";
            // 
            // sourceValueLbl
            // 
            this.sourceValueLbl.AutoSize = true;
            this.sourceValueLbl.Location = new System.Drawing.Point(209, 65);
            this.sourceValueLbl.Name = "sourceValueLbl";
            this.sourceValueLbl.Size = new System.Drawing.Size(38, 14);
            this.sourceValueLbl.TabIndex = 3;
            this.sourceValueLbl.Text = "label1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(149, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "Source:";
            // 
            // captureDayValueLbl
            // 
            this.captureDayValueLbl.AutoSize = true;
            this.captureDayValueLbl.Location = new System.Drawing.Point(83, 37);
            this.captureDayValueLbl.Name = "captureDayValueLbl";
            this.captureDayValueLbl.Size = new System.Drawing.Size(38, 14);
            this.captureDayValueLbl.TabIndex = 5;
            this.captureDayValueLbl.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 14);
            this.label5.TabIndex = 4;
            this.label5.Text = "CaptureDay:";
            // 
            // saleValueLbl
            // 
            this.saleValueLbl.AutoSize = true;
            this.saleValueLbl.Location = new System.Drawing.Point(209, 93);
            this.saleValueLbl.Name = "saleValueLbl";
            this.saleValueLbl.Size = new System.Drawing.Size(38, 14);
            this.saleValueLbl.TabIndex = 11;
            this.saleValueLbl.Text = "label6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(165, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 14);
            this.label7.TabIndex = 10;
            this.label7.Text = "Sale:";
            // 
            // dotComValueLbl
            // 
            this.dotComValueLbl.AutoSize = true;
            this.dotComValueLbl.Location = new System.Drawing.Point(83, 93);
            this.dotComValueLbl.Name = "dotComValueLbl";
            this.dotComValueLbl.Size = new System.Drawing.Size(38, 14);
            this.dotComValueLbl.TabIndex = 9;
            this.dotComValueLbl.Text = "label8";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 14);
            this.label9.TabIndex = 8;
            this.label9.Text = ".COM.VN:";
            // 
            // dotVNValueLbl
            // 
            this.dotVNValueLbl.AutoSize = true;
            this.dotVNValueLbl.Location = new System.Drawing.Point(83, 65);
            this.dotVNValueLbl.Name = "dotVNValueLbl";
            this.dotVNValueLbl.Size = new System.Drawing.Size(45, 14);
            this.dotVNValueLbl.TabIndex = 7;
            this.dotVNValueLbl.Text = "label10";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(48, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 14);
            this.label11.TabIndex = 6;
            this.label11.Text = ".VN:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(271, 119);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 14);
            this.label12.TabIndex = 14;
            this.label12.Text = "Note3";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 229);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 14);
            this.label13.TabIndex = 13;
            this.label13.Text = "Note2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 119);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 14);
            this.label14.TabIndex = 12;
            this.label14.Text = "Note1";
            // 
            // updateDomainBtn
            // 
            this.updateDomainBtn.Location = new System.Drawing.Point(280, 301);
            this.updateDomainBtn.Name = "updateDomainBtn";
            this.updateDomainBtn.Size = new System.Drawing.Size(98, 27);
            this.updateDomainBtn.TabIndex = 18;
            this.updateDomainBtn.Text = "Update";
            this.updateDomainBtn.UseVisualStyleBackColor = true;
            this.updateDomainBtn.Click += new System.EventHandler(this.updateDomainBtn_Click);
            // 
            // cancelUpdateDomainBtn
            // 
            this.cancelUpdateDomainBtn.Location = new System.Drawing.Point(384, 303);
            this.cancelUpdateDomainBtn.Name = "cancelUpdateDomainBtn";
            this.cancelUpdateDomainBtn.Size = new System.Drawing.Size(103, 25);
            this.cancelUpdateDomainBtn.TabIndex = 19;
            this.cancelUpdateDomainBtn.Text = "Cancel";
            this.cancelUpdateDomainBtn.UseVisualStyleBackColor = true;
            this.cancelUpdateDomainBtn.Click += new System.EventHandler(this.cancelUpdateDomainBtn_Click);
            // 
            // note2RTB
            // 
            this.note2RTB.Location = new System.Drawing.Point(22, 246);
            this.note2RTB.Name = "note2RTB";
            this.note2RTB.Size = new System.Drawing.Size(216, 82);
            this.note2RTB.TabIndex = 20;
            this.note2RTB.Text = "";
            // 
            // note1RTB
            // 
            this.note1RTB.Location = new System.Drawing.Point(22, 136);
            this.note1RTB.Name = "note1RTB";
            this.note1RTB.Size = new System.Drawing.Size(216, 90);
            this.note1RTB.TabIndex = 21;
            this.note1RTB.Text = "";
            // 
            // note3RTB
            // 
            this.note3RTB.Location = new System.Drawing.Point(274, 136);
            this.note3RTB.Name = "note3RTB";
            this.note3RTB.Size = new System.Drawing.Size(219, 90);
            this.note3RTB.TabIndex = 22;
            this.note3RTB.Text = "";
            // 
            // cboSale
            // 
            this.cboSale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSale.FormattingEnabled = true;
            this.cboSale.Location = new System.Drawing.Point(6, 21);
            this.cboSale.Name = "cboSale";
            this.cboSale.Size = new System.Drawing.Size(207, 22);
            this.cboSale.TabIndex = 23;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboSale);
            this.groupBox1.Location = new System.Drawing.Point(274, 229);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 50);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Tag = "1";
            this.groupBox1.Text = "Đổi Sale";
            // 
            // PopupEditDomain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 342);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.note3RTB);
            this.Controls.Add(this.note1RTB);
            this.Controls.Add(this.note2RTB);
            this.Controls.Add(this.cancelUpdateDomainBtn);
            this.Controls.Add(this.updateDomainBtn);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.saleValueLbl);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dotComValueLbl);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dotVNValueLbl);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.captureDayValueLbl);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.sourceValueLbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.domainValueLbl);
            this.Controls.Add(this.domainTitleLbl);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Name = "PopupEditDomain";
            this.Text = "PopupEditDomain";
            this.Load += new System.EventHandler(this.PopupEditDomain_Load);
            this.Shown += new System.EventHandler(this.PopupEditDomain_Shown);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label domainTitleLbl;
        private System.Windows.Forms.Label domainValueLbl;
        private System.Windows.Forms.Label sourceValueLbl;
        private System.Windows.Forms.Label captureDayValueLbl;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label dotVNValueLbl;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label dotComValueLbl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label saleValueLbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button updateDomainBtn;
        private System.Windows.Forms.Button cancelUpdateDomainBtn;
        private System.Windows.Forms.RichTextBox note2RTB;
        private System.Windows.Forms.RichTextBox note1RTB;
        private System.Windows.Forms.RichTextBox note3RTB;
        private System.Windows.Forms.ComboBox cboSale;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}