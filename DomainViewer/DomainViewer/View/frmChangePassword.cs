﻿using DomainViewer.Controller;
using DomainViewer.Model;
using DomainViewer.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DomainViewer.View
{
    public partial class frmChangePassword : Form
    {
        Salesman accountLogin;

        public frmChangePassword(Salesman accountLogin)
        {
            InitializeComponent();
            this.accountLogin = accountLogin;
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(txtOldPassword.Text) || String.IsNullOrEmpty(txtNewPassword.Text) || String.IsNullOrEmpty(txtReNewPassword.Text))
            {
                MessageBox.Show("Nhập đầy đủ thông tin");
                return;
            }

            if(!txtNewPassword.Text.Equals(txtReNewPassword.Text))
            {
                MessageBox.Show("Mật khẩu mới không giống nhau");
                return;
            }

            var item = SaleControler.GetByID(this.accountLogin.SaleId);
            if(item == null)
            {
                MessageBox.Show("Thông tin không hợp lệ");
                return;
            }

            if(!Tool.GetMD5(txtOldPassword.Text).Equals(item.Password))
            {
                MessageBox.Show("Mật khẩu củ không đúng");
                return;
            }

            item.Password = Tool.GetMD5(txtNewPassword.Text);
            if(SaleControler.UpdateSaleMan(item))
            {
                MessageBox.Show("Đổi mật khẩu thành công");
                this.Dispose();
            }
            else
            {
                MessageBox.Show("Đổi mật khẩu thất bại");
            }
        }
    }
}
