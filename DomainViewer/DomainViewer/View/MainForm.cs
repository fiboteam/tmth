﻿using DomainViewer;
using DomainViewer.Model;
using DomainViewer.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DomainViewer.View;
using DomainViewer.Controller;
using System.Configuration;
using System.Data.SqlClient;
using System.Media;
using DomainService;
using DomainService.Models;
using DTO.Objects;

namespace DomainViewer
{
    public partial class Form1 : Form
    {
        FrmLogin frmLogin;
        public Salesman accountLogin = null;
        ToolDomain toolDomain = null;
        bool finish = true;
        int indexSelectShowMenu = -1;
		int columnIndex = -1;
		string columnNameClick = string.Empty;
        DateTime LastViewWhois = new DateTime(0);
        int DelayWhois = int.Parse(Tool.GetParamAppConfig("DelayWhois"));

		private readonly DomainBookedService _domainBookedService = new DomainBookedService();
		private DomainBooked _DomainBookedSelected;

        public Form1(FrmLogin frm)
        {
            frmLogin = frm;

            InitializeComponent();

            tabControl1.TabPages[0].Text = "Quản lý Sale";

            tabControl1.TabPages[1].Text = "Quản lý Domain";

            this.WindowState = FormWindowState.Maximized;
            Dictionary<string, string> fieldsSearch = new Dictionary<string, string>();
            fieldsSearch.Add("Domain", "Domain");
            fieldsSearch.Add("SourceString", "Source");
            fieldsSearch.Add("DotVN", ".vn");
            fieldsSearch.Add("DotCom", ".com.vn");
            fieldsSearch.Add("Sales", "Sale");
            valueSearchByTextCbb.DataSource = new BindingSource(fieldsSearch, null);
            valueSearchByTextCbb.DisplayMember = "Value";
            valueSearchByTextCbb.ValueMember = "Key";

            Dictionary<string, string> fieldsSearchDate = new Dictionary<string, string>();
            fieldsSearchDate.Add("", "");
            fieldsSearchDate.Add("LastUpdate", "Last Update");
            fieldsSearchDate.Add("CaptureDay", "Capture Day");
            valueSearchByDateCbb.DataSource = new BindingSource(fieldsSearchDate, null);
            valueSearchByDateCbb.DisplayMember = "Value";
            valueSearchByDateCbb.ValueMember = "Key";
            this.valueSearchByDateCbb.SelectedIndexChanged +=
            new System.EventHandler(valueSearchByTextCbb_SelectedIndexChanged);
            
            this.setSalesToCheckbox();
            //mainForm = this;
        }
        //public DomainViewerDbContext context = new DomainViewerDbContext();
        private void button1_Click(object sender, EventArgs e)
        {


            //dgvDomain.DataBindings= lstDomainData;
        }

        private void import_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string filePath = openFileDialog1.FileName;
            urlFileImortLbl.Text = filePath;

            //dgvDomain.DataSource = dt;
        }

        private void importBtn_Click(object sender, EventArgs e)
        {
            if(!finish)
            {
                MessageBox.Show("Đang chạy!!!\r\n" + toolDomain.messageProcess);
                return;
            }
            
            finish = false;
            statusImportLbl.Text = "Đang thực hiện Import!!!";

            if (toolDomain == null)
                toolDomain = new ToolDomain();

            Task.Factory.StartNew(() => {
                try
                {
                    var result = ToolDomain.ReadExcel(urlFileImortLbl.Text, header: true);

                    if (result.Value == null || result.Value.Rows.Count == 0)
                    {
                        MessageBox.Show(result.Key);
                        return;
                    }

                    string resultImport = toolDomain.AddUpdateToDomainData(result.Value);

                    this.Invoke((MethodInvoker)(() => {
                        statusImportLbl.Text = resultImport;
                        if (toolDomain.listError.Count > 0)
                        {
                            PopupViewData frm = new PopupViewData(toolDomain.listError.ToList());
                            frm.Text = "Danh sách các Domain Import lỗi";
                            frm.Show();
                        }
                    }));
                    finish = true;

                    toolDomain = null;
                    SystemSounds.Beep.Play();
                    MessageBox.Show("Đã chạy xong!!!");
                }
                catch(Exception ex)
                {
                    toolDomain = null;
                    SystemSounds.Beep.Play();
                    MessageBox.Show("Lỗi\r\n---" + ex.Message + "\r\n---" + ex.ToString());
                    finish = true;
                }
            });

            //statusImportLbl.Text = ExcelController.ImportToDatabase(urlFileImortLbl.Text);
        }

        public void dgvDomainData()
        {
            DomainViewerDbContext context = ExcelController.getContext();

            var query = (IEnumerable<DomainData>)context.DomainDatas;

            if (accountLogin.AccessType.Value != (short)AccessType.Admin)
            {
                query = query.Where(p => p.Sales.Equals(accountLogin.SaleName));
            }

            query = query.OrderBy(p => p.CaptureDay);

            //var query = (from c in context.DomainDatas select c).OrderBy(c=>c.CaptureDay);
            List<DomainDataModel> lstDomainData = new List<DomainDataModel>();
            foreach (var element in query)
            {
                DomainDataModel domainData = new DomainDataModel();
                domainData.CaptureDay = element.CaptureDay;
                domainData.Domain = element.Domain;
                domainData.DotCom = element.DotCom;
                domainData.DotVN = element.DotVN;
                domainData.LastUpdate = element.LastUpdate;
                domainData.Note1 = element.Note1;
                domainData.Note2 = element.Note2;
                domainData.Note3 = element.Note3;
                domainData.SourceString = element.SourceString;
                domainData.Sales = element.Sales;
                domainData.ID = element.ID;
                lstDomainData.Add(domainData);
            }
            dgvDomain.DataSource = new BindingList<DomainDataModel>(lstDomainData);
            dgvDomain.Columns["ID"].Visible = false;
            //dgvDomain.Columns.Add("STT", "STT");
            dgvDomain.Columns["DotVN"].HeaderText = ".VN";
            dgvDomain.Columns["DotCOM"].HeaderText = ".COM.VN";
            dgvDomain.Columns["Domain"].DisplayIndex = 1;
            dgvDomain.Columns["SourceString"].DisplayIndex = 2;
            dgvDomain.Columns["Domain"].DisplayIndex = 1;
            dgvDomain.Columns["DotVN"].DisplayIndex = 2;
            dgvDomain.Columns["DotCOM"].DisplayIndex = 3;
            dgvDomain.Columns["Sales"].DisplayIndex = 4;
            dgvDomain.Columns["Note1"].DisplayIndex = 5;
            dgvDomain.Columns["Note2"].DisplayIndex = 6;
            dgvDomain.Columns["Note3"].DisplayIndex = 7;
            dgvDomain.Columns["CaptureDay"].DisplayIndex = 8;
            dgvDomain.Columns["LastUpdate"].DisplayIndex = 9;
            LoadSerial(dgvDomain);
            foreach (DataGridViewColumn column in dgvDomain.Columns)
            {

                dgvDomain.Columns[column.Name].SortMode = DataGridViewColumnSortMode.Automatic;
            }

        }
        private void LoadSerial(DataGridView dt)
        {
            
        }
        private void dgvDomain_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        
        public class ComboboxItem
        {
            public string Text { get; set; }
            public string Value { get; set; }
            public override string ToString() { return Text; }
        }
        private void searchValueBtn_Click(object sender, EventArgs e)
        {
            //ComboboxItem selectedValue = (ComboboxItem)valueSearchByTextCbb.SelectedItem;
            string selectedValueCbb="";
            if (valueSearchByTextCbb.SelectedValue != null)
            {
                selectedValueCbb = valueSearchByTextCbb.SelectedValue.ToString();
            }


            string selectedValueDateCbb = valueSearchByDateCbb.SelectedValue != null ? valueSearchByDateCbb.SelectedValue.ToString() : "";
            List<DomainDataModel> tempLst = new List<DomainDataModel>();
            if (selectedValueDateCbb=="") {
                tempLst = ExcelController.search(accountLogin, selectedValueCbb, searchTextTxt.Text);
            }
            else {
                tempLst = ExcelController.search(accountLogin, selectedValueCbb, searchTextTxt.Text, selectedValueDateCbb,dateFromDtb.Value,dateToDtb.Value);
            }
            lblCountRecord.Text=  tempLst.Count + " dòng";
            dgvDomain.DataSource = tempLst;
        }
        
        private void selectAllBtn_Click(object sender, EventArgs e)
        {

        }

        private void valueSearchByTextCbb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (valueSearchByDateCbb.SelectedValue==null|| valueSearchByDateCbb.SelectedValue.ToString() == "")
            {
                dateFromDtb.Visible = false;
                dateToDtb.Visible = false;
                fromdateLbl.Visible = false;
                todateLbl.Visible = false;
            }
            else {
                dateFromDtb.Visible = true;
                dateToDtb.Visible = true;
                fromdateLbl.Visible = true;
                todateLbl.Visible = true;
            }
        }

        private void addSaleBtn_Click(object sender, EventArgs e)
        {
            PopupAddSale form = new PopupAddSale(frmLogin);
            //form.Parent = this;
            form.StartPosition = FormStartPosition.CenterParent;
            form.ShowDialog(this);
        }
        public void setSalesToCheckbox() {
            List<SalesManModel> lstSalesmen = new List<SalesManModel>();

            lstSalesmen = SaleControler.getAllSalesMan();
            this.salesmanClb.BeginUpdate();
            ((ListBox)this.salesmanClb).DataSource = lstSalesmen;
            ((ListBox)this.salesmanClb).DisplayMember = "SaleName";
            ((ListBox)this.salesmanClb).ValueMember = "SaleId";

            this.salesmanClb.EndUpdate();
        }

        public void updateRecordDomain(DomainData a,int indexRow) {
            dgvDomain.Rows[indexRow].Cells["Sales"].Value = a.Sales;
            dgvDomain.Rows[indexRow].Cells["Note1"].Value = a.Note1;
            dgvDomain.Rows[indexRow].Cells["Note2"].Value = a.Note2;
            dgvDomain.Rows[indexRow].Cells["Note3"].Value = a.Note3;
            dgvDomain.Rows[indexRow].Cells["LastUpdate"].Value = a.LastUpdate;
            dgvDomain.Refresh();
        }
        public void addSaleToCheckbox() {
            //this.salesmanClb.Items.Add();
            SalesManModel sale = new SalesManModel();
            sale.SaleId = 10;
            sale.SaleName = "123213";
            sale.IsCheck = false;
            this.salesmanClb.Items.Add(sale);
        }

        private void salesmanClb_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int a =0;
            foreach (object itemChecked in salesmanClb.CheckedItems)
            {
                a++;
            }
            if (e.NewValue == CheckState.Checked)
            {
                a++;
            }
            else {
                a--;
            }
            numbarCheckLb.Text = "Đã chọn " +a + " sale(s)";
        }

        private void salesmanClb_EnabledChanged(object sender, EventArgs e)
        {

            int a = 0;

        }

        private void salesmanClb_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void divineTask_Click(object sender, EventArgs e)
        {
            List<SalesManModel> lstSalesmen = new List<SalesManModel>();
            foreach (SalesManModel element in salesmanClb.CheckedItems) {
                lstSalesmen.Add(element);
            }
            resultLbl.Text=SaleControler.DivineTasks(lstSalesmen);
        }

        private void dgvDomain_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            int currentRowIndex = Int32.Parse(e.RowIndex.ToString());
            //Get the GridViewRow from Current Row Index
            if (currentRowIndex != -1) {
                var row = dgvDomain.Rows[currentRowIndex];
                int id = Int32.Parse(row.Cells["ID"].Value.ToString());
                DomainData domainData = ExcelController.getDomainDataById(id);
                PopupEditDomain form = new PopupEditDomain(domainData, currentRowIndex, accountLogin);
                //form.Parent = this;
                form.StartPosition = FormStartPosition.CenterParent;
                form.ShowDialog(this);
            }
            
        }

        private void salesmanClb_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void dgvDomain_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void dgvDomain_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string i = dgvDomain.Columns[e.ColumnIndex].Name;
            //if(i=="LastUpdate")
            //dgvDomain.DataSource = ((List<DomainDataModel>)dgvDomain.DataSource).OrderBy(c=>c.LastUpdate).ToList();
        }

        private void dgvDomain_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0) {
                e.Value = e.RowIndex + 1;
            }
        }

        public bool LoadControlPermission()
        {
            try
            {
                if(!accountLogin.AccessType.HasValue)
                {
                    MessageBox.Show("Tài khoản không có quyền. Chương trình sẽ tắt!!!");
                    this.Close();
                    return false;
                }

                if(!Enum.GetValues(typeof(AccessType)).Cast<short>().Contains(accountLogin.AccessType.Value))
                {
                    MessageBox.Show("Tài khoản không có quyền. Chương trình sẽ tắt!!!");
                    this.Close();
                    return false;
                }

                var listControl = Tool.GetControls(this.Controls)
                    .Where(p => 
                        p.Tag != null && 
                        !String.IsNullOrEmpty(p.Tag.ToString())
                        )
                    .ToList();

                var listPermission = listControl
                    .Where(p => p.Tag.ToString() != accountLogin.AccessType.Value.ToString())
                    .ToList();

                if (listPermission == null)
                    return false;

                foreach(var i in listPermission)
                {
                    i.Visible = false;
                }

                return true;
            }
            catch(Exception)
            {
                MessageBox.Show("Lỗi phần quyền. Chương trình sẽ tắt!!!");
            }

            return false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
			dgvDomainBooked.CellFormatting +=
			new System.Windows.Forms.DataGridViewCellFormattingEventHandler(
			this.dgvDomainBooked_CellFormatting);
        }

		private void dgvDomainBooked_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			// Set the background to red for negative values in the Balance column.
			if (dgvDomainBooked.Columns[e.ColumnIndex].Name.Equals("CaptureDate"))
			{
				DateTime intValue;

				try
				{
					if (DateTime.TryParse(e.Value.ToString(), out intValue))
					{
						if ((intValue - DateTime.Now).TotalDays > 0 && (intValue - DateTime.Now).TotalDays <= 10)
						{
							e.CellStyle.BackColor = Color.RoyalBlue;
							e.CellStyle.SelectionBackColor = Color.RoyalBlue;
						}

						if ((intValue - DateTime.Now).TotalDays <= 0)
						{
							e.CellStyle.BackColor = Color.Crimson;
							e.CellStyle.SelectionBackColor = Color.Crimson;
						}
					}
				}
				catch
				{

				}
			}
		}

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            frmChangePassword frm = new frmChangePassword(accountLogin);
            frm.Show();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            frmLogin.Show();
            this.Dispose();
        }

        private void dgvDomain_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                indexSelectShowMenu = e.RowIndex;
				columnIndex=e.ColumnIndex;
				columnNameClick = dgvDomain.Columns[columnIndex].Name;

				switch (columnNameClick)
				{
					case "DotCom":
					case "DotVN":
					case "Domain":
						cmsGrid.Show(Cursor.Position);
						break;
				}

                
            }
        }

        private void cmsGrid_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if(e.ClickedItem.Name.Equals("miShowWhois"))
            {
                if((DateTime.Now - LastViewWhois).TotalSeconds < DelayWhois)
                {
                    MessageBox.Show("Xem từ từ thôi. Cách " + DelayWhois + " giây 1 lần");
                    return;
                }

                if (indexSelectShowMenu < 0 || dgvDomain.Rows.Count == 0)
                    return;

                LastViewWhois = DateTime.Now;

				string domainName = dgvDomain.Rows[indexSelectShowMenu].Cells["Domain"].Value.ToString();
				string domainNameVN = "";
				string domainNameComVN = "";

				switch (columnNameClick)
				{
					case "DotCom":
						domainNameComVN = domainName.Remove(domainName.IndexOf(".")) + ".com.vn";
						domainName = string.Empty;
						break;
					case "DotVN":
						domainNameVN = domainName.Remove(domainName.IndexOf(".")) + ".vn";
						domainName = string.Empty;
						break;
				}
				
                
                

				//if (dgvDomain.Rows[indexSelectShowMenu].Cells["DotVN"].Value != null
				//	&& !String.IsNullOrEmpty(dgvDomain.Rows[indexSelectShowMenu].Cells["DotVN"].Value.ToString()))
				//{
				//	domainNameVN = domainName.Remove(domainName.IndexOf(".")) + ".vn";
				//}

				//if (dgvDomain.Rows[indexSelectShowMenu].Cells["DotCOM"].Value != null
				//	&& !String.IsNullOrEmpty(dgvDomain.Rows[indexSelectShowMenu].Cells["DotCOM"].Value.ToString()))
				//{
				//	domainNameComVN = domainName.Remove(domainName.IndexOf(".")) + ".com.vn";
				//}

				if (String.IsNullOrEmpty(domainNameVN) && String.IsNullOrEmpty(domainNameComVN) && String.IsNullOrEmpty(domainName))
                {
					//MessageBox.Show("Domain này không có .vn và .com.vn");
					MessageBox.Show(string.Format("Chưa chọn domainname"));
                    return;
                }

				PopupWhois frm = new PopupWhois(domainNameVN, domainNameComVN, domainName);
                frm.Show();
            }
        }

		private void btn_DomainBooked_Add_Click(object sender, EventArgs e)
		{
			string domainName = txt_DomainBooked_DomainName.Text.TrimEnd().TrimStart();
			string priceStr = txt_DomainBooked_DepositPrice.Text.TrimEnd().TrimStart();
			string info = rtxt_DomainBooked_CustomerInfo.Text;
			decimal price = 0;

			if(string.IsNullOrEmpty(domainName))
			{
				errorProvider1.SetError(txt_DomainBooked_DomainName, "Chưa nhập tên domain");
				return;
			}
			else
			{
				errorProvider1.SetError(txt_DomainBooked_DomainName, string.Empty);
			}

			if (!decimal.TryParse(priceStr,out price))
			{
				errorProvider1.SetError(txt_DomainBooked_DepositPrice, "Số tiền đặt trước không hợp lệ");
				return;
			}
			else
			{
				errorProvider1.SetError(txt_DomainBooked_DepositPrice, string.Empty);
			}

			DomainBooked booked = new DomainBooked();
			booked.DomainBookedName = domainName;
			booked.DepositPrice = price;
			booked.CustomerInfomation = info;
			booked.SaleManId = accountLogin.SaleId;

			if (booked.DomainBookedName.ToLower().EndsWith(".vn"))
			{
				//whois vn
				ProcessVnnicWithGetData process = new ProcessVnnicWithGetData(booked.DomainBookedName);

				if (process.WhoisDomain() && process.DomainObjectBase != null)
				{
					booked.ExpireDate = process.DomainObjectBase.ExpireDate;
				}

				if (booked.ExpireDate != null)
				{
					booked.CaptureDate = ((DateTime)booked.ExpireDate).AddDays(20);
				}
				else if (chkInput_CaptureDate.Checked)
				{
					booked.CaptureDate = dtp_DomainBooked_CaptureDate.Value;
				}
				//return;
			}
			else
			{
				//whois qt
				ProcessMatBao process = new ProcessMatBao(booked.DomainBookedName);

				if (process.WhoisDomain() && process.DomainObjectBase!=null)
				{
					booked.ExpireDate = process.DomainObjectBase.ExpireDate;
				}

				if(chkInput_CaptureDate.Checked)
				{
					booked.CaptureDate = dtp_DomainBooked_CaptureDate.Value;
				}
			}



			if (_domainBookedService.Insert(booked))
			{
				MessageBox.Show(string.Format("Đặt trước domain {0} thành công.",booked.DomainBookedName));

				DomainBooked searchModel = new DomainBooked();
				searchModel.SaleManId = accountLogin.SaleId;
				LoadDomainBooked(searchModel);
			}
			else
			{
				MessageBox.Show(string.Format("Đặt trước domain {0} thất bại.",booked.DomainBookedName));
			}
		}

		private void LoadDomainBooked(DomainBooked model)
		{

			var listBooked = _domainBookedService.GetAllWithFilter(model);
			if(listBooked!=null)
			{
				dgvDomainBooked.Columns.Clear();
				dgvDomainBooked.DataSource = null;
				//dgvDomainBooked.Columns.Add("ExtenstionProperties", "Extenstion Properties");
				//dgvDomainBooked.Columns[0].DataPropertyName = "ExtenstionProperties";
				dgvDomainBooked.DataSource = listBooked;
			}
		}

		private void btnDomainBookedSearchAll_Click(object sender, EventArgs e)
		{
			DomainBooked searchModel = new DomainBooked();
			searchModel.SaleManId = accountLogin.AccessType == 1 ? 0 : accountLogin.SaleId;
			LoadDomainBooked(searchModel);
		}

		private void btn_DomainBooked_Search_Click(object sender, EventArgs e)
		{
			string name = txt_SearchDomainBookedName.Text.TrimEnd().TrimStart();
			string priceStr = txtSearchDomainBookedPrice.Text.TrimEnd().TrimStart();
			decimal price=0;
			price = decimal.TryParse(priceStr, out price) ? price : 0;
			string info = txtSearchDomainBookedInfo.Text;
			DateTime capture = dtpSearchDomainBookedCaptureDay.Value;
			DateTime expire = dtpSearchDomanBookedExpireDay.Value;

			DomainBooked searchModel = new DomainBooked()
			{
				DomainBookedName = name,
				DepositPrice = price,
				CustomerInfomation = info,
				//CaptureDate = chkUseCaptureDay.Checked ? capture:null,
				//ExpireDate = expire,
				SaleManId = accountLogin.AccessType == 1 ? 0 : accountLogin.SaleId,
			};

			if (chkUseCaptureDay.Checked)
			{
				searchModel.CaptureDate = capture;
			}

			if (chkUseExpireDay.Checked)
			{
				searchModel.ExpireDate = expire;
			}

			LoadDomainBooked(searchModel);
		}

		private void chkUseCaptureDay_CheckedChanged(object sender, EventArgs e)
		{
			dtpSearchDomainBookedCaptureDay.Enabled = chkUseCaptureDay.Checked;
		}

		private void chkUseExpireDay_CheckedChanged(object sender, EventArgs e)
		{
			dtpSearchDomanBookedExpireDay.Enabled = chkUseExpireDay.Checked;
		}

		private void dgvDomainBooked_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if(e.RowIndex>=0)
			{
				DomainBooked model = dgvDomainBooked.Rows[e.RowIndex].DataBoundItem as DomainBooked;
				txt_DomainBooked_DomainName.Text = model.DomainBookedName;
				txt_DomainBooked_DepositPrice.Text = model.DepositPrice.ToString();
				rtxt_DomainBooked_CustomerInfo.Text = model.CustomerInfomation;
				_DomainBookedSelected = model;
			}
			else
			{
				_DomainBookedSelected = null;
			}
		}

		private void btnUpdateDomainBooked_Click(object sender, EventArgs e)
		{
			if(_DomainBookedSelected!=null)
			{
				string priceStr = txt_DomainBooked_DepositPrice.Text.TrimEnd().TrimStart();
				decimal price=0;
				price = decimal.TryParse(priceStr, out price) ? price : 0;
				_DomainBookedSelected.DepositPrice = price;
				_DomainBookedSelected.CustomerInfomation = rtxt_DomainBooked_CustomerInfo.Text;
				if(_domainBookedService.Update(_DomainBookedSelected))
				{
					MessageBox.Show("Update thành công domain: " + _DomainBookedSelected.DomainBookedName);
				}
				else
				{
					MessageBox.Show("Update thất bại.");
				}
			}
			else
			{
				MessageBox.Show("Chưa chọn domain.");
			}
		}

		private void chkInput_CaptureDate_CheckedChanged(object sender, EventArgs e)
		{
			dtp_DomainBooked_CaptureDate.Enabled = chkInput_CaptureDate.Checked;
		}
    }
}
