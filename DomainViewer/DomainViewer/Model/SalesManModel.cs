﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainViewer.Model
{
    class SalesManModel
    {
        public int SaleId { get; set; }
        public string SaleName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public bool IsCheck { get; set; }
    }
}
