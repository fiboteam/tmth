﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainViewer.Model
{
    class DomainDataModel
    {
        public string Domain { get; set; }
        public string SourceString { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime CaptureDay { get; set; }
        public string DotVN { get; set; }
        public string DotCom { get; set; }
        public string Sales { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string Note3 { get; set; }
        public int ID { get; set; }
    }
}
