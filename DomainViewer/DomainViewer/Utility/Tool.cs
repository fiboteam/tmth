﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace DomainViewer.Utility
{
    public class Tool
    {
        static DomainViewerDbContext getContext()
        {
            try
            {
                DomainViewerDbContext context = new DomainViewerDbContext(); ;
                //DomainViewerDbContext.Exists();
                context.Database.Connection.Open();
                context.Database.Connection.Close();
                return context;
            }
            catch
            {
                return null;
            }

        }

        public static string GetMD5(string pwd)
        {
            try
            {
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] bHash = md5.ComputeHash(Encoding.UTF8.GetBytes(pwd));
                StringBuilder sbHash = new StringBuilder();

                foreach (byte b in bHash)
                {
                    sbHash.Append(String.Format("{0:x2}", b));
                }
                return sbHash.ToString();
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public static Salesman checkLogin(string userName, string password)
        {
            try
            {
                password = GetMD5(password);
                var result = getContext().Salesmen.FirstOrDefault(p => p.SaleName.ToLower().Equals(userName) && p.Password.Equals(password));
                if (result != null)
                    result.Password = "";

                return result;
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static string GetParamAppConfig(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings.Get(key);
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public static object EnumToObject(Type enumType)
        {
            try
            {
                Dictionary<string, short> dicEnum = new Dictionary<string, short>();
                foreach(var i in Enum.GetNames(enumType))
                {
                    dicEnum.Add(i, (short)Enum.Parse(enumType, i));
                }

                return dicEnum.Select(p => new { Name = p.Key, Value = p.Value }).ToList();
            }
            catch(Exception)
            {

            }

            return null;
        }

        public static List<Control> GetControls(Control.ControlCollection controls)
        {
            if (controls == null)
                return new List<Control>();

            List<Control> listControl = new List<Control>();
            foreach (Control i in controls)
            {
                listControl.Add(i);
                var property = i.GetType().GetProperty("Controls");
                if (property != null)
                {
                    listControl.AddRange(GetControls((Control.ControlCollection)property.GetValue(i, null)));
                }
            }

            return listControl;
        }
    }
}
