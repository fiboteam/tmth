﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DomainViewer.Utility
{
    public abstract class ProcessBase
    {
        public DateTime LastWhois { set; get; }
        protected int DelayTime { set; get; }
        protected bool FinishGet { set; get; }
        public StatusWhois StatusWhois { set; get; }

        public ProcessBase()
        {
            FinishGet = true;
            LastWhois = new DateTime(0);
            StatusWhois =StatusWhois.Registered;
        }

		//public abstract DomainObjectBase Get(string domainName);
        public abstract bool WhoisDomain();
    }
}
