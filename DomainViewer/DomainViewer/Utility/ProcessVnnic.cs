﻿using DomainViewer.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DomainViewer.Utility
{
    public class ProcessVnnic
    {
        string Vnnic_SelectName = Tool.GetParamAppConfig("Vnnic_SelectName");
        string Vnnic_InputName = Tool.GetParamAppConfig("Vnnic_InputName");
        string Vnnic_SubmitNam = Tool.GetParamAppConfig("Vnnic_SubmitNam");
        string Vnnic_UrlMain = Tool.GetParamAppConfig("Vnnic_UrlMain");
        string Vnnic_UrlCheck = Tool.GetParamAppConfig("Vnnic_UrlCheck");
        string Vnnic_LinkDetail = Tool.GetParamAppConfig("Vnnic_LinkDetail");
        string Vnnic_UrlDetail = Tool.GetParamAppConfig("Vnnic_UrlDetail");
        int Vnnic_TimeWait = int.Parse(Tool.GetParamAppConfig("Vnnic_TimeWait"));
        int TimeTryWhoisProcess = int.Parse(Tool.GetParamAppConfig("TimeTryWhoisProcess"));


        string _domainExtension = "";
        string _domainName = "";
        string _urlCurrent = "";
        bool? _resultWhois = null;
        AutoResetEvent _webResponse;
        DateTime _timeWait = DateTime.Now;
        WebBrowser _webBrowser;

        string DomainName
        {
            set
            {
                value = value.ToLower();
                _domainName = value.Substring(0, value.IndexOf("."));
                _domainExtension = "." + value.Substring(value.IndexOf(".") + 1);
            }
            get { return _domainName + _domainExtension; }
        }

        public ProcessVnnic(string domainName)
        {
            DomainName = domainName;
            _webResponse = new AutoResetEvent(false);
            LoadControl();
        }

        void LoadControl()
        {
            _webBrowser = new WebBrowser();
            _webBrowser.AllowNavigation = true;
            _webBrowser.ScriptErrorsSuppressed = true;
            _webBrowser.DocumentCompleted += _webBrowser_DocumentCompleted;
        }

        void _webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            _timeWait = DateTime.Now;
            _urlCurrent = e.Url.AbsoluteUri;
            ReadWebResponse();
        }

        void ReadWebResponse()
        {
            try
            {
                if (_urlCurrent.Contains(Vnnic_UrlMain))
                {
                    var select = _webBrowser.Document.GetElementsByTagName("option");

                    for (int i = 0; i < select.Count; i++)
                    {
                        if (!select[i].Parent.Name.Equals(Vnnic_SelectName))
                            continue;

                        if (!select[i].InnerText.Equals(_domainExtension))
                        {
                            break;
                        }
                        else
                        {
                            select[i].SetAttribute("selected", "selected");
                        }
                    }

                    var inputDomain = _webBrowser.Document.GetElementsByTagName("input");

                    for (int i = 0; i < inputDomain.Count; i++)
                    {
                        if (inputDomain[i].Name.Equals(Vnnic_InputName))
                        {
                            inputDomain[i].SetAttribute("value", _domainName);
                            break;
                        }
                    }

                    var formElements = _webBrowser.Document.GetElementsByTagName("input");

                    for (int i = 0; i < formElements.Count; i++)
                    {
                        if (formElements[i].Name.Equals(Vnnic_SubmitNam))
                        {
                            formElements[i].InvokeMember("click");
                            break;
                        }
                    }
                }

                //Tìm xem kết quả trả về domain có tồn tại hay không
                if (_urlCurrent.Contains(Vnnic_UrlCheck))
                {
                    var linkElement = _webBrowser.Document.GetElementsByTagName("a");
                    for (int i = 0; i < linkElement.Count; i++)
                    {
                        var tmp = linkElement[i].GetAttribute("href");
                        if (linkElement[i].GetAttribute("href").Contains(Vnnic_LinkDetail))
                        {
                            linkElement[i].InvokeMember("click");
                            break;
                        }
                    }

                    if (_resultWhois == null)
                    {
                        _resultWhois = true;
                    }
                }

                //Submit thành công vào page chi tiết domain
                if (_urlCurrent.Contains(Vnnic_UrlDetail))
                {
                    _resultWhois = true;
                }
            }
            catch (Exception)
            {

            }
        }

        public WebBrowser WhoisDomain()
        {
            try
            {
                int timeTry = 0;
                do
                {
                    LoadControl();
                    _webBrowser.Navigate(Vnnic_UrlMain);

                    while (_resultWhois == null)
                    {
                        Application.DoEvents(); //Phải có mới chạy được Event _webBrowser_DocumentCompleted
                        if ((DateTime.Now - _timeWait).TotalSeconds >= Vnnic_TimeWait)
                            break;
                    }

                    if (_resultWhois != null)
                    {
                        break;
                    }
                } while ((timeTry++) < TimeTryWhoisProcess); //Thử lại

                _webBrowser.DocumentCompleted -= _webBrowser_DocumentCompleted;
                return _webBrowser;
            }
            catch (Exception)
            {
                _webBrowser.DocumentCompleted -= _webBrowser_DocumentCompleted;
                return null;
            }
        }
    }
}
