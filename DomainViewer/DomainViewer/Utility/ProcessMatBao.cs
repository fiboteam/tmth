﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using DomainViewer.Code;
using Utility;
using System.Windows.Forms;

namespace DomainViewer.Utility
{
	public class ProcessMatBao : ProcessBase
    {
        string MatBao_WhoisUrl = Tool.GetParamAppConfig("MatBao_WhoisUrl");
        string MatBao_NoRegister = Tool.GetParamAppConfig("MatBao_NoRegister");
        string MatBao_ReplaceCharDNS = Tool.GetParamAppConfig("MatBao_ReplaceCharDNS");
        string MatBao_Reserved = Tool.GetParamAppConfig("MatBao_Reserved");
        string[] MatBao_CharRemove = Tool.GetParamAppConfig("MatBao_CharRemove").Split('|');
		int TimeTryWhoisProcess = int.Parse(Tool.GetParamAppConfig("TimeTryWhoisProcess"));

        List<string> MatBao_ParamsData = new List<string>(Tool.GetParamAppConfig("MatBao_ParamsData").Split('|'));
        List<string> MatBao_Params = new List<string>(Tool.GetParamAppConfig("MatBao_Params").Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries));

		bool? _resultWhois = null;
		string _urlCurrent = "";
		AutoResetEvent _webResponse;
		DateTime _timeWait = DateTime.Now;
		WebBrowser _webBrowser;

		public string DomainName
		{
			set
			{
				value = value.ToLower();
				domain = value.Substring(0, value.IndexOf("."));
				domainExtension = "." + value.Substring(value.IndexOf(".") + 1);

				MatBao_WhoisUrl = Tool.GetParamAppConfig("MatBao_WhoisUrl").Replace("[domainname]", value);
				MatBao_NoRegister = Tool.GetParamAppConfig("MatBao_NoRegister").Replace("@@@", value);
				MatBao_Reserved = Tool.GetParamAppConfig("MatBao_Reserved").Replace("@@@", value);
			}
			get
			{
				return domain + domainExtension;
			}
		}

		public ProcessMatBao(string domainName)
        {
			DelayTime = int.Parse(Tool.GetParamAppConfig("MatBao_DelayTime"));
			DomainName = domainName;
			_webResponse = new AutoResetEvent(false);
			LoadControl();
        }

		private void LoadControl()
		{
			_webBrowser = new WebBrowser();
			_webBrowser.AllowNavigation = true;
			_webBrowser.ScriptErrorsSuppressed = true;
			_webBrowser.DocumentCompleted += _webBrowser_DocumentCompleted;
		}

		private void _webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
		{
			_timeWait = DateTime.Now;
			_urlCurrent = e.Url.AbsoluteUri;
			ReadWebResponse();
		}

		private void ReadWebResponse()
		{
			try
			{
				//if (_urlCurrent.Contains(MatBao_WhoisUrl))
				//{
				//	var select = _webBrowser.Document.GetElementsByTagName("option");

				//	for (int i = 0; i < select.Count; i++)
				//	{
				//		if (!select[i].Parent.Name.Equals(Vnnic_SelectName))
				//			continue;

				//		if (!select[i].InnerText.Equals(_domainExtension))
				//		{
				//			break;
				//		}
				//		else
				//		{
				//			select[i].SetAttribute("selected", "selected");
				//		}
				//	}

				//	var inputDomain = _webBrowser.Document.GetElementsByTagName("input");

				//	for (int i = 0; i < inputDomain.Count; i++)
				//	{
				//		if (inputDomain[i].Name.Equals(Vnnic_InputName))
				//		{
				//			inputDomain[i].SetAttribute("value", _domainName);
				//			break;
				//		}
				//	}

				//	var formElements = _webBrowser.Document.GetElementsByTagName("input");

				//	for (int i = 0; i < formElements.Count; i++)
				//	{
				//		if (formElements[i].Name.Equals(Vnnic_SubmitNam))
				//		{
				//			formElements[i].InvokeMember("click");
				//			break;
				//		}
				//	}
				//}

				////Tìm xem kết quả trả về domain có tồn tại hay không
				//if (_urlCurrent.Contains(Vnnic_UrlCheck))
				//{
				//	var linkElement = _webBrowser.Document.GetElementsByTagName("a");
				//	for (int i = 0; i < linkElement.Count; i++)
				//	{
				//		var tmp = linkElement[i].GetAttribute("href");
				//		if (linkElement[i].GetAttribute("href").Contains(Vnnic_LinkDetail))
				//		{
				//			linkElement[i].InvokeMember("click");
				//			break;
				//		}
				//	}

				//	if (_resultWhois == null)
				//	{
				//		_resultWhois = true;
				//	}
				//}

				//Submit thành công vào page chi tiết domain
				if (_urlCurrent.Contains(MatBao_WhoisUrl))
				{
					_resultWhois = true;
				}
			}
			catch (Exception)
			{

			}
		}

        

		public DomainObjectBase DomainObjectBase;
        string domain = "";
        string domainExtension = "";

		//public override DomainObjectBase Get(string domainName)
		//{
		//	//Chờ Task Khác Get xong
		//	while (!FinishGet) { }
		//	FinishGet = false;

		//	if ((DateTime.Now - LastWhois).TotalSeconds < DelayTime)
		//	{
		//		Thread.Sleep((int)(DelayTime - (DateTime.Now - LastWhois).TotalSeconds) * 1000);
		//	}

		//	DomainName = domainName;

		//	if (String.IsNullOrEmpty(domain) || String.IsNullOrEmpty(domainExtension))
		//	{
		//		FinishGet = true;
		//		LastWhois = DateTime.Now;
		//		return null;
		//	}

		//	if (WhoisDomain())
		//	{
		//		LastWhois = DateTime.Now;
		//		FinishGet = true;
		//		return DomainObjectBase;
		//	}

		//	FinishGet = true;
		//	LastWhois = DateTime.Now;
		//	return null;
		//}

		public WebBrowser WhoisDomainToView()
		{
			try
			{
				int timeTry = 0;
				do
				{
					LoadControl();
					_webBrowser.Navigate(MatBao_WhoisUrl);

					while (_resultWhois == null)
					{
						Application.DoEvents(); //Phải có mới chạy được Event _webBrowser_DocumentCompleted
						if ((DateTime.Now - _timeWait).TotalSeconds >= DelayTime)
							break;
					}

					if (_resultWhois != null)
					{
						break;
					}
				} while ((timeTry++) < TimeTryWhoisProcess); //Thử lại

				_webBrowser.DocumentCompleted -= _webBrowser_DocumentCompleted;
				return _webBrowser;
			}
			catch (Exception)
			{
				_webBrowser.DocumentCompleted -= _webBrowser_DocumentCompleted;
				return null;
			}
		}

        public override bool WhoisDomain()
        {
            try
            {
                var webClient = new WebClient();
                webClient.Encoding = Encoding.UTF8;
                var textResponse = webClient.DownloadString(MatBao_WhoisUrl);
                textResponse = textResponse.Replace(MatBao_ReplaceCharDNS, " - ");
                textResponse = DomainUtility.RemoveHtmlTags(textResponse);

                var parms = DomainUtility.DeserializeResponseRaw(MatBao_Params, textResponse, MatBao_CharRemove);
                DomainObjectBase = DomainUtility.DeserializeResponse(parms, MatBao_ParamsData);

                if (DomainObjectBase == null)
                {
                    if (textResponse.Contains(MatBao_NoRegister))
                    {
                        //Tên miền chưa được đăng ký
                        StatusWhois = StatusWhois.NotFound;
                        return true;
                    }

                    if(textResponse.Contains(MatBao_Reserved))
                    {
                        //Được Vnnic giữ chỗ không được cấp phát
                        StatusWhois = StatusWhois.NotFound;
                        return true;
                    }
                }

                if (DomainObjectBase == null)
                    return false;

                DomainObjectBase.DomainName = DomainName;
                DomainObjectBase.WhoisServerName = "MatBao";
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

	
	}
}

