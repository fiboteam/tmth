﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DomainViewer.Utility
{
    public enum AccessType: short
    {
        Admin = 1,
        Sale = 2
    }

    public enum SaleStatus: short
    {
        Active = 1,
        DeActive = 2
    }
}
