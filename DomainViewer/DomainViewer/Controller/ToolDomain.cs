﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Excel;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using System.Globalization;
using System.Data.Entity.Migrations;
using System.Collections.Concurrent;

namespace DomainViewer.Controller
{
    public class ToolDomain
    {
        List<string> columnsNonUpdate = new List<string>(ConfigurationManager.AppSettings["ColumnsNonUpdate"].Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries));
        int AmountTask = int.Parse(ConfigurationManager.AppSettings["AmountTask"]);
        List<string> ColumnsName = new List<string>(
                ConfigurationManager.AppSettings["ColumnsName"].Split(new string[] { "|" }
                , StringSplitOptions.RemoveEmptyEntries));

        public ConcurrentBag<DomainData> listError = new ConcurrentBag<DomainData>();

        public static DomainViewerDbContext getContext()
        {
            try
            {
                DomainViewerDbContext context = new DomainViewerDbContext(); ;
                //DomainViewerDbContext.Exists();
                context.Database.Connection.Open();
                context.Database.Connection.Close();
                return context;
            }
            catch
            {
                return null;
            }

        }

        object lockObject = new object();
        int countRecord = 0;
        int countInsert = 0;
        int countUpdate = 0;
        int countError = 0;
        int sumRecourd = 0;
        public string messageProcess
        {
            get
            {
                return "Số dòng đã xữ lý: " + countRecord
                    + "\r\nSố dòng Insert: " + countInsert
                    + "\r\nSố dòng Update: " + countUpdate
                    + "\r\nSố dòng thất bại:" + countError
                    + "\r\nTổng số dòng:" + sumRecourd;
            }
        }

        public string AddUpdateToDomainData(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0)
                return "Dữ liệu rỗng";

            countError = 0;
            countRecord = 0;
            countInsert = 0;
            countUpdate = 0;
            sumRecourd = dt.Rows.Count;
            listError = new ConcurrentBag<DomainData>();

            DomainViewerDbContext _context = getContext();
            if (_context == null)
                return "Không kết nối được Server";
            _context.Dispose();

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                dt.Columns[i].ColumnName = ColumnsName[i];
            }

            List<Task> listTask = new List<Task>();
            List<List<DataRow>> listItemTask = new List<List<DataRow>>();
            int amountTmp = (int)Math.Ceiling((double)dt.Rows.Count / AmountTask);

            for (int i = 0; i < AmountTask; i++)
            {
                listItemTask.Add(dt.AsEnumerable().Skip(i * amountTmp).Take(amountTmp).ToList());
            }

            foreach (var i in listItemTask)
            {
                if (i == null || i.Count == 0)
                    continue;

                var task = new Task(() =>
                {
                    ProcessAddUpdateToDomainData(i);
                }, TaskCreationOptions.LongRunning);

                listTask.Add(task);
                task.Start();
            }

            Task.WaitAll(listTask.ToArray());

            return messageProcess;
        }

        public void ProcessAddUpdateToDomainData(List<DataRow> ListRow)
        {
            try
            {
                DomainViewerDbContext _context = getContext();
                List<DomainData> listDomainData = new List<DomainData>();

                foreach (var i in ListRow)
                {
                    DomainData domainData = null;

                    try
                    {
                        lock (lockObject)
                        {
                            countRecord++;
                        }

                        var valueField = i["Domain"].ToString();

                        if (String.IsNullOrEmpty(valueField) || !valueField.Contains("."))
                        {
                            lock (lockObject)
                            {
                                countError++;
                            }
                            continue;
                        }

                        valueField = valueField.ToLower();
                        var item = _context.DomainDatas.FirstOrDefault(p => p.Domain.ToLower().Equals(valueField));
                        domainData = null;

                        if (item == null)
                            domainData = new DomainData()
                            {
                                Status = "N"
                            };
                        else
                        {
                            domainData = item;
                        }

                        var domainType = domainData.GetType();

                        foreach (var colName in ColumnsName)
                        {
                            if (item != null && columnsNonUpdate.Contains(colName))
                                continue;

                            object value = i[colName];
                            var property = domainType.GetProperty(colName);
                            if (!property.PropertyType.Equals(i[colName].GetType()))
                            {
                                if (value == null)
                                {

                                }
                                else
                                {
                                    if (property.PropertyType.Equals(typeof(DateTime)))
                                    {
                                        value = ParseStringToDateTime(value.ToString());
                                    }
                                    else
                                    {
                                        value = TypeDescriptor.GetConverter(property.PropertyType).ConvertFrom(value.ToString());
                                    }
                                }
                            }

                            property.SetValue(domainData, value, null);
                        }

                        domainData.LastUpdate = DateTime.Now;
                        _context.DomainDatas.AddOrUpdate(domainData);
                        _context.SaveChanges();

                        lock (lockObject)
                        {
                            if (item == null)
                                countInsert++;
                            else
                                countUpdate++;
                        }
                    }
                    catch (Exception)
                    {
                        _context = getContext();

                        if (domainData != null)
                            listError.Add(domainData);

                        lock (lockObject)
                        {
                            countError++;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        public static KeyValuePair<string, DataTable> ReadExcel(string filePath, int sheetIndex = 0, bool header = true)
        {
            FileStream importFile = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            Dictionary<string, DataTable> dicResult = new Dictionary<string, DataTable>();

            try
            {
                IExcelDataReader reader;

                if (importFile == null || importFile.Length <= 0)
                {
                    return new KeyValuePair<string, DataTable>("File không hợp lệ", null);
                }

                if (importFile.Length > (10 * 1024 * 1024))
                {
                    return new KeyValuePair<string, DataTable>("File quá lớn (> 10MB)", null);
                }

                if (importFile.Name.EndsWith(".xlsx"))
                    reader = ExcelReaderFactory.CreateOpenXmlReader(importFile);
                else if (importFile.Name.EndsWith(".xls"))
                    reader = ExcelReaderFactory.CreateBinaryReader(importFile);
                else
                {
                    return new KeyValuePair<string, DataTable>("Định dạng File không hợp lệ. (*.xls|*.xlsx)", null);
                }

                reader.IsFirstRowAsColumnNames = header;
                DataSet ds = reader.AsDataSet();
                reader.Close();

                if (ds.Tables.Count <= 0)
                {
                    return new KeyValuePair<string, DataTable>("Không đọc được dữ liệu trong File", null);
                }

                DataTable dt = ds.Tables[sheetIndex];

                return new KeyValuePair<string, DataTable>("Thành công", dt);
            }
            catch (Exception ex)
            {
                return new KeyValuePair<string, DataTable>("Có lỗi khi đọc file. Hãy kiểm tra lại file", null);
            }
        }

        public static DateTime? ParseStringToDateTime(string dateTime)
        {
            try
            {
                string[] pattern = new string[] 
                { 
                    "d/M/yyyy",
                    "d/M/yyyy HH:mm:ss",
                    "d/M/yyyy hh:mm:ss tt",
                    "dd/MM/yyyy",
                    "dd/MM/yyyy HH:mm:ss",
                    "dd/MM/yyyy hh:mm:ss tt",
                    "M/d/yyyy",
                    "M/d/yyyy HH:mm:ss",
                    "M/d/yyyy hh:mm:ss tt",
                    "MM/dd/yyyy",
                    "MM/dd/yyyy HH:mm:ss",
                    "MM/dd/yyyy hh:mm:ss tt",
                    "yyyy/M/d",
                    "yyyy/M/d HH:mm:ss",
                    "yyyy/M/d hh:mm:ss tt",
                    "yyyy/MM/dd",
                    "yyyy/MM/dd HH:mm:ss",
                    "yyyy/MM/dd hh:mm:ss tt"
                };

                string[] arrParms = dateTime.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                if (arrParms.Length > 0)
                {
                    arrParms[0] = arrParms[0].Replace("-", "/");
                    string rawDateTime = arrParms[0];

                    if (arrParms.Length > 1)
                    {
                        arrParms[1] = arrParms[1].Replace("/", ":");
                        arrParms[1] = arrParms[1].Replace("-", ":");
                        rawDateTime += " " + arrParms[1];

                        if (arrParms.Length > 2)
                        {
                            rawDateTime += " " + arrParms[2];
                        }
                    }

                    DateTime resultDateTime = DateTime.ParseExact(rawDateTime, pattern, CultureInfo.CurrentCulture, DateTimeStyles.None);
                    return resultDateTime;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
