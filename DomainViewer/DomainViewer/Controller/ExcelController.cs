﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data.OleDb;
using System.IO;
using System.Data.Entity;
using DomainViewer.Model;
using ExcelInterop = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Threading;
using System.Data.Entity.Migrations;
using DomainViewer.Utility;

namespace DomainViewer.Controller
{
    class ExcelController
    {
        public static object SqlMethods { get; private set; }
        public static DomainViewerDbContext getContext()
        {
            try
            {
                DomainViewerDbContext context = new DomainViewerDbContext(); ;
                //DomainViewerDbContext.Exists();
                context.Database.Connection.Open();
                context.Database.Connection.Close();
                return context;
            }
            catch
            {
                MessageBox.Show("Không thể kết nối server");
                return null;
            }

        }
        public static List<DomainDataModel> search(Salesman accountLogin, string searchField, string searchText, string searchFieldDate, DateTime dateFrom, DateTime dateTo)
        {
            //DomainViewerDbContext context = getContext();;
            DomainViewerDbContext context = getContext();
            IEnumerable<DomainData> query = context.DomainDatas; //(from c in context.DomainDatas select c).ToList();

            if (accountLogin.AccessType.Value != (short)AccessType.Admin)
            {
                query = query.Where(p => p.Sales.Equals(accountLogin.SaleName));
            }

            switch (searchField)
            {
                case "Domain":
                    query = query.Where(c => c.Domain.ToLower().Contains(searchText.ToLower()));
                    break;
                case "SourceString":
                    query = query.Where(c => c.SourceString.Contains(searchText));
                    break;
                case "DotVN":
                    query = query.Where(c => !String.IsNullOrEmpty(c.DotVN) && c.Domain.ToLower().Contains(searchText.ToLower()));
                    break;
                case "DotCom":
                    query = query.Where(c => !String.IsNullOrEmpty(c.DotCom) && c.Domain.ToLower().Contains(searchText.ToLower()));
                    break;
                case "Sales":
                    query = query.Where(c => c.Sales.ToLower().Contains(searchText.ToLower()));
                    break;
                default:
                    break;
            }

            switch (searchFieldDate)
            {
                case "LastUpdate":
                    query = query.Where(c => DateTime.Compare(c.LastUpdate, DateTime.Parse(dateFrom.ToString("yyyy-MM-dd"))) >= 0 && DateTime.Compare(c.LastUpdate, DateTime.Parse(dateTo.ToString("yyyy-MM-dd"))) <= 0);
                    break;
                case "CaptureDay":
                    query = query.Where(c => DateTime.Compare(c.CaptureDay, DateTime.Parse(dateFrom.ToString("yyyy-MM-dd"))) >= 0 && DateTime.Compare(c.CaptureDay, DateTime.Parse(dateTo.ToString("yyyy-MM-dd"))) <= 0);
                    break;
                default:
                    break;
            }

            query = query.OrderBy(c => c.CaptureDay);
            List<DomainDataModel> lstDomainData = new List<DomainDataModel>();
            foreach (var element in query)
            {
                DomainDataModel domainData = new DomainDataModel();
                domainData.CaptureDay = element.CaptureDay;
                domainData.Domain = element.Domain;
                domainData.DotCom = element.DotCom;
                domainData.DotVN = element.DotVN;
                domainData.LastUpdate = element.LastUpdate;
                domainData.Note1 = element.Note1;
                domainData.Note2 = element.Note2;
                domainData.Note3 = element.Note3;
                domainData.Sales = element.Sales;
                domainData.SourceString = element.SourceString;
                domainData.ID = element.ID;
                lstDomainData.Add(domainData);
            }
            return lstDomainData;
        }

        public static List<DomainDataModel> search(Salesman accountLogin, string searchField, string searchText)
        {
            DomainViewerDbContext context = getContext(); ;
            IEnumerable<DomainData> query = context.DomainDatas; //(from c in context.DomainDatas select c).ToList();

            if (accountLogin.AccessType.Value != (short)AccessType.Admin)
            {
                query = query.Where(p => p.Sales.Equals(accountLogin.SaleName));
            }

            switch (searchField)
            {
                case "Domain":
                    query = query.Where(c => c.Domain.ToLower().Contains(searchText.ToLower())).ToList();
                    break;
                case "SourceString":
                    query = query.Where(c => c.SourceString.Contains(searchText)).ToList();
                    break;
                case "DotVN":
                    query = query.Where(c => !String.IsNullOrEmpty(c.DotVN) && c.Domain.ToLower().Contains(searchText.ToLower())).ToList();
                    break;
                case "DotCom":
                    query = query.Where(c => !String.IsNullOrEmpty(c.DotCom) && c.Domain.ToLower().Contains(searchText.ToLower())).ToList();
                    break;
                case "Sales":
                    query = query.Where(c => c.Sales.ToLower().Contains(searchText.ToLower())).ToList();
                    break;
                default:
                    break;
            }
            query = query.OrderBy(c => c.CaptureDay);
            List<DomainDataModel> lstDomainData = new List<DomainDataModel>();
            foreach (var element in query)
            {
                DomainDataModel domainData = new DomainDataModel();
                domainData.CaptureDay = element.CaptureDay;
                domainData.Domain = element.Domain;
                domainData.DotCom = element.DotCom;
                domainData.DotVN = element.DotVN;
                domainData.LastUpdate = element.LastUpdate;
                domainData.Note1 = element.Note1;
                domainData.Note2 = element.Note2;
                domainData.Note3 = element.Note3;
                domainData.Sales = element.Sales;
                domainData.SourceString = element.SourceString;
                domainData.ID = element.ID;
                lstDomainData.Add(domainData);
            }
            return lstDomainData;
        }

        public static DomainData getDomainDataById(int id)
        {
            DomainViewerDbContext context = getContext(); ;
            DomainData dataSelect = ((from c in context.DomainDatas select c).Where(c => c.ID == id)).FirstOrDefault();
            return dataSelect;
        }

        public DataTable readExcelFromFiles1(string filePath)
        {
            ExcelInterop.Application xlApp;
            ExcelInterop.Workbook xlWorkBook;
            ExcelInterop.Worksheet xlWorkSheet;
            ExcelInterop.Range range;

            //string str="";
            int rCnt = 0;
            int cCnt = 0;

            xlApp = new ExcelInterop.Application();
            xlWorkBook = xlApp.Workbooks.Open(filePath, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (ExcelInterop.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            DataTable dt = new DataTable();
            range = xlWorkSheet.UsedRange;

            dt.Columns.Add("Domain");
            dt.Columns.Add("SourceString");
            dt.Columns.Add("LastUpdate");
            dt.Columns.Add("CaptureDay");
            dt.Columns.Add("DotVN");
            dt.Columns.Add("DotCom");
            dt.Columns.Add("Sales");
            dt.Columns.Add("Note1");
            dt.Columns.Add("Note2");
            dt.Columns.Add("Note3");

            for (rCnt = 2; rCnt <= range.Rows.Count; rCnt++)
            {
                DataRow row = dt.NewRow();
                String[] arr = new string[10];
                for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
                {


                    var a = (range.Cells[rCnt, cCnt] as ExcelInterop.Range);
                    arr[cCnt - 1] = (range.Cells[rCnt, cCnt] as ExcelInterop.Range).Text;
                    //str += str;
                    ////str = (string)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;
                    //MessageBox.Show(str);
                }
                dt.Rows.Add(arr);
            }
            xlWorkBook.Close(true, null, null);
            xlApp.Quit();

            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            return dt;
        }


        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        public DataTable readExcelFromFiles(string filePath)
        {
            if (filePath == "" || filePath == null)
            {
                return null;
            }
            string extension = Path.GetExtension(filePath);
            string header = "Yes";
            string conStr, sheetName;

            string Excel03ConString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";
            string Excel07ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'";

            conStr = string.Empty;
            switch (extension)
            {

                case ".xls": //Excel 97-03
                    conStr = string.Format(Excel03ConString, filePath, header);
                    break;

                case ".xlsx": //Excel 07
                    conStr = string.Format(Excel07ConString, filePath, header);
                    break;
            }

            //Get the name of the First Sheet.
            using (OleDbConnection con = new OleDbConnection(conStr))
            {
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                    con.Close();
                }
            }

            //Read Data from the First Sheet.
            using (OleDbConnection con = new OleDbConnection(conStr))
            {
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    using (OleDbDataAdapter oda = new OleDbDataAdapter())
                    {
                        DataTable dt = new DataTable();
                        cmd.CommandText = "SELECT * From [" + sheetName + "]";
                        cmd.Connection = con;
                        con.Open();
                        oda.SelectCommand = cmd;
                        oda.Fill(dt);
                        con.Close();

                        //Populate DataGridView.
                        //dgvDomain.DataSource = dt;
                        return dt;
                    }
                }
            }

        }
        public static bool IsDate(string strDate)
        {
            //string strDate = obj.ToString();
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                DateTime dt = DateTime.Parse(strDate);
                //if ((dt.Month != System.DateTime.Now.Month) || (dt.Day < 1 && dt.Day > 31) || dt.Year != System.DateTime.Now.Year)
                //    return false;
                //else
                //    return true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void UpdateDomainData(DomainData domainDataOld, DomainData domainDataNew)
        {
            //domain.ID = int.Parse(dataSelect.ID.ToString());
            DomainViewerDbContext context = getContext(); ;
            //context.DomainDatas.Attach(domainDataOld);
            //domainDataOld.SourceString = domainDataNew.SourceString;
            //domainDataOld.Sales = domainDataNew.Sales;
            //domainDataOld.CaptureDay = domainDataNew.CaptureDay;
            //domainDataOld.LastUpdate = domainDataNew.LastUpdate;
            //domainDataOld.Note1 = domainDataNew.Note1;
            //domainDataOld.Note2 = domainDataNew.Note2;
            //domainDataOld.Note3 = domainDataNew.Note3;
            ////context.ObjectStateManager.ChangeObjectState(domain, EntityState.Modified);
            //context.Entry(domainDataOld).State = EntityState.Modified;
            //context.SaveChanges();
            string format = "yyyy-MM-dd HH:mm:ss";
            context.Database.ExecuteSqlCommand("update  DomainData " + "set SourceString='" + domainDataNew.SourceString + "',LastUpdate='" + domainDataNew.LastUpdate.ToString(format) + "',CaptureDay='" + domainDataNew.CaptureDay.ToString(format) + "',DotVN='" + domainDataNew.DotVN + "',DotCom='" + domainDataNew.DotCom + "' " + "where id='" + domainDataOld.ID + "'");
        }

        public static bool AddOrUpdateEntity(DomainData domainDataOld, DomainData domainDataNew)
        {
            try
            {
                DomainViewerDbContext _context = getContext();
                domainDataNew.ID = domainDataOld.ID;
                _context.DomainDatas.AddOrUpdate(domainDataNew);
                _context.SaveChanges();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public static void InsertDomainData(DomainData domain)
        {
            string format = "yyyy-MM-dd HH:MM:ss";
            DomainViewerDbContext context = getContext(); ;
            domain.Note1 = domain.Note1.Replace("\n", "@nl");
            domain.Note2 = domain.Note2.Replace("\n", "@nl");
            domain.Note3 = domain.Note3.Replace("\n", "@nl");
            context.Database.ExecuteSqlCommand("insert into DomainData (Domain,SourceString,LastUpdate,CaptureDay,DotVN,DotCom,Sales,Note1,Note2,Note3) values ('" + domain.Domain + "','" + domain.SourceString + "','" + domain.LastUpdate.ToString(format) + "','" + domain.CaptureDay.ToString(format) + "','" + domain.DotVN + "','" + domain.DotCom + "',N'" + domain.Sales + "',N'" + domain.Note1 + "',N'" + domain.Note2 + "',N'" + domain.Note3 + "')");
        }
        public static string ImportToDatabase(string filePath)
        {
            ExcelController excel = new ExcelController();
            DataTable dt = new DataTable();
            int curentRows = 1;
            int countInsert = 0;
            int countUpdate = 0;
            //dt = excel.readExcelFromFiles(filePath);
            dt.Columns.Add("Domain");
            dt.Columns.Add("SourceString");
            dt.Columns.Add("LastUpdate");
            dt.Columns.Add("CaptureDay");
            dt.Columns.Add("DotVN");
            dt.Columns.Add("DotCom");
            dt.Columns.Add("Sales");
            dt.Columns.Add("Note1");
            dt.Columns.Add("Note2");
            dt.Columns.Add("Note3");
            dt = excel.readExcelFromFiles1(filePath);

            if (dt == null)
            {
                return "Không có dữ liệu";
            }
            DomainViewerDbContext context = getContext(); ;
            foreach (DataRow element in dt.Rows)
            {
                //for (int j = 0; j < element.ItemArray.Length; j++)
                //{
                try
                {
                    curentRows++;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                    //var dataSelect = ((from c in context.DomainDatas select c).Where(c => c.Domain == domain.Domain)).FirstOrDefault();

                    DomainData domain = new DomainData();
                    domain.Domain = element.ItemArray[0].ToString();
                    domain.SourceString = element.ItemArray[1].ToString();
                    //if (!ExcelController.IsDate(element.ItemArray[2].ToString()))
                    //{
                    //    double d = double.Parse(element.ItemArray[2].ToString());
                    //    domain.LastUpdate = DateTime.FromOADate(d);
                    //}
                    //else {
                    //    domain.LastUpdate = DateTime.Parse(element.ItemArray[2].ToString());
                    //    //domain.LastUpdate = new DateTime();
                    //}
                    domain.LastUpdate = DateTime.Now;
                    if (!ExcelController.IsDate(element.ItemArray[2].ToString()))
                    {
                        double d = double.Parse(element.ItemArray[3].ToString());
                        domain.CaptureDay = DateTime.FromOADate(d);
                    }
                    else
                    {
                        domain.CaptureDay = DateTime.Parse(element.ItemArray[3].ToString());
                        //domain.CaptureDay = new DateTime();
                    }
                    domain.DotVN = element.ItemArray[4].ToString();
                    domain.DotCom = element.ItemArray[5].ToString();
                    domain.Sales = element.ItemArray[6].ToString();
                    domain.Note1 = element.ItemArray[7].ToString();
                    domain.Note2 = element.ItemArray[8].ToString();
                    domain.Note3 = element.ItemArray[9].ToString();
                    DomainData dataSelect = ((from c in context.DomainDatas select c).Where(c => c.Domain == domain.Domain)).FirstOrDefault();

                    if (dataSelect != null)
                    {
                        //if (dataSelect.SourceString != domain.SourceString) {
                        countUpdate++;
                        ExcelController.UpdateDomainData(dataSelect, domain);
                        //}


                    }
                    else
                    {

                        ExcelController.InsertDomainData(domain);
                        countInsert++;


                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Lỗi ở gần dòng thứ '{0}'", curentRows);
                    return "Lỗi ở gần dòng thứ: " + curentRows;
                }

                //}
                //var i = element;
            };
            return "Thành công: \n" + countInsert + " insert \n" + countUpdate + " update";
        }
    }
}
