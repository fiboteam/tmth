﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainViewer.Model;
using System.Windows.Forms;
using DomainViewer.Utility;

namespace DomainViewer.Controller
{
    class SaleControler
    {
        public static bool InsertSaleman(Salesman saleMan)
        {
            try
            {
                DomainViewerDbContext context = ExcelController.getContext();
                context.Salesmen.Add(saleMan);
                context.SaveChanges();
                return true;
                //context.Database.ExecuteSqlCommand("insert into SalesMan (SaleName,Address,Phone,Password,AccessType) values (N'" + saleMan.SaleName + "',N'" + saleMan.Address + "',N'" + saleMan.Phone + "', '" + saleMan.Password + "', " + saleMan.AccessType + ")");
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool UpdateSaleMan(Salesman saleMan)
        {
            try
            {
                DomainViewerDbContext context = ExcelController.getContext();
                context.Entry(saleMan).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        public static Salesman GetByID(int ID)
        {
            try
            {
                DomainViewerDbContext context = ExcelController.getContext();
                return context.Salesmen.FirstOrDefault(p => p.SaleId == ID);
            }
            catch(Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// Lấy danh sách Save để chia task
        /// </summary>
        /// <returns></returns>
        public static List<SalesManModel> getAllSalesMan()
        {
            DomainViewerDbContext context = ExcelController.getContext();
            var dataSelect = (from c in context.Salesmen 
                              where c.AccessType.HasValue && 
                                c.AccessType.Value == (short)AccessType.Sale &&
                                c.Status == (short)SaleStatus.Active
                              select c)
                .ToList();
            List<SalesManModel> lstSalesmen = new List<SalesManModel>();
            foreach (var element in dataSelect)
            {
                SalesManModel saleMan = new SalesManModel();
                saleMan.SaleName = element.SaleName;
                saleMan.SaleId = element.SaleId;
                saleMan.Phone = element.Phone;
                saleMan.Address = element.Address;
                lstSalesmen.Add(saleMan);
            }
            return lstSalesmen;
        }

        public static List<Salesman> GetAll()
        {
            try
            {
                DomainViewerDbContext context = ExcelController.getContext();
                return context.Salesmen.ToList();
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static string DivineTasks(List<SalesManModel> lstSalesmen)
        {

            DomainViewerDbContext context = ExcelController.getContext();
            List<DomainData> lstDomain = ((from c in context.DomainDatas select c).Where(c => c.Status == "N").OrderByDescending(c => c.CaptureDay)).ToList();
            if (lstSalesmen.Count == 0 || lstDomain.Count == 0)
            {
                return "";
            }
            int lengthOfDomain = lstDomain.Count;
            int lengthOfSales = lstSalesmen.Count;
            int taskOfEachSale = lengthOfDomain / lengthOfSales;
            int bonusTasks = lengthOfDomain % lengthOfSales;
            int currentUpdateTop = taskOfEachSale;

            int indexArray = 0,
            j = 0;
            int[,] tempArrays = new int[lengthOfSales, (taskOfEachSale + 1)];
            //foreach(SalesManModel sale in lstSalesmen)
            for (int i = 0; i < lstDomain.Count; i++)
            {
                try
                {
                    if (j == lengthOfSales)
                    {
                        j = 0;
                        indexArray++;
                    }
                    tempArrays[j, indexArray] = lstDomain[i].ID;
                    j++;

                }
                catch (Exception e)
                {
                    return "fail";
                }
                //if (i == (lstSalesmen.Count - 1)) {
                //    currentUpdateTop = lengthOfDomain;
                //}
                //if (bonusTasks > 0)
                //{
                //    indexRow = taskOfEachSale + 1;
                //    bonusTasks--;
                //}
                //else {
                //    indexRow = taskOfEachSale;
                //}
                //context.Database.ExecuteSqlCommand("update DomainData set Status='U', Sales=N'"+ lstSalesmen[i].SaleName+ "' "+
                //    "where id in (select b.id from(SELECT *, ROW_NUMBER() OVER(ORDER BY a.id) as rownumber "+
                //    "FROM     DomainData as a where Status = 'N') as b "+
                //    "where  b.rownumber <= "+ indexRow + " )");
            }
            string stringIDUpdate;
            for (int i = 0; i < lengthOfSales; i++)
            {
                stringIDUpdate = "";
                for (int z = 0; z <= indexArray; z++)
                {
                    if (tempArrays[i, z] != 0)
                    {
                        stringIDUpdate += tempArrays[i, z].ToString() + ',';
                    }
                }
                stringIDUpdate = stringIDUpdate.Substring(0, stringIDUpdate.Length - 1);
                context.Database.ExecuteSqlCommand("update DomainData set Status='U', Sales=N'" + lstSalesmen[i].SaleName + "' " +
                    "where id in (" + stringIDUpdate + ")");

                //MessageBox.Show(stringIDUpdate);
            }
            return "Đã chia " + lengthOfDomain + " cho " + lengthOfSales + " !";
        }
    }
}
