﻿using DTO.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Core.Partner
{
	public class NameJetAuction:Partner
	{
		public NameJetAuction(string id)
			: base(id)
		{
			
		}

		public override void ReadFileDownload()
		{
			WriteLogHandle("Thực hiện đọc file");
			WebIsBusy = true;
			FileDownload fileDownload = GetFileDownload();
			if (fileDownload != null)
			{
				WriteLogHandle("Lấy được 1 file Auction chưa đọc: " + fileDownload.LocalPath);
				string filecontent = "";
				using (StreamReader reader = new StreamReader(fileDownload.LocalPath))
				{
					filecontent = reader.ReadToEnd();
				}
				var rows = Regex.Split(filecontent, @"[\r\n]+").ToList();
				WriteLogHandle("File có tất cả: "+rows.Count+" records");
				//foreach (string row in rows)
				//{
				//	WriteLogHandle(row);
				//}
			}
			else
			{
				WriteLogHandle("Không có file Auction nào chưa được đọc.");
			}
			WebIsBusy = false;
			WriteLogHandle("Kết thúc đọc file");
		}
	}
}
