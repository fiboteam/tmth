﻿using Core.Code;
using DTO.Objects;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utility;

namespace Core.Partner
{
	public class DomainsByDay:Partner
	{
		private List<string> _domainNames=new List<string>();
		public DomainsByDay(string id)
			: base(id)
		{
			//_webClient.Credentials = new NetworkCredential(_xPath.UserName, _xPath.Password);
			//_webClient.Encoding = Encoding.UTF8;
		}
		public override string GetContent()
		{
			if (PartnerHtmlDocument != null)
			{
				var node = PartnerHtmlDocument.DocumentNode.SelectNodes(_xPath.Value.Replace("{0}","[1]"));
				if (node != null)
				{
					string date = node.FirstOrDefault().InnerText;
					return date.TrimEnd().TrimStart();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
		public override string GetUrl()
		{
			if (PartnerHtmlDocument != null)
			{
				var node = PartnerHtmlDocument.DocumentNode.SelectNodes(_xPath.Value.Replace("{0}", "[1]"));
				if (node != null)
				{
					string link = node.FirstOrDefault().Attributes["href"].Value;
					return _xPath.HomePage + link.TrimEnd().TrimStart();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public override void DownloadFiles()
		{
			WriteLogHandle("Thực hiện get domains từ DomainsByDay");
			_domainNames.Clear();
			try
			{
				/*Kiểm tra folder ngày xem có chưa*/
				string folderToday = DateTime.Now.ToString("yyyyMMdd");
				string pathFolderToday = Directory.GetCurrentDirectory() + @"\Download\" + folderToday;
				if (!Directory.Exists(pathFolderToday))
				{
					Directory.CreateDirectory(pathFolderToday);
				}
				file = new FileDownload();
				file.Title = GetContent();
				//Url
				file.LinkDownload = GetUrl();
				file.Origin = _xPath.Name;
				file.HomePage = _xPath.HomePage;
				file.Query = _xPath.Value;
				file.Type = _xPath.Id;
				file.FileName = file.Type+"_"+file.Title+".txt";
				file.LocalPath = pathFolderToday + @"\" +DateTime.Now.ToString("ddMMHHmmss_")+ file.FileName;

				var paging=GetPaging(file.LinkDownload);
				if(paging!=null)
				{
					paging.NextLinkTemplate = file.HomePage + @"/" + file.Title + @"/domains-{0}.html";
				}

				//List<Task> tasks = new List<Task>();

				WriteMaxProgressHandle(paging.TotalPage+1);
				WriteLogHandle("Thực hiện get domain từng trang...");
				Parallel.For(0, paging.TotalPage+1,
					new ParallelOptions() { MaxDegreeOfParallelism = 3 }, 
					i => 
					{
							string url = "";
							if (i == 0)
							{
								url = paging.From;
							}
							else
							{
								url = paging.NextLinkTemplate.Replace("{0}", i.ToString());
							}
							//tasks.Add(Task.Factory.StartNew(() =>
							//{
								string content = "";
								using (MyWebClient webClient = new MyWebClient())
								{
									//webClient.DownloadStringCompleted += DownloadStringCompleted;
									content = webClient.DownloadString(new Uri(url));
								}
								DownloadStringCompleted(content);
								//
							//}));
					});

				//Task.WaitAll(tasks.ToArray());
				WriteLogHandle("Get được tổng cộng " + _domainNames.Count + " domains .com");
				if (_domainNames.Count > 0)
				{
					File.WriteAllLines(file.LocalPath, _domainNames);

					if (InsertFileDownload(file))
					{
						WriteLogHandle("File Save as: " + file.LocalPath);
					}
					else
					{
						WriteLogHandle("Insert file thất bại");
					}
				}
				WriteLogHandle("Kết thúc get domain.");
			}
			catch(Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi get domain: "+ex.Message);
			}
		}
		/// <summary>
		/// Lấy tất cả domain có trong trang
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void DownloadStringCompleted(string content)
		{
			try
			{
				//if (e.Cancelled == false && e.Error == null)
				//{
				HtmlDocument html = new HtmlDocument();
				html.LoadHtml(content);

				var nodes = html.DocumentNode.SelectNodes(_xPath.ValueDomainName);
				//int count = nodes.Count;
				//Parallel.ForEach(nodes, domain =>
				//{
					
				//});
				foreach (var domain in nodes)
				{
					string name = domain.InnerText.TrimEnd().TrimStart();

					if (name.ToLower().EndsWith(".com") && name.Length > 4)
					{
						_domainNames.Add(name);

					}
				}
				//}
				//WriteLogHandle("Get được " + count + " domains từ page");
				WriteInsertPropressHandle();
			}
			catch (Exception ex)
			{
				WriteLogHandle("phát sinh lỗi từ trang: " + ex.Message);
			}
		}

		public List<string> GetAllDomainIn(string content)
		{
			List<string> list = new List<string>();
			try
			{
				HtmlDocument html = new HtmlDocument();
				html.LoadHtml(content);

				var nodes = html.DocumentNode.SelectNodes(_xPath.ValueDomainName);
				//int count = nodes.Count;
				//Parallel.ForEach(nodes, domain =>
				//{

				//});
				foreach (var domain in nodes)
				{
					string name = domain.InnerText.TrimEnd().TrimStart();

					if (name.ToLower().EndsWith(".com") && name.Length > 4)
					{
						list.Add(name);

					}
				}
				return list;
			}
			catch (Exception ex)
			{
				//WriteLogHandle("Error in get domain name. " + ex.Message);
				return list;
			}
		}

		/// <summary>
		/// Lấy phân trang
		/// </summary>
		/// <param name="Url"></param>
		/// <returns></returns>
		public PagingDomainsByDay GetPaging(string Url)
		{
			try
			{
				PagingDomainsByDay paging = new PagingDomainsByDay(Url);
				//WriteLogHandle("Get all page in link: " + Url);
				HtmlDocument html = new HtmlDocument();
				using (MyWebClient webClient = new MyWebClient())
				{
					string s = webClient.DownloadString(Url);
					html.LoadHtml(s);
				}

				var nodePageEnd = html.DocumentNode.SelectNodes(_xPath.ValueGetEndPage);
				paging.End = nodePageEnd.FirstOrDefault().Attributes["href"].Value;
				paging.NextLinkTemplate = _xPath.HomePage+Regex.Replace(paging.End, @"(-)([\d]+)(.html)$", "$1{0}$3");
				paging.TotalPage=int.Parse(Regex.Match(paging.End, @"-([\d]+).html$").Groups[1].Value);
				//WriteLogHandle(string.Format("Total:{0} pages.",paging.TotalPage));
				return paging;
			}
			catch
			{
				return null;
			}
		}

		public PagingDomainsByDay GetPaging()
		{
			try
			{
				PagingDomainsByDay paging = new PagingDomainsByDay(_xPath.LinkDownload);
				string source=_webClient.DownloadString(new Uri(_xPath.LinkDownload));
				if(source!=null)
				{
					PartnerHtmlDocument.LoadHtml(source);
					var nodePageEnd = PartnerHtmlDocument.DocumentNode.SelectNodes(_xPath.ValueGetEndPage);
					paging.End = nodePageEnd.FirstOrDefault().Attributes["href"].Value;
					paging.TotalPage = int.Parse(Regex.Match(paging.End, @"/([\d]+).html$").Groups[1].Value);
					paging.NextLinkTemplate = _xPath.HomePage + Regex.Replace(paging.End, @"(/)([\d]+)(.html)$", "$1{0}$3");
					WriteLogHandle(string.Format("Total:{0} pages.", paging.TotalPage));
				}
				else
				{
					WriteLogHandle("Partner not found");
				}
				return paging;
			}
			catch (Exception ex)
			{
				WriteLogHandle("Download HTML error: " + ex.Message);
				return null;
			}
		}
		/// <summary>
		/// Get tất ca ngày trong page đó
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public Dictionary<string, string> GetAllLinkDownloadDomainFrom(string url)
		{
			try
			{
				Dictionary<string, string> list = new Dictionary<string, string>();
				WriteLogHandle(string.Format("Download link: {0}", url));
				HtmlDocument sourceHTML = new HtmlDocument();
				string sourceStr="";
				using(MyWebClient client=new MyWebClient())
				{
					sourceStr=client.DownloadString(url);
					if(!string.IsNullOrEmpty(sourceStr))
					{
						sourceHTML.LoadHtml(sourceStr);
					}
					WriteLogHandle(sourceStr);
				}

				/*Lấy tất cả ngày có trong trang đó*/
				var nodeDate = sourceHTML.DocumentNode.SelectNodes(_xPath.Value.Replace("{0}",""));
				if(nodeDate!=null)
				{
					WriteLogHandle(string.Format("Total {0} link download in this page.",nodeDate.Count));

					/*duyệt tất cả các link*/
					foreach (var linkDate in nodeDate)
					{
						string title = linkDate.InnerText.TrimEnd().TrimStart();
						string link = string.Format("{0}{1}", _xPath.HomePage, linkDate.Attributes["href"].Value.TrimEnd().TrimStart());
						list.Add(title, link);
					}
				}
				return list;
			}
			catch (Exception ex)
			{
				WriteLogHandle("Download HTML lỗi: " + ex.Message);
				return null;
			}
		}

		public bool CreateAndInsertFile(List<string> listDomains,string title,string linkpage)
		{
			try
			{
				/*Kiểm tra folder ngày xem có chưa*/
				string folderToday = DateTime.Now.ToString("yyyyMMdd");
				string pathFolderToday = Directory.GetCurrentDirectory() + @"\Download\DataOnPartner";
				if (!Directory.Exists(pathFolderToday))
				{
					Directory.CreateDirectory(pathFolderToday);
				}

				var file = new FileDownload();
				file.Title = title;
				file.LinkDownload = linkpage;
				file.Origin = _xPath.Name;
				file.HomePage = _xPath.HomePage;
				file.Query = "";
				file.Type = "DownloadAll";
				file.FileName = string.Format("DownloadAll_{0}_{1}.txt", DateTime.Now.ToString("yyyyMMdd_HHmmss"), title);
				file.LocalPath = pathFolderToday + @"\" + file.FileName;
				File.WriteAllLines(file.LocalPath, listDomains);
				if (InsertFileDownload(file))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch
			{
				return false;
			}

		}
	}

	public class PagingDomainsByDay
	{
		public string NextLinkTemplate { get; set; }
		public string From { get; set; }
		public string End { get; set; }
		public int TotalPage { get; set; }
		public PagingDomainsByDay(string from)
		{
			From = from;
		}
	}
}
