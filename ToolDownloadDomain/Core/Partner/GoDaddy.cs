﻿using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Utility;

namespace Core.Partner
{
	public class GoDaddy:Partner
	{
		public GoDaddy(string id):base(id)
		{
			_webClient.Credentials = new NetworkCredential(_xPath.UserName, _xPath.Password);
		}

		private void DownloadDataCompleted(object sender, System.Net.DownloadDataCompletedEventArgs e)
		{
			
		}


		public override void DownloadFiles()
		{
			WriteLogHandle("Thực hiện download file từ FTP.");
			//// The serverUri parameter should start with the ftp:// scheme.
			//if (serverUri.Scheme != Uri.UriSchemeFtp)
			//{
			//	return false;
			//}
			// Get the object used to communicate with the server.

			// This example assumes the FTP site uses anonymous logon.
			
			//try
			//{
			//	//_webClient.DownloadDataAsync(new Uri(_xPath.LinkDownload));
			//	_webClient.DownloadFileAsync(new Uri(_xPath.LinkDownload),)
			//}
			//catch (WebException e)
			//{
			//	WriteLogHandle("Download file gặp lỗi. " + e.Message);
			//}

			try
			{
				/*Kiểm tra folder ngày xem có chưa*/
				string folderToday = DateTime.Now.ToString("yyyyMMdd");
				string pathFolderToday = Directory.GetCurrentDirectory() + @"\Download\" + folderToday;
				if (!Directory.Exists(pathFolderToday))
				{
					Directory.CreateDirectory(pathFolderToday);
				}
				file = new FileDownload();
				file.Title = "tdnam_all_listings";
				//Url
				file.LinkDownload = _xPath.LinkDownload;
				file.Origin = _xPath.Name;
				file.HomePage = _xPath.HomePage;
				file.Query = _xPath.Value;
				file.Type = _xPath.Id;
				//file name saved
				Regex regex = new Regex(_xPath.HomePage+@"[/]([\w.-]{1,})$");
				var match = regex.Match(file.LinkDownload);
				if (match.Success)
				{
					file.FileName = match.Groups[1].Value;
					file.LocalPath = pathFolderToday + @"\" +DateTime.Now.ToString("ddMMHHmmss_")+ file.FileName;
					WriteLogHandle("Start download: " + file.LinkDownload);
					//_webClient.DownloadFileAsync(new System.Uri(file.LinkDownload), file.LocalPath);
					using (var client = new WebClient())
					{
						client.Credentials = new NetworkCredential(_xPath.UserName, _xPath.Password);
						client.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler((sender, e) => DownLoadCompleted(sender, e, file));
						client.DownloadProgressChanged += new DownloadProgressChangedEventHandler((sender, e) => DownLoading(sender, e, file));
						try
						{
							client.DownloadFileAsync(new System.Uri(file.LinkDownload), file.LocalPath);
						}
						catch (Exception ex)
						{
							WriteLogHandle("Khong the download file."+ex.Message);
						}
					}
				}
			}
			catch
			{
				WriteLogHandle("Không tìm thấy file để download");
			}
		}

		public override void DownLoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e, FileDownload ob)
		{
			try
			{
				if (!e.Cancelled && e.Error == null)
				{
					WriteLogHandle(ob.Type + " Download Completed: " + ob.LocalPath);
					string pathFinish = "";
					if (UnFileRar(ob.LocalPath, out pathFinish))
					{
						/*Lấy tên file đã giải nén*/
						string[] fileArray = Directory.GetFiles(pathFinish);

						ob.LocalPath = fileArray[0];

						if (InsertFileDownload(ob))
						{
							WriteLogHandle("Insert file thành công");
						}
						else
						{
							WriteLogHandle("Insert file thất bại");
						}
						
					}
					else
					{
						WriteLogHandle("Không insert file do giải nén thất bại");
					}
				}
				else
				{
					WriteLogHandle("Download file bị dừng."+e.Error.Message);
				}
				WriteLogHandle("Kết thúc download file");
				///Sau khi insert file xong thì thực hiện đọc file luôn
				//ReadFileDownload();
			}
			catch (Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi chuẩn bị insert file. " + ex.Message);
			}
		}
		public override void ReadFileDownload()
		{
			WebIsBusy = true;
			WriteLogHandle("Thực hiện đọc file");
			Stopwatch watch = new Stopwatch();
			watch.Start();
			WriteProgressHandle(0);
			FileDownload fileDownload = GetFileDownload();
			try
			{
				if (fileDownload != null)
				{
					WriteLogHandle("Lấy được 1 file Pending chưa đọc: " + fileDownload.LocalPath);
					string filecontent = "";
					//using (StreamReader reader = new StreamReader(fileDownload.LocalPath))
					//{
					//	filecontent = reader.ReadToEnd();
					//}
					filecontent = File.ReadAllText(fileDownload.LocalPath);

					List<string> rows = Regex.Split(filecontent, @"[\r\n]+").ToList();

					WriteLogHandle("File có tất cả: " + rows.Count + " records");


				}
				else
				{
					WriteLogHandle("Không có file Pending nào chưa được đọc.");
				}
			}
			catch (Exception ex)
			{
				WriteLogHandle("Đọc file bị lỗi. " + ex.Message);
				fileDownload.Status = FileDownloadStatus.Read_Error;
				if (UpdateFileDownload(fileDownload))
				{
					WriteLogHandle("Update status file success");
				}
				else
				{
					WriteLogHandle("Update status file fail");
				}
				WebIsBusy = false;
				watch.Stop();
				WriteLogHandle("Kết thúc đọc file, Total: " + watch.Elapsed.TotalMinutes + " minutes");
			}
		}
	}
}
