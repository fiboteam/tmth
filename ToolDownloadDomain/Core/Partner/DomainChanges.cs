﻿using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Utility;

namespace Core.Partner
{
	public class DomainChanges : Partner
	{
		public DomainChanges(string id)
			: base(id)
		{

		}

		public override void DownLoadHtml()
		{
			try
			{
				//_webClient.DownloadStringAsync(new Uri(_xPath.LinkDownload));
				if(_xPath.DNS!=null)
				{
					string date=DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd");
					foreach (var dns in _xPath.DNS)
					{
						string url = string.Format(_xPath.LinkDownload, dns, date);
						_webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
						var source = _webClient.DownloadString(new Uri(url));
						if (!string.IsNullOrEmpty(source))
						{
							PartnerHtmlDocument.LoadHtml(source);
							WriteLogHandle("DNS: "+url);
							file = new FileDownload();
							file.Type = dns;
							Thread.Sleep(60000);
							DownloadFiles();
							Thread.Sleep(180000);
						}
						else
						{
							WriteLogHandle("Không vào được web của partner");
						}
					}
					
				}
				else
				{
					WriteLogHandle("Không tìm thấy DNS nào cần download.");
				}
				
			}
			catch (Exception ex)
			{
				WriteLogHandle("Download HTML lỗi: " + ex.Message);
			}
		}

		public override void DownloadFiles()
		{
			try
			{
				/*Kiểm tra folder ngày xem có chưa*/
				string folderToday = DateTime.Now.ToString("yyyyMMdd");
				string pathFolderToday = Directory.GetCurrentDirectory() + @"\Download\" + folderToday;
				if (!Directory.Exists(pathFolderToday))
				{
					Directory.CreateDirectory(pathFolderToday);
				}
				//file = new FileDownload();
				file.Title = GetContent();
				//Url
				file.LinkDownload = GetUrl();
				file.Origin = _xPath.Name;
				file.HomePage = _xPath.HomePage;
				file.Query = _xPath.Value;
				//file.Type = _xPath.Id;
				//file name saved
				Regex regex = new Regex(@"/([\w.-]{1,})$");
				var match = regex.Match(file.LinkDownload);
				if (match.Success)
				{
					file.FileName = match.Groups[1].Value;
					file.LocalPath = pathFolderToday + @"\" + DateTime.Now.ToString("ddMMHHmmss_") + file.FileName;
					WriteLogHandle("Start download: " + file.LinkDownload);
					//using (WebClient wc = new WebClient())
					//{
					//_webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler((sender, e) => DownLoading(sender, e, file));
					//_webClient.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler((sender, e) => DownLoadCompleted(sender, e, file));
					//_autoResetEvent.Reset();
					//_webClient.DownloadFileAsync(new System.Uri(file.LinkDownload), file.LocalPath);
					_webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
					_webClient.DownloadFile(new System.Uri(file.LinkDownload), file.LocalPath);
					//}
					//_autoResetEvent.WaitOne();
					WriteLogHandle("Download completed: " + file.LinkDownload);
				}
			}
			catch(Exception ex)
			{
				WriteLogHandle("Không tìm thấy file để download: "+ex.Message);
			}
		}

		

		public override void DownLoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e, DTO.Objects.FileDownload ob)
		{
			try
			{
				
				if (!e.Cancelled && e.Error == null)
				{
					WriteLogHandle(ob.Type + " Download Completed: " + ob.LocalPath);


					//if (PrepareFileAfterDownload(ob.LocalPath, 2))
					//{
					//	if (InsertFileDownload(ob))
					//	{
					//		WriteLogHandle("Insert file thành công");
					//	}
					//	else
					//	{
					//		WriteLogHandle("Insert file thất bại");
					//	}
					//}
					//else
					//{
					//	WriteLogHandle("Prepare file thất bại, không thực hiện insert");
					//}
					
				}
				else
				{
					WriteLogHandle("Download file đã bị stop. "+e.Error);
				}
				WriteLogHandle("Kết thúc download file");
				///Sau khi insert file xong thì thực hiện đọc file luôn
				//ReadFileDownload();
				
			}
			catch (Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi chuẩn bị insert file. " + ex.Message);
			}
			
		}
		public override bool PrepareFileAfterDownload(string path, int rowstart)
		{
			try
			{
				WriteLogHandle("Format lại file.");
				/*Đọc file*/
				string text = File.ReadAllText(path);
				var mathches = Regex.Matches(text, @"\r\n");
				string main = text.Substring(mathches[rowstart].Index + 2);
				main = Regex.Replace(main, @"[ \t]+", "\t");
				main = Regex.Replace(main, @"[,]+", "");
				File.WriteAllText(path, main);
				WriteLogHandle("Kết thúc format lại file.");
				return true;
			}
			catch (Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi format file. " + ex.Message);
				return false;
			}
		}

		public override void ReadFileDownload()
		{
			WebIsBusy = true;
			WriteLogHandle("Thực hiện đọc file");
			Stopwatch watch = new Stopwatch();
			watch.Start();
			WriteProgressHandle(0);
			FileDownload fileDownload = GetFileDownload();
			try
			{
				if (fileDownload != null)
				{
					WriteLogHandle("Lấy được 1 file Pending chưa đọc: " + fileDownload.LocalPath);
					string filecontent = "";
					using (StreamReader reader = new StreamReader(fileDownload.LocalPath))
					{
						filecontent = reader.ReadToEnd();
					}
					List<string> rows = Regex.Split(filecontent, @"[\r\n]+").ToList();

					WriteLogHandle("File có tất cả: " + rows.Count + " records");
				}
				else
				{
					WriteLogHandle("Không có file Pending nào chưa được đọc.");
				}
			}
			catch (Exception ex)
			{
				WriteLogHandle("Đọc file bị lỗi. " + ex.Message);
				fileDownload.Status = FileDownloadStatus.Read_Error;
				if (UpdateFileDownload(fileDownload))
				{
					WriteLogHandle("Update status file success");
				}
				else
				{
					WriteLogHandle("Update status file fail");
				}
				WebIsBusy = false;
				watch.Stop();
				WriteLogHandle("Kết thúc đọc file, Total: " + watch.Elapsed.TotalMinutes + " minutes");
			}
		}
	}
}
