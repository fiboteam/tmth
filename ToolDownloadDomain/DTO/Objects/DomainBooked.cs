using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace DTO.Objects
{
    public class DomainBooked : ObjectBase
    {

		public DomainBooked():base(){
			DomainBookedStatus = DomainBookedStatus.New;
			DomainBookedName = "";
			DepositPrice = 0;
			CustomerInfomation = "";
			//CaptureDate = null;
			//ExpireDate = null;
		}

		[DisplayName("Domain Name")]
		public string DomainBookedName { get; set; }

		[Browsable(false)]
		public long SaleManId { get; set; }

		[DisplayName("Giá đặt trước")]
		public decimal DepositPrice { get; set; }

		[DisplayName("Thông tin khách hàng")]
		public string CustomerInfomation { get; set; }

		[Browsable(false)]
		public string Note { get; set; }

		[Browsable(false)]
		public string Remark { get; set; }

		[Browsable(false)]
		public long DomainIdRelated { get; set; }

		[Browsable(false)]
		public string TableRelated { get; set; }

		[DisplayName("Capture Date")]
		public DateTime ? CaptureDate { get; set; }

		[DisplayName("Expire Date")]
		public DateTime ? ExpireDate { get; set; }

		[DisplayName("Trạng thái")]
		public DomainBookedStatus DomainBookedStatus { get; set; }

		[Browsable(false)]
		public int ShortCreatedDate { get; set; }
		[DisplayName("Sale")]
		public string SaleName { get; set; }

    }
}