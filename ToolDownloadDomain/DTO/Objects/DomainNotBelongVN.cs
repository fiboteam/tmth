using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace DTO.Objects
{
    [DataContract]
    public class DomainNotBelongVN : ObjectBase
    {

		public DomainNotBelongVN():base(){

		}

		[DataMember]
		public string DomainName { get; set; } 

		[DataMember]
		public string Origin { get; set; } 

		[DataMember]
		public string DomainType { get; set; } 

		[DataMember]
		public DateTime ? AuctionsDate { get; set; } 

		[DataMember]
		public int ShortCreatedDate { get; set; } 

		[DataMember]
		public bool ForSale { get; set; } 

		[DataMember]
		public string Note { get; set; } 


    }
}