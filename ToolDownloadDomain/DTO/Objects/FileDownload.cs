﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DTO.Objects
{
	public class FileDownload : ObjectBase
	{
		public string Origin { get; set; }
		public string Type { get; set; }
		public string Query { get; set; }
		public string HomePage { get; set; }
		public string LinkDownload { get; set; }
		public string FileName { get; set; }
		public string LocalPath { get; set; }
		public FileDownloadStatus Status { get; set; }
		public string Title { get; set; }
		public FileDownload()
		{
			Status = FileDownloadStatus.New;
		}
	}
}
