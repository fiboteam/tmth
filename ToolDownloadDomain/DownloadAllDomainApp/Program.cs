﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Partner;
using Core.Code;

namespace DownloadAllDomainApp
{
	class Program
	{
		private static DomainsByDay _domainByDay = new DomainsByDay("DomainsByDay");
		private static PagingDomainsByDay _paging;
		private static int posX = 0;
		private static int posY = 0;
		static void Main(string[] args)
		{
			Console.WindowWidth = 100;
			int code = 1;
			string name = string.Format("Application download all domain .com in {0}", ConfigurationManager.AppSettings["Partner"].ToString());

			_domainByDay.WriteLogEvent += WriteMessageLine;
			_paging = _domainByDay.GetPaging();
			while(code>0)
			{
				code=Menu(name);

				switch(code)
				{
					case 1:
						DownloadSinglePage();
						break;
					case 2:
						DownloadMultiPage();
						break;
				}
			}
			
			Console.ReadKey();
		}
		static void WriteMessageLine(string message)
		{
			Console.WriteLine(message);
			posX = Console.CursorLeft;
			posY = Console.CursorTop;
		}
		static void WriteMessage(string message)
		{
			Console.Write(message);
			posX = Console.CursorLeft;
			posY = Console.CursorTop;
		}
		static T ReadInput<T>()
		{
			T temp = default(T);
			try
			{
				return (T)Convert.ChangeType(Console.ReadLine(), typeof(T));
			}
			catch
			{
				return temp;
			}
		}
		static int Menu(string name)
		{
			WriteMessageLine("\n\n");
			WriteMessageLine(name);
			WriteMessageLine(string.Format("Total {0} page", _paging.TotalPage));
			WriteMessageLine("1.DownLoad Single Page");
			WriteMessageLine("2.DownLoad Multi Page");
			WriteMessageLine("0.Exit");
			WriteMessage("\tChoose: ");

			return ReadInput<int>();
		}
		static void DownloadSinglePage()
		{
			try
			{
				Console.Clear();
				WriteMessageLine(string.Format("Total {0} page", _paging.TotalPage));
				WriteMessage("Download page: ");
				int page = ReadInput<int>();
				string linkdownload = "";
				if (page >= 0 && page<=_paging.TotalPage)
				{
					if (page <= 0)
					{
						linkdownload = string.Format(_paging.From);
					}
					else
					{
						linkdownload = string.Format(_paging.NextLinkTemplate, page);
					}
				}
				else
				{
					WriteMessageLine("Page not found");
				}

				if (!string.IsNullOrEmpty(linkdownload))
				{
					DownloadAllDomainInPage(linkdownload, page);
				}
				else
				{
					WriteMessageLine(string.Format("Link not found"));
				}
			}
			catch(Exception ex)
			{
				WriteMessageLine(string.Format("Download Page {0} error: {1}", "",ex.Message));
			}
		}

		private static void WritePercent(int x, int y, int percent,int total)
		{
			int xbefore = Console.CursorLeft;
			int ybefore = Console.CursorTop;
			Console.SetCursorPosition(x, y);
			Console.Write(string.Format("\t[{0}/{1}][{2}%]", percent, total, Math.Round(((float)percent/(float)total)*100),2));
			Console.SetCursorPosition(posX, posY);
		}

		
		static void DownloadMultiPage()
		{
			try
			{
				Console.Clear();
				WriteMessageLine(string.Format("Total {0} page", _paging.TotalPage));
				WriteMessage("Download from page: ");
				int fromPage = ReadInput<int>();
				WriteMessage("Download to page: ");
				int toPage = ReadInput<int>();
				for (int i = fromPage; i <= toPage; i++)
				{
					
					string linkdownload = "";
					if (i >= 0 && i <= _paging.TotalPage)
					{

						if (i <= 0)
						{
							linkdownload = string.Format(_paging.From);
						}
						else
						{
							linkdownload = string.Format(_paging.NextLinkTemplate, i);
						}
					}
					else
					{
						WriteMessageLine("Page not found");
					}

					if(!string.IsNullOrEmpty(linkdownload))
					{
						DownloadAllDomainInPage(linkdownload, i);
					}
					else
					{
						WriteMessageLine(string.Format("Link not found"));
					}
					
				}
			}
			catch (Exception ex)
			{
				WriteMessageLine(string.Format("Download Page {0} error: {1}", "", ex.Message));
			}
		}

		static void DownloadAllDomainInPage(string linkdownload,int indexOfPage)
		{
			Stopwatch stop = new Stopwatch();
			stop.Start();
			List<string> listDomainName = new List<string>();
			Dictionary<string, string> listLink = null;

			/*lấy tất cả ngày trong page đó*/
			listLink = _domainByDay.GetAllLinkDownloadDomainFrom(linkdownload);

			if (listLink != null)
			{
				/*duyet cac ngay trong page đó*/
				for (int i = 0; i < listLink.Count; i++)
				//for (int i = 0; i < 1; i++)
				{
					string title = listLink.Keys.ElementAt(i);
					string link = listLink.Values.ElementAt(i);
					WriteMessage(string.Format("{2}. {0} link: {1}", title, link, i + 1));
					int x = Console.CursorLeft;
					int y = Console.CursorTop;
					WriteMessageLine("");
					var task = Task.Factory.StartNew<List<string>>(() =>
					{
						return DownloadDomain(i, title, link, x, y);
					}
					).ContinueWith(result =>
					{
						listDomainName.AddRange(result.Result);
					}
					);
					task.Wait();
				}
			}

			if (listDomainName.Count > 0)
			{
				_domainByDay.CreateAndInsertFile(listDomainName, indexOfPage.ToString(), linkdownload);
			}
			stop.Stop();
			WriteMessageLine(string.Format("Total {0} domains .com", listDomainName.Count));
			WriteMessageLine(string.Format("Completed after: {0} minutes", stop.Elapsed.TotalMinutes));
		}
		private static List<string> DownloadDomain(int i, string title, string link, int x, int y)
		{
			List<string> domainNames = new List<string>();
			int currentPageCompleted = 0;
			/*Lấy tất cả trang */
			var pageGetDomain = _domainByDay.GetPaging(link);
			if (pageGetDomain != null)
			{
				/*duyệt từng trang*/
				Parallel.For(0, pageGetDomain.TotalPage + 1,
				new ParallelOptions() { MaxDegreeOfParallelism = 3 },
				p =>
				{
					try
					{
						string url = "";
						if (p == 0)
						{
							url = pageGetDomain.From;
						}
						else
						{
							url = pageGetDomain.NextLinkTemplate.Replace("{0}", p.ToString());
						}

						string content = "";
						using (MyWebClient webClient = new MyWebClient())
						{
							content = webClient.DownloadString(new Uri(url));
						}
						/*Lấy domain*/
						var listName = _domainByDay.GetAllDomainIn(content);

						if (listName.Count>0)
						{
							domainNames.AddRange(listName);
						}
						else
						{
							WriteMessageLine("Not found any domain name.");
						}
						currentPageCompleted += 1;
						WritePercent(x, y, currentPageCompleted, pageGetDomain.TotalPage + 1);

					}
					catch (Exception ex)
					{
						WriteMessageLine(string.Format("Task error {0}.", ex.Message));
					}
				});
			}
			else
			{
				WriteMessageLine(string.Format("Not found any page in link {0}.", link));
			}

			return domainNames;
		}
	}
}
