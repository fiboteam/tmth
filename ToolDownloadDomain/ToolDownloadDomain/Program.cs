﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolDownloadDomain
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			string namePartner = ConfigurationManager.AppSettings["Partner"].ToString();
			switch(namePartner)
			{
				case"SnapNames":
					Application.Run(new FrmSnapNames());
					break;
				//case"DomainsByDay":
				//	Application.Run(new FrmDomainsByDay());
				//	break;
				default:
				case"NameJet":
					Application.Run(new FrmMain());
					break;
			}
			
		}
	}
}
