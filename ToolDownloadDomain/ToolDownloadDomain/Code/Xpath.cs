﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ToolDownloadDomain
{
	public class Xpath
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public string Value { get; set; }
		public string HomePage { get; set; }
		public string LinkDownload { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public List<string> DNS { get; set; }
	}
}
