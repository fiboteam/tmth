﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Utility;

namespace ToolDownloadDomain
{
	public partial class FrmMain : Form
	{
		private readonly NameJetPreRelease _NameJetPreRelease = new NameJetPreRelease("Pre-Release");
		private readonly NameJetPending _NameJetPending = new NameJetPending("Pending-Delete");
		private readonly NameJetAuction _NameJetAuction = new NameJetAuction("Auctions-Listings");
		private readonly GoDaddy _GoDaddy = new GoDaddy("AllListing");
		private readonly DomainsByDay _DomainsByDay = new DomainsByDay("DomainsByDay");
		private readonly DomainChanges_v2 _DomainChanges = new DomainChanges_v2("DomainChanges");

		public delegate void WriteProgressBarDelegate(ProgressBar bar, int percent);
		private WriteProgressBarDelegate _WriteProgressBarDelegate;

		public delegate void WriteTextViewDelegate(RichTextBox rtxt, string text);
		private WriteTextViewDelegate _WriteTextViewDelegate;

		public delegate void WriteMaximumProgressbarwDelegate(ProgressBar bar, int max);
		private WriteMaximumProgressbarwDelegate _WriteMaximumProgressbarwDelegate;

		public delegate void WriteInsertProgressBarDelegate(ProgressBar bar);
		private WriteInsertProgressBarDelegate _WriteInsertProgressBarDelegate;

		public FrmMain()
		{
			InitializeComponent();
			_NameJetPreRelease.WriteLogEvent += WriteLogPreToView;
			_NameJetPreRelease.WritePropressEvent += WriteProgressBarPreRelease;
			_NameJetPending.WriteLogEvent += WriteLogPendingToView;
			_NameJetPending.WritePropressEvent += WriteProgressBarPending;
			_NameJetAuction.WriteLogEvent += WriteLogAuctionToView;
			_NameJetAuction.WritePropressEvent += WriteProgressBarAuctions;
			_GoDaddy.WriteLogEvent += WriteLogGoDaddyToView;
			_GoDaddy.WritePropressEvent += WriteProgressBarGoDaddy;
			_DomainsByDay.WriteLogEvent += WriteLogDomainsByDayToView;
			_DomainsByDay.WritePropressEvent += WriteProgressBarDomainsByDay;
			_DomainsByDay.WriteInsertPropressEvent += WriteInsertProgressBarDomainsByDay;
			_DomainsByDay.WriteMaxPropressEvent += WriteMaxProgressBarDomainsByDay;
			_DomainChanges.WriteLogEvent += WriteLogDomainChangesToView;
			_DomainChanges.WritePropressEvent += WriteProgressBarDomainChanges;

			_WriteProgressBarDelegate = WriteProgressBar;
			_WriteTextViewDelegate = WriteTextView;
			_WriteMaximumProgressbarwDelegate = WriteMaximumProgressbar;
			_WriteInsertProgressBarDelegate = WriteInsertProgressBar;

			//double minutes = 60;
			//double.TryParse(txtIntervalRefreshSEODomain.Text, out minutes);
			timerPre.Interval = (int)TimeSpan.FromMinutes(txtIntervalPre.Text.AsDouble(60)).TotalMilliseconds;
			timerPending.Interval = (int)TimeSpan.FromMinutes(txtIntervalPending.Text.AsDouble(60)).TotalMilliseconds;
			timerAuction.Interval = (int)TimeSpan.FromMinutes(txtIntervalAuction.Text.AsDouble(60)).TotalMilliseconds;
			timerGoDaddy.Interval = (int)TimeSpan.FromMinutes(txtIntervalGoDaddy.Text.AsDouble(60)).TotalMilliseconds;
			timerDomainsByDay.Interval = (int)TimeSpan.FromMinutes(txtIntervalDomainsByDay.Text.AsDouble(60)).TotalMilliseconds;
			timerDomainChanges.Interval = (int)TimeSpan.FromMinutes(txtIntervalDomainChanges.Text.AsDouble(60)).TotalMilliseconds;

			timerAuction.Enabled = false;
			timerPending.Enabled = false;
			timerPre.Enabled = false;
			timerGoDaddy.Enabled = false;
			timerDomainsByDay.Enabled = false;
			timerDomainChanges.Enabled = false;
		}

		private void WriteProgressBarDomainChanges(int percent)
		{
			this.Invoke(_WriteProgressBarDelegate, new object[] { pgbDomainChanges, percent });
		}

		private void WriteLogDomainChangesToView(string Message)
		{
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtDomainChanges, Message });
		}

		private void WriteMaxProgressBarDomainsByDay(int max)
		{
			this.Invoke(_WriteMaximumProgressbarwDelegate, new object[] { pgbDomainsByDay, max });
		}

		private void WriteInsertProgressBarDomainsByDay()
		{
			this.Invoke(_WriteInsertProgressBarDelegate, new object[] { pgbDomainsByDay });
		}

		private void WriteProgressBarDomainsByDay(int percent)
		{
			this.Invoke(_WriteProgressBarDelegate, new object[] { pgbDomainsByDay, percent });
		}

		private void WriteLogDomainsByDayToView(string Message)
		{
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtDomainsByDay, Message });
		}

		private void WriteProgressBarGoDaddy(int percent)
		{
			this.Invoke(_WriteProgressBarDelegate, new object[] { pgbGoDaddy, percent });
		}

		private void WriteLogGoDaddyToView(string Message)
		{
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtGoDaddy, Message });
		}

		private void WriteTextView(RichTextBox rtxt, string text)
		{
			rtxt.AppendText(text + "\n\n");
		}

		private void WriteProgressBar(ProgressBar bar, int percent)
		{
			bar.Value = percent;
		}

		private void WriteInsertProgressBar(ProgressBar bar)
		{
			try
			{
				if (bar.Value!=bar.Maximum)
					bar.Value = bar.Value + 1;
			}
			catch
			{

			}
		}

		private void WriteMaximumProgressbar(ProgressBar bar, int max)
		{
			bar.Maximum = max;
		}

		private void WriteTextASync(RichTextBox rtxt, string message)
		{
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxt, message });
		}

		private void WriteLogPreToView(string Message)
		{
			//rtxtViewPreRelease.AppendText(Message+"\n");
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtViewPreRelease, Message });
			//_WriteTextViewDelegate.Invoke(rtxtViewPreRelease, Message);
		}
		private void WriteLogPendingToView(string Message)
		{
			//rtxtViewPending.AppendText(Message + "\n");
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtViewPending, Message });
			//_WriteTextViewDelegate.Invoke(rtxtViewPending, Message);
		}
		private void WriteLogAuctionToView(string Message)
		{
			//rtxtViewAuctions.AppendText(Message + "\n");
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtViewAuctions, Message });
			//_WriteTextViewDelegate.Invoke(rtxtViewAuctions, Message);
		}
		private void WriteProgressBarPreRelease(int percent)
		{
			//ppbPreRelease.Value = percent;
			this.BeginInvoke(_WriteProgressBarDelegate, new object[] { ppbPreRelease, percent });
			//_WriteProgressBarDelegate.Invoke(ppbPreRelease, percent);
		}
		private void WriteProgressBarPending(int percent)
		{
			//ppbPending.Value = percent;
			this.BeginInvoke(_WriteProgressBarDelegate, new object[] { ppbPending, percent });
			//_WriteProgressBarDelegate.Invoke(ppbPending, percent);
		}
		private void WriteProgressBarAuctions(int percent)
		{
			//ppbAuctions.Value = percent;
			this.BeginInvoke(_WriteProgressBarDelegate, new object[] { ppbAuctions, percent });
			//_WriteProgressBarDelegate.Invoke(ppbAuctions, percent);
		}

		private void btnDownloadHTML_Click(object sender, EventArgs e)
		{
			//WriteProgressBarPreRelease(0);
			//_NameJetPreRelease.DownLoadHtml();

			if (!_NameJetPreRelease.WebIsBusy)
			{
				WriteProgressBarPreRelease(0);
				Task.Factory.StartNew(() => { _NameJetPreRelease.DownLoadHtml(); });
			}
		}

		private void btnStartDownloadPending_Click(object sender, EventArgs e)
		{
			//WriteProgressBarPending(0);
			//_NameJetPending.DownLoadHtml();

			if (!_NameJetPending.WebIsBusy)
			{
				WriteProgressBarPending(0);
				Task.Factory.StartNew(() => { _NameJetPending.DownLoadHtml(); });
			}
		}

		private void btnStartDowloadAuctions_Click(object sender, EventArgs e)
		{
			//WriteProgressBarAuctions(0);
			//_NameJetAuction.DownLoadHtml();

			if (!_NameJetAuction.WebIsBusy)
			{
				WriteProgressBarAuctions(0);
				Task.Factory.StartNew(() => { _NameJetAuction.DownLoadHtml(); });
			}
		}

		private void FrmMain_Load(object sender, EventArgs e)
		{

		}

		private void btnReadFilePreRelease_Click(object sender, EventArgs e)
		{
			_NameJetPreRelease.ReadFileDownload();
		}

		private void bgwPre_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_NameJetPreRelease.WebIsBusy)
			{
				WriteTextASync(rtxtViewPreRelease, "Thực hiện download file");
				WriteProgressBarPreRelease(0);
				_NameJetPreRelease.DownLoadHtml();
			}
			else
			{
				WriteTextASync(rtxtViewPreRelease, "Downloading...");
			}
		}

		private void bgwPending_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_NameJetPending.WebIsBusy)
			{
				WriteTextASync(rtxtViewPending, "Thực hiện download file");
				WriteProgressBarPending(0);
				_NameJetPending.DownLoadHtml();
			}
			else
			{
				WriteTextASync(rtxtViewPending, "Downloading...");
			}
		}

		private void bgwAuctions_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_NameJetAuction.WebIsBusy)
			{
				WriteTextASync(rtxtViewAuctions, "Thực hiện download file");
				WriteProgressBarAuctions(0);
				_NameJetAuction.DownLoadHtml();
			}
			else
			{
				WriteTextASync(rtxtViewAuctions, "Downloading...");
			}
		}

		private void timerPre_Tick(object sender, EventArgs e)
		{
			if(!bgwPre.IsBusy)
			{
				bgwPre.RunWorkerAsync();
			}
		}

		private void timerPending_Tick(object sender, EventArgs e)
		{
			if (!bgwPending.IsBusy)
			{
				bgwPending.RunWorkerAsync();
			}
		}

		private void timerAuction_Tick(object sender, EventArgs e)
		{
			if (!bgwAuctions.IsBusy)
			{
				bgwAuctions.RunWorkerAsync();
			}
		}

		private void btnStartPre_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewPreRelease, "Start Download domain Pre-Release");
			timerPre.Enabled = true;
		}

		private void btnStopPre_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewPreRelease, "Stop Download domain Pre-Release");
			timerPre.Enabled = false;
		}

		private void btnStartPending_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewPending, "Start Download domain Pending");
			timerPending.Enabled = true;
		}

		private void btnStopPending_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewPending, "Stop Download domain Pending");
			timerPending.Enabled = false;
		}

		private void btnStartAuctions_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewAuctions, "Start Download domain Auctions");
			timerAuction.Enabled = true;
		}

		private void btnStopAuctions_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewAuctions, "Stop Download domain Auctions");
			timerAuction.Enabled = false;
		}

		private void button4_Click(object sender, EventArgs e)
		{
			_NameJetPending.ReadFileDownload();
		}

		private void btnSavePre_Click(object sender, EventArgs e)
		{
			timerPre.Interval = (int)TimeSpan.FromMinutes(txtIntervalPre.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnSavePending_Click(object sender, EventArgs e)
		{
			timerPending.Interval = (int)TimeSpan.FromMinutes(txtIntervalPending.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnSaveAuctions_Click(object sender, EventArgs e)
		{
			timerAuction.Interval = (int)TimeSpan.FromMinutes(txtIntervalAuction.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnReadTestAuctions_Click(object sender, EventArgs e)
		{
			_NameJetAuction.ReadFileDownload();
		}

		private void btnStartGodaddy_Click(object sender, EventArgs e)
		{
			//FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://www.contoso.com/test.htm");
			//request.Method = WebRequestMethods.Ftp.DownloadFile;
			//// This example assumes the FTP site uses anonymous logon.
			//request.Credentials = new NetworkCredential("anonymous", "janeDoe@contoso.com");
			////FtpClient
			//FtpWebResponse response = (FtpWebResponse)request.GetResponse();

			//Stream responseStream = response.GetResponseStream();
			//StreamReader reader = new StreamReader(responseStream);
			//Console.WriteLine(reader.ReadToEnd());

			//Console.WriteLine("Download Complete, status {0}", response.StatusDescription);

			//reader.Close();
			//response.Close();
			_WriteTextViewDelegate.Invoke(rtxtGoDaddy, "Start Download domain");
			timerGoDaddy.Enabled = true;
		}

		private void bgwGoDaddy_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_GoDaddy.WebIsBusy)
			{
				WriteTextASync(rtxtGoDaddy, "Thực hiện download file");
				WriteProgressBarGoDaddy(0);
				_GoDaddy.DownloadFiles();
			}
			else
			{
				WriteTextASync(rtxtGoDaddy, "Downloading...");
			}
		}

		private void timerGoDaddy_Tick(object sender, EventArgs e)
		{
			if (!bgwGoDaddy.IsBusy)
			{
				bgwGoDaddy.RunWorkerAsync();
			}
		}

		private void btnSaveGoDaddy_Click(object sender, EventArgs e)
		{
			timerGoDaddy.Interval = (int)TimeSpan.FromMinutes(txtIntervalGoDaddy.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnStopGoDaddy_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtGoDaddy, "Stop Download domain");
			timerGoDaddy.Enabled = false;
		}

		private void btnDownloadFileGoDaddy_Click(object sender, EventArgs e)
		{
			if(!_GoDaddy.WebIsBusy)
			{
				_GoDaddy.DownloadFiles();
			}
		}

		private void btnReadFileGoddy_Click(object sender, EventArgs e)
		{
			if (!_GoDaddy.WebIsBusy)
			{
				_GoDaddy.ReadFileDownload();
			}
		}

		private void btnDownloadDomainsByDay_Click(object sender, EventArgs e)
		{
			if (!_DomainsByDay.WebIsBusy)
			{
				WriteTextASync(rtxtDomainsByDay, "Thực hiện get domains");
				WriteProgressBarDomainsByDay(0);
				Task.Factory.StartNew(() => { _DomainsByDay.DownLoadHtml(); });
			}
		}

		private void btnReadFileDomainsByDay_Click(object sender, EventArgs e)
		{
			//var text=File.ReadAllText(@"F:\Hieu\Bitbucket\Hieu@fibo.vn\ToolDownloadDomain\ToolDownloadDomain\bin\Debug\Download\20160314\1403195142_DomainsByDay_2016-03-13.txt");
		}

		private void btnSaveDomainsByDay_Click(object sender, EventArgs e)
		{
			timerDomainsByDay.Interval = (int)TimeSpan.FromMinutes(txtIntervalDomainsByDay.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnStartDomainsByDay_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtDomainsByDay, "Start Download domain");
			timerDomainsByDay.Enabled = true;
		}

		private void btnStopDomainsByDay_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtDomainsByDay, "Stop Download domain");
			timerDomainsByDay.Enabled = false;
		}

		private void bgwDomainsByDay_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_DomainsByDay.WebIsBusy)
			{
				WriteTextASync(rtxtDomainsByDay, "Thực hiện get domains");
				WriteProgressBarDomainsByDay(0);
				_DomainsByDay.DownLoadHtml();
			}
			else
			{
				WriteTextASync(rtxtGoDaddy, "Downloading...");
			}
		}

		private void timerDomainsByDay_Tick(object sender, EventArgs e)
		{
			if (!bgwDomainsByDay.IsBusy)
			{
				bgwDomainsByDay.RunWorkerAsync();
			}
		}

		private void timerDomainChanges_Tick(object sender, EventArgs e)
		{
			if (!bgwDomainChanges.IsBusy)
			{
				bgwDomainChanges.RunWorkerAsync();
			}
		}

		private void bgwDomainChanges_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_DomainChanges.WebIsBusy)
			{
				WriteTextASync(rtxtDomainsByDay, "Thực hiện download file");
				WriteProgressBarDomainChanges(0);
				_DomainChanges.DownLoadHtml();
			}
			else
			{
				WriteTextASync(rtxtGoDaddy, "Downloading...");
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (!_DomainChanges.WebIsBusy)
			{
				WriteTextASync(rtxtDomainChanges, "Thực hiện download file");
				WriteProgressBarDomainChanges(0);
				Task.Factory.StartNew(() => { _DomainChanges.DownLoadHtml(); });
			}
		}

		private void btnReadFileDomainChanges_Click(object sender, EventArgs e)
		{
			WriteTextASync(rtxtDomainChanges, "Thực hiện đọc file");
			WriteProgressBarDomainChanges(0);
			Task.Factory.StartNew(() => { _DomainChanges.ReadFileDownload(); });
		}

		private void btnSaveDomainChanges_Click(object sender, EventArgs e)
		{
			timerDomainChanges.Interval = (int)TimeSpan.FromMinutes(txtIntervalDomainChanges.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnStartDomainChanges_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtDomainChanges, "Start Download domain");
			timerDomainChanges.Enabled = true;
		}

		private void btnStopDomainChanges_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtDomainChanges, "Stop Download domain");
			timerDomainChanges.Enabled = false;
		}

		private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			_DomainChanges.Close();
		}

	}
}
