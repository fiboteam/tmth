﻿namespace ToolDownloadDomain
{
	partial class FrmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnStartDownloadRelease = new System.Windows.Forms.Button();
			this.rtxtViewPreRelease = new System.Windows.Forms.RichTextBox();
			this.splitContainerMain = new System.Windows.Forms.SplitContainer();
			this.splitContainerLeft = new System.Windows.Forms.SplitContainer();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnSavePre = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.txtIntervalPre = new System.Windows.Forms.TextBox();
			this.btnStopPre = new System.Windows.Forms.Button();
			this.btnReadTestPreRelease = new System.Windows.Forms.Button();
			this.btnStartPre = new System.Windows.Forms.Button();
			this.ppbPreRelease = new System.Windows.Forms.ProgressBar();
			this.label1 = new System.Windows.Forms.Label();
			this.splitContainerRight = new System.Windows.Forms.SplitContainer();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.btnSavePending = new System.Windows.Forms.Button();
			this.btnStartDownloadPending = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.txtIntervalPending = new System.Windows.Forms.TextBox();
			this.btnStopPending = new System.Windows.Forms.Button();
			this.btnReadTestPending = new System.Windows.Forms.Button();
			this.btnStartPending = new System.Windows.Forms.Button();
			this.ppbPending = new System.Windows.Forms.ProgressBar();
			this.rtxtViewPending = new System.Windows.Forms.RichTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.btnSaveAuctions = new System.Windows.Forms.Button();
			this.btnStartDowloadAuctions = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.txtIntervalAuction = new System.Windows.Forms.TextBox();
			this.btnStopAuctions = new System.Windows.Forms.Button();
			this.btnReadTestAuctions = new System.Windows.Forms.Button();
			this.btnStartAuctions = new System.Windows.Forms.Button();
			this.rtxtViewAuctions = new System.Windows.Forms.RichTextBox();
			this.ppbAuctions = new System.Windows.Forms.ProgressBar();
			this.label3 = new System.Windows.Forms.Label();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.btnReadFileGoddy = new System.Windows.Forms.Button();
			this.btnDownloadFileGoDaddy = new System.Windows.Forms.Button();
			this.label8 = new System.Windows.Forms.Label();
			this.btnStopGoDaddy = new System.Windows.Forms.Button();
			this.btnStartGodaddy = new System.Windows.Forms.Button();
			this.pgbGoDaddy = new System.Windows.Forms.ProgressBar();
			this.rtxtGoDaddy = new System.Windows.Forms.RichTextBox();
			this.btnSaveGoDaddy = new System.Windows.Forms.Button();
			this.txtIntervalGoDaddy = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.btnReadFileDomainsByDay = new System.Windows.Forms.Button();
			this.rtxtDomainsByDay = new System.Windows.Forms.RichTextBox();
			this.btnDownloadDomainsByDay = new System.Windows.Forms.Button();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.txtIntervalDomainsByDay = new System.Windows.Forms.TextBox();
			this.btnStopDomainsByDay = new System.Windows.Forms.Button();
			this.btnSaveDomainsByDay = new System.Windows.Forms.Button();
			this.btnStartDomainsByDay = new System.Windows.Forms.Button();
			this.pgbDomainsByDay = new System.Windows.Forms.ProgressBar();
			this.btnReadFileDomainChanges = new System.Windows.Forms.Button();
			this.rtxtDomainChanges = new System.Windows.Forms.RichTextBox();
			this.pgbDomainChanges = new System.Windows.Forms.ProgressBar();
			this.btnDownloadDomainChanges = new System.Windows.Forms.Button();
			this.btnStartDomainChanges = new System.Windows.Forms.Button();
			this.label11 = new System.Windows.Forms.Label();
			this.btnSaveDomainChanges = new System.Windows.Forms.Button();
			this.label12 = new System.Windows.Forms.Label();
			this.btnStopDomainChanges = new System.Windows.Forms.Button();
			this.txtIntervalDomainChanges = new System.Windows.Forms.TextBox();
			this.bgwPre = new System.ComponentModel.BackgroundWorker();
			this.bgwPending = new System.ComponentModel.BackgroundWorker();
			this.bgwAuctions = new System.ComponentModel.BackgroundWorker();
			this.timerPre = new System.Windows.Forms.Timer(this.components);
			this.timerPending = new System.Windows.Forms.Timer(this.components);
			this.timerAuction = new System.Windows.Forms.Timer(this.components);
			this.bgwGoDaddy = new System.ComponentModel.BackgroundWorker();
			this.timerGoDaddy = new System.Windows.Forms.Timer(this.components);
			this.timerDomainsByDay = new System.Windows.Forms.Timer(this.components);
			this.bgwDomainsByDay = new System.ComponentModel.BackgroundWorker();
			this.timerDomainChanges = new System.Windows.Forms.Timer(this.components);
			this.bgwDomainChanges = new System.ComponentModel.BackgroundWorker();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
			this.splitContainerMain.Panel1.SuspendLayout();
			this.splitContainerMain.Panel2.SuspendLayout();
			this.splitContainerMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).BeginInit();
			this.splitContainerLeft.Panel1.SuspendLayout();
			this.splitContainerLeft.Panel2.SuspendLayout();
			this.splitContainerLeft.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerRight)).BeginInit();
			this.splitContainerRight.Panel1.SuspendLayout();
			this.splitContainerRight.Panel2.SuspendLayout();
			this.splitContainerRight.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnStartDownloadRelease
			// 
			this.btnStartDownloadRelease.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartDownloadRelease.Location = new System.Drawing.Point(3, 57);
			this.btnStartDownloadRelease.Name = "btnStartDownloadRelease";
			this.btnStartDownloadRelease.Size = new System.Drawing.Size(100, 23);
			this.btnStartDownloadRelease.TabIndex = 0;
			this.btnStartDownloadRelease.Text = "Download file";
			this.btnStartDownloadRelease.UseVisualStyleBackColor = true;
			this.btnStartDownloadRelease.Click += new System.EventHandler(this.btnDownloadHTML_Click);
			// 
			// rtxtViewPreRelease
			// 
			this.rtxtViewPreRelease.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtViewPreRelease.Location = new System.Drawing.Point(3, 25);
			this.rtxtViewPreRelease.Name = "rtxtViewPreRelease";
			this.rtxtViewPreRelease.Size = new System.Drawing.Size(249, 163);
			this.rtxtViewPreRelease.TabIndex = 1;
			this.rtxtViewPreRelease.Text = "";
			// 
			// splitContainerMain
			// 
			this.splitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerMain.Location = new System.Drawing.Point(0, 0);
			this.splitContainerMain.Name = "splitContainerMain";
			this.splitContainerMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainerMain.Panel1
			// 
			this.splitContainerMain.Panel1.Controls.Add(this.splitContainerLeft);
			// 
			// splitContainerMain.Panel2
			// 
			this.splitContainerMain.Panel2.Controls.Add(this.splitContainer1);
			this.splitContainerMain.Size = new System.Drawing.Size(773, 547);
			this.splitContainerMain.SplitterDistance = 302;
			this.splitContainerMain.TabIndex = 2;
			// 
			// splitContainerLeft
			// 
			this.splitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerLeft.Location = new System.Drawing.Point(0, 0);
			this.splitContainerLeft.Name = "splitContainerLeft";
			// 
			// splitContainerLeft.Panel1
			// 
			this.splitContainerLeft.Panel1.Controls.Add(this.groupBox1);
			this.splitContainerLeft.Panel1.Controls.Add(this.ppbPreRelease);
			this.splitContainerLeft.Panel1.Controls.Add(this.label1);
			this.splitContainerLeft.Panel1.Controls.Add(this.rtxtViewPreRelease);
			// 
			// splitContainerLeft.Panel2
			// 
			this.splitContainerLeft.Panel2.Controls.Add(this.splitContainerRight);
			this.splitContainerLeft.Size = new System.Drawing.Size(773, 302);
			this.splitContainerLeft.SplitterDistance = 255;
			this.splitContainerLeft.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.btnSavePre);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.txtIntervalPre);
			this.groupBox1.Controls.Add(this.btnStopPre);
			this.groupBox1.Controls.Add(this.btnReadTestPreRelease);
			this.groupBox1.Controls.Add(this.btnStartPre);
			this.groupBox1.Controls.Add(this.btnStartDownloadRelease);
			this.groupBox1.Location = new System.Drawing.Point(3, 213);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(249, 86);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Options";
			// 
			// btnSavePre
			// 
			this.btnSavePre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSavePre.Location = new System.Drawing.Point(107, 18);
			this.btnSavePre.Name = "btnSavePre";
			this.btnSavePre.Size = new System.Drawing.Size(44, 23);
			this.btnSavePre.TabIndex = 8;
			this.btnSavePre.Text = "Save";
			this.btnSavePre.UseVisualStyleBackColor = true;
			this.btnSavePre.Click += new System.EventHandler(this.btnSavePre_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(58, 23);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(43, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "minutes";
			// 
			// txtIntervalPre
			// 
			this.txtIntervalPre.Location = new System.Drawing.Point(3, 19);
			this.txtIntervalPre.Name = "txtIntervalPre";
			this.txtIntervalPre.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalPre.TabIndex = 3;
			this.txtIntervalPre.Text = "60";
			// 
			// btnStopPre
			// 
			this.btnStopPre.Location = new System.Drawing.Point(206, 18);
			this.btnStopPre.Name = "btnStopPre";
			this.btnStopPre.Size = new System.Drawing.Size(37, 23);
			this.btnStopPre.TabIndex = 7;
			this.btnStopPre.Text = "Stop";
			this.btnStopPre.UseVisualStyleBackColor = true;
			this.btnStopPre.Click += new System.EventHandler(this.btnStopPre_Click);
			// 
			// btnReadTestPreRelease
			// 
			this.btnReadTestPreRelease.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadTestPreRelease.Location = new System.Drawing.Point(109, 57);
			this.btnReadTestPreRelease.Name = "btnReadTestPreRelease";
			this.btnReadTestPreRelease.Size = new System.Drawing.Size(66, 23);
			this.btnReadTestPreRelease.TabIndex = 4;
			this.btnReadTestPreRelease.Text = "Read File";
			this.btnReadTestPreRelease.UseVisualStyleBackColor = true;
			this.btnReadTestPreRelease.Click += new System.EventHandler(this.btnReadFilePreRelease_Click);
			// 
			// btnStartPre
			// 
			this.btnStartPre.Location = new System.Drawing.Point(162, 18);
			this.btnStartPre.Name = "btnStartPre";
			this.btnStartPre.Size = new System.Drawing.Size(38, 23);
			this.btnStartPre.TabIndex = 6;
			this.btnStartPre.Text = "Start";
			this.btnStartPre.UseVisualStyleBackColor = true;
			this.btnStartPre.Click += new System.EventHandler(this.btnStartPre_Click);
			// 
			// ppbPreRelease
			// 
			this.ppbPreRelease.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ppbPreRelease.Location = new System.Drawing.Point(2, 194);
			this.ppbPreRelease.Name = "ppbPreRelease";
			this.ppbPreRelease.Size = new System.Drawing.Size(250, 13);
			this.ppbPreRelease.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(109, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Pre-Release Domains";
			// 
			// splitContainerRight
			// 
			this.splitContainerRight.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerRight.Location = new System.Drawing.Point(0, 0);
			this.splitContainerRight.Name = "splitContainerRight";
			// 
			// splitContainerRight.Panel1
			// 
			this.splitContainerRight.Panel1.Controls.Add(this.groupBox2);
			this.splitContainerRight.Panel1.Controls.Add(this.ppbPending);
			this.splitContainerRight.Panel1.Controls.Add(this.rtxtViewPending);
			this.splitContainerRight.Panel1.Controls.Add(this.label2);
			// 
			// splitContainerRight.Panel2
			// 
			this.splitContainerRight.Panel2.Controls.Add(this.groupBox3);
			this.splitContainerRight.Panel2.Controls.Add(this.rtxtViewAuctions);
			this.splitContainerRight.Panel2.Controls.Add(this.ppbAuctions);
			this.splitContainerRight.Panel2.Controls.Add(this.label3);
			this.splitContainerRight.Size = new System.Drawing.Size(514, 302);
			this.splitContainerRight.SplitterDistance = 255;
			this.splitContainerRight.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.btnSavePending);
			this.groupBox2.Controls.Add(this.btnStartDownloadPending);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.txtIntervalPending);
			this.groupBox2.Controls.Add(this.btnStopPending);
			this.groupBox2.Controls.Add(this.btnReadTestPending);
			this.groupBox2.Controls.Add(this.btnStartPending);
			this.groupBox2.Location = new System.Drawing.Point(3, 213);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(249, 86);
			this.groupBox2.TabIndex = 8;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Options";
			// 
			// btnSavePending
			// 
			this.btnSavePending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSavePending.Location = new System.Drawing.Point(107, 18);
			this.btnSavePending.Name = "btnSavePending";
			this.btnSavePending.Size = new System.Drawing.Size(44, 23);
			this.btnSavePending.TabIndex = 9;
			this.btnSavePending.Text = "Save";
			this.btnSavePending.UseVisualStyleBackColor = true;
			this.btnSavePending.Click += new System.EventHandler(this.btnSavePending_Click);
			// 
			// btnStartDownloadPending
			// 
			this.btnStartDownloadPending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartDownloadPending.Location = new System.Drawing.Point(3, 57);
			this.btnStartDownloadPending.Name = "btnStartDownloadPending";
			this.btnStartDownloadPending.Size = new System.Drawing.Size(100, 23);
			this.btnStartDownloadPending.TabIndex = 4;
			this.btnStartDownloadPending.Text = "Download file";
			this.btnStartDownloadPending.UseVisualStyleBackColor = true;
			this.btnStartDownloadPending.Click += new System.EventHandler(this.btnStartDownloadPending_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(58, 23);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(43, 13);
			this.label5.TabIndex = 3;
			this.label5.Text = "minutes";
			// 
			// txtIntervalPending
			// 
			this.txtIntervalPending.Location = new System.Drawing.Point(3, 19);
			this.txtIntervalPending.Name = "txtIntervalPending";
			this.txtIntervalPending.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalPending.TabIndex = 3;
			this.txtIntervalPending.Text = "60";
			// 
			// btnStopPending
			// 
			this.btnStopPending.Location = new System.Drawing.Point(206, 18);
			this.btnStopPending.Name = "btnStopPending";
			this.btnStopPending.Size = new System.Drawing.Size(37, 23);
			this.btnStopPending.TabIndex = 7;
			this.btnStopPending.Text = "Stop";
			this.btnStopPending.UseVisualStyleBackColor = true;
			this.btnStopPending.Click += new System.EventHandler(this.btnStopPending_Click);
			// 
			// btnReadTestPending
			// 
			this.btnReadTestPending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadTestPending.Location = new System.Drawing.Point(109, 57);
			this.btnReadTestPending.Name = "btnReadTestPending";
			this.btnReadTestPending.Size = new System.Drawing.Size(66, 23);
			this.btnReadTestPending.TabIndex = 4;
			this.btnReadTestPending.Text = "Read File";
			this.btnReadTestPending.UseVisualStyleBackColor = true;
			this.btnReadTestPending.Click += new System.EventHandler(this.button4_Click);
			// 
			// btnStartPending
			// 
			this.btnStartPending.Location = new System.Drawing.Point(162, 18);
			this.btnStartPending.Name = "btnStartPending";
			this.btnStartPending.Size = new System.Drawing.Size(38, 23);
			this.btnStartPending.TabIndex = 6;
			this.btnStartPending.Text = "Start";
			this.btnStartPending.UseVisualStyleBackColor = true;
			this.btnStartPending.Click += new System.EventHandler(this.btnStartPending_Click);
			// 
			// ppbPending
			// 
			this.ppbPending.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ppbPending.Location = new System.Drawing.Point(2, 194);
			this.ppbPending.Name = "ppbPending";
			this.ppbPending.Size = new System.Drawing.Size(250, 13);
			this.ppbPending.TabIndex = 5;
			// 
			// rtxtViewPending
			// 
			this.rtxtViewPending.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtViewPending.Location = new System.Drawing.Point(3, 25);
			this.rtxtViewPending.Name = "rtxtViewPending";
			this.rtxtViewPending.Size = new System.Drawing.Size(249, 163);
			this.rtxtViewPending.TabIndex = 2;
			this.rtxtViewPending.Text = "";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Pending Delete";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.btnSaveAuctions);
			this.groupBox3.Controls.Add(this.btnStartDowloadAuctions);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.txtIntervalAuction);
			this.groupBox3.Controls.Add(this.btnStopAuctions);
			this.groupBox3.Controls.Add(this.btnReadTestAuctions);
			this.groupBox3.Controls.Add(this.btnStartAuctions);
			this.groupBox3.Location = new System.Drawing.Point(2, 213);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(249, 86);
			this.groupBox3.TabIndex = 9;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Options";
			// 
			// btnSaveAuctions
			// 
			this.btnSaveAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveAuctions.Location = new System.Drawing.Point(107, 18);
			this.btnSaveAuctions.Name = "btnSaveAuctions";
			this.btnSaveAuctions.Size = new System.Drawing.Size(44, 23);
			this.btnSaveAuctions.TabIndex = 10;
			this.btnSaveAuctions.Text = "Save";
			this.btnSaveAuctions.UseVisualStyleBackColor = true;
			this.btnSaveAuctions.Click += new System.EventHandler(this.btnSaveAuctions_Click);
			// 
			// btnStartDowloadAuctions
			// 
			this.btnStartDowloadAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartDowloadAuctions.Location = new System.Drawing.Point(4, 57);
			this.btnStartDowloadAuctions.Name = "btnStartDowloadAuctions";
			this.btnStartDowloadAuctions.Size = new System.Drawing.Size(100, 23);
			this.btnStartDowloadAuctions.TabIndex = 6;
			this.btnStartDowloadAuctions.Text = "Download file";
			this.btnStartDowloadAuctions.UseVisualStyleBackColor = true;
			this.btnStartDowloadAuctions.Click += new System.EventHandler(this.btnStartDowloadAuctions_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(58, 23);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(43, 13);
			this.label6.TabIndex = 3;
			this.label6.Text = "minutes";
			// 
			// txtIntervalAuction
			// 
			this.txtIntervalAuction.Location = new System.Drawing.Point(3, 19);
			this.txtIntervalAuction.Name = "txtIntervalAuction";
			this.txtIntervalAuction.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalAuction.TabIndex = 3;
			this.txtIntervalAuction.Text = "60";
			// 
			// btnStopAuctions
			// 
			this.btnStopAuctions.Location = new System.Drawing.Point(206, 18);
			this.btnStopAuctions.Name = "btnStopAuctions";
			this.btnStopAuctions.Size = new System.Drawing.Size(37, 23);
			this.btnStopAuctions.TabIndex = 7;
			this.btnStopAuctions.Text = "Stop";
			this.btnStopAuctions.UseVisualStyleBackColor = true;
			this.btnStopAuctions.Click += new System.EventHandler(this.btnStopAuctions_Click);
			// 
			// btnReadTestAuctions
			// 
			this.btnReadTestAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadTestAuctions.Location = new System.Drawing.Point(109, 57);
			this.btnReadTestAuctions.Name = "btnReadTestAuctions";
			this.btnReadTestAuctions.Size = new System.Drawing.Size(66, 23);
			this.btnReadTestAuctions.TabIndex = 4;
			this.btnReadTestAuctions.Text = "Read File";
			this.btnReadTestAuctions.UseVisualStyleBackColor = true;
			this.btnReadTestAuctions.Click += new System.EventHandler(this.btnReadTestAuctions_Click);
			// 
			// btnStartAuctions
			// 
			this.btnStartAuctions.Location = new System.Drawing.Point(162, 19);
			this.btnStartAuctions.Name = "btnStartAuctions";
			this.btnStartAuctions.Size = new System.Drawing.Size(38, 23);
			this.btnStartAuctions.TabIndex = 6;
			this.btnStartAuctions.Text = "Start";
			this.btnStartAuctions.UseVisualStyleBackColor = true;
			this.btnStartAuctions.Click += new System.EventHandler(this.btnStartAuctions_Click);
			// 
			// rtxtViewAuctions
			// 
			this.rtxtViewAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtViewAuctions.Location = new System.Drawing.Point(2, 25);
			this.rtxtViewAuctions.Name = "rtxtViewAuctions";
			this.rtxtViewAuctions.Size = new System.Drawing.Size(249, 163);
			this.rtxtViewAuctions.TabIndex = 3;
			this.rtxtViewAuctions.Text = "";
			// 
			// ppbAuctions
			// 
			this.ppbAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ppbAuctions.Location = new System.Drawing.Point(3, 194);
			this.ppbAuctions.Name = "ppbAuctions";
			this.ppbAuctions.Size = new System.Drawing.Size(250, 13);
			this.ppbAuctions.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(107, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Auctions and Listings";
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.btnReadFileGoddy);
			this.splitContainer1.Panel1.Controls.Add(this.btnDownloadFileGoDaddy);
			this.splitContainer1.Panel1.Controls.Add(this.label8);
			this.splitContainer1.Panel1.Controls.Add(this.btnStopGoDaddy);
			this.splitContainer1.Panel1.Controls.Add(this.btnStartGodaddy);
			this.splitContainer1.Panel1.Controls.Add(this.pgbGoDaddy);
			this.splitContainer1.Panel1.Controls.Add(this.rtxtGoDaddy);
			this.splitContainer1.Panel1.Controls.Add(this.btnSaveGoDaddy);
			this.splitContainer1.Panel1.Controls.Add(this.txtIntervalGoDaddy);
			this.splitContainer1.Panel1.Controls.Add(this.label7);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
			this.splitContainer1.Size = new System.Drawing.Size(773, 241);
			this.splitContainer1.SplitterDistance = 255;
			this.splitContainer1.TabIndex = 0;
			// 
			// btnReadFileGoddy
			// 
			this.btnReadFileGoddy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadFileGoddy.Location = new System.Drawing.Point(112, 184);
			this.btnReadFileGoddy.Name = "btnReadFileGoddy";
			this.btnReadFileGoddy.Size = new System.Drawing.Size(66, 23);
			this.btnReadFileGoddy.TabIndex = 10;
			this.btnReadFileGoddy.Text = "Read File";
			this.btnReadFileGoddy.UseVisualStyleBackColor = true;
			this.btnReadFileGoddy.Click += new System.EventHandler(this.btnReadFileGoddy_Click);
			// 
			// btnDownloadFileGoDaddy
			// 
			this.btnDownloadFileGoDaddy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnDownloadFileGoDaddy.Location = new System.Drawing.Point(6, 184);
			this.btnDownloadFileGoDaddy.Name = "btnDownloadFileGoDaddy";
			this.btnDownloadFileGoDaddy.Size = new System.Drawing.Size(100, 23);
			this.btnDownloadFileGoDaddy.TabIndex = 9;
			this.btnDownloadFileGoDaddy.Text = "Download file";
			this.btnDownloadFileGoDaddy.UseVisualStyleBackColor = true;
			this.btnDownloadFileGoDaddy.Click += new System.EventHandler(this.btnDownloadFileGoDaddy_Click);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(3, 3);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(105, 13);
			this.label8.TabIndex = 12;
			this.label8.Text = "GoDaddy - All Listing";
			// 
			// btnStopGoDaddy
			// 
			this.btnStopGoDaddy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStopGoDaddy.Location = new System.Drawing.Point(209, 214);
			this.btnStopGoDaddy.Name = "btnStopGoDaddy";
			this.btnStopGoDaddy.Size = new System.Drawing.Size(37, 23);
			this.btnStopGoDaddy.TabIndex = 11;
			this.btnStopGoDaddy.Text = "Stop";
			this.btnStopGoDaddy.UseVisualStyleBackColor = true;
			this.btnStopGoDaddy.Click += new System.EventHandler(this.btnStopGoDaddy_Click);
			// 
			// btnStartGodaddy
			// 
			this.btnStartGodaddy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartGodaddy.Location = new System.Drawing.Point(165, 214);
			this.btnStartGodaddy.Name = "btnStartGodaddy";
			this.btnStartGodaddy.Size = new System.Drawing.Size(38, 23);
			this.btnStartGodaddy.TabIndex = 10;
			this.btnStartGodaddy.Text = "Start";
			this.btnStartGodaddy.UseVisualStyleBackColor = true;
			this.btnStartGodaddy.Click += new System.EventHandler(this.btnStartGodaddy_Click);
			// 
			// pgbGoDaddy
			// 
			this.pgbGoDaddy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pgbGoDaddy.Location = new System.Drawing.Point(2, 165);
			this.pgbGoDaddy.Name = "pgbGoDaddy";
			this.pgbGoDaddy.Size = new System.Drawing.Size(250, 13);
			this.pgbGoDaddy.TabIndex = 6;
			// 
			// rtxtGoDaddy
			// 
			this.rtxtGoDaddy.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtGoDaddy.Location = new System.Drawing.Point(3, 19);
			this.rtxtGoDaddy.Name = "rtxtGoDaddy";
			this.rtxtGoDaddy.Size = new System.Drawing.Size(249, 140);
			this.rtxtGoDaddy.TabIndex = 0;
			this.rtxtGoDaddy.Text = "";
			// 
			// btnSaveGoDaddy
			// 
			this.btnSaveGoDaddy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveGoDaddy.Location = new System.Drawing.Point(110, 214);
			this.btnSaveGoDaddy.Name = "btnSaveGoDaddy";
			this.btnSaveGoDaddy.Size = new System.Drawing.Size(44, 23);
			this.btnSaveGoDaddy.TabIndex = 10;
			this.btnSaveGoDaddy.Text = "Save";
			this.btnSaveGoDaddy.UseVisualStyleBackColor = true;
			this.btnSaveGoDaddy.Click += new System.EventHandler(this.btnSaveGoDaddy_Click);
			// 
			// txtIntervalGoDaddy
			// 
			this.txtIntervalGoDaddy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtIntervalGoDaddy.Location = new System.Drawing.Point(6, 215);
			this.txtIntervalGoDaddy.Name = "txtIntervalGoDaddy";
			this.txtIntervalGoDaddy.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalGoDaddy.TabIndex = 11;
			this.txtIntervalGoDaddy.Text = "60";
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(61, 219);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(43, 13);
			this.label7.TabIndex = 10;
			this.label7.Text = "minutes";
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.btnReadFileDomainsByDay);
			this.splitContainer2.Panel1.Controls.Add(this.rtxtDomainsByDay);
			this.splitContainer2.Panel1.Controls.Add(this.btnDownloadDomainsByDay);
			this.splitContainer2.Panel1.Controls.Add(this.label10);
			this.splitContainer2.Panel1.Controls.Add(this.label9);
			this.splitContainer2.Panel1.Controls.Add(this.txtIntervalDomainsByDay);
			this.splitContainer2.Panel1.Controls.Add(this.btnStopDomainsByDay);
			this.splitContainer2.Panel1.Controls.Add(this.btnSaveDomainsByDay);
			this.splitContainer2.Panel1.Controls.Add(this.btnStartDomainsByDay);
			this.splitContainer2.Panel1.Controls.Add(this.pgbDomainsByDay);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.btnReadFileDomainChanges);
			this.splitContainer2.Panel2.Controls.Add(this.rtxtDomainChanges);
			this.splitContainer2.Panel2.Controls.Add(this.pgbDomainChanges);
			this.splitContainer2.Panel2.Controls.Add(this.btnDownloadDomainChanges);
			this.splitContainer2.Panel2.Controls.Add(this.btnStartDomainChanges);
			this.splitContainer2.Panel2.Controls.Add(this.label11);
			this.splitContainer2.Panel2.Controls.Add(this.btnSaveDomainChanges);
			this.splitContainer2.Panel2.Controls.Add(this.label12);
			this.splitContainer2.Panel2.Controls.Add(this.btnStopDomainChanges);
			this.splitContainer2.Panel2.Controls.Add(this.txtIntervalDomainChanges);
			this.splitContainer2.Size = new System.Drawing.Size(514, 241);
			this.splitContainer2.SplitterDistance = 255;
			this.splitContainer2.TabIndex = 0;
			// 
			// btnReadFileDomainsByDay
			// 
			this.btnReadFileDomainsByDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadFileDomainsByDay.Location = new System.Drawing.Point(112, 184);
			this.btnReadFileDomainsByDay.Name = "btnReadFileDomainsByDay";
			this.btnReadFileDomainsByDay.Size = new System.Drawing.Size(66, 23);
			this.btnReadFileDomainsByDay.TabIndex = 16;
			this.btnReadFileDomainsByDay.Text = "Read File";
			this.btnReadFileDomainsByDay.UseVisualStyleBackColor = true;
			this.btnReadFileDomainsByDay.Click += new System.EventHandler(this.btnReadFileDomainsByDay_Click);
			// 
			// rtxtDomainsByDay
			// 
			this.rtxtDomainsByDay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtDomainsByDay.Location = new System.Drawing.Point(3, 19);
			this.rtxtDomainsByDay.Name = "rtxtDomainsByDay";
			this.rtxtDomainsByDay.Size = new System.Drawing.Size(249, 140);
			this.rtxtDomainsByDay.TabIndex = 13;
			this.rtxtDomainsByDay.Text = "";
			// 
			// btnDownloadDomainsByDay
			// 
			this.btnDownloadDomainsByDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnDownloadDomainsByDay.Location = new System.Drawing.Point(6, 184);
			this.btnDownloadDomainsByDay.Name = "btnDownloadDomainsByDay";
			this.btnDownloadDomainsByDay.Size = new System.Drawing.Size(100, 23);
			this.btnDownloadDomainsByDay.TabIndex = 15;
			this.btnDownloadDomainsByDay.Text = "Download file";
			this.btnDownloadDomainsByDay.UseVisualStyleBackColor = true;
			this.btnDownloadDomainsByDay.Click += new System.EventHandler(this.btnDownloadDomainsByDay_Click);
			// 
			// label10
			// 
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(61, 219);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(43, 13);
			this.label10.TabIndex = 19;
			this.label10.Text = "minutes";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(3, 3);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(79, 13);
			this.label9.TabIndex = 22;
			this.label9.Text = "DomainsByDay";
			// 
			// txtIntervalDomainsByDay
			// 
			this.txtIntervalDomainsByDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtIntervalDomainsByDay.Location = new System.Drawing.Point(6, 215);
			this.txtIntervalDomainsByDay.Name = "txtIntervalDomainsByDay";
			this.txtIntervalDomainsByDay.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalDomainsByDay.TabIndex = 21;
			this.txtIntervalDomainsByDay.Text = "60";
			// 
			// btnStopDomainsByDay
			// 
			this.btnStopDomainsByDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStopDomainsByDay.Location = new System.Drawing.Point(209, 214);
			this.btnStopDomainsByDay.Name = "btnStopDomainsByDay";
			this.btnStopDomainsByDay.Size = new System.Drawing.Size(37, 23);
			this.btnStopDomainsByDay.TabIndex = 20;
			this.btnStopDomainsByDay.Text = "Stop";
			this.btnStopDomainsByDay.UseVisualStyleBackColor = true;
			this.btnStopDomainsByDay.Click += new System.EventHandler(this.btnStopDomainsByDay_Click);
			// 
			// btnSaveDomainsByDay
			// 
			this.btnSaveDomainsByDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveDomainsByDay.Location = new System.Drawing.Point(110, 214);
			this.btnSaveDomainsByDay.Name = "btnSaveDomainsByDay";
			this.btnSaveDomainsByDay.Size = new System.Drawing.Size(44, 23);
			this.btnSaveDomainsByDay.TabIndex = 18;
			this.btnSaveDomainsByDay.Text = "Save";
			this.btnSaveDomainsByDay.UseVisualStyleBackColor = true;
			this.btnSaveDomainsByDay.Click += new System.EventHandler(this.btnSaveDomainsByDay_Click);
			// 
			// btnStartDomainsByDay
			// 
			this.btnStartDomainsByDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartDomainsByDay.Location = new System.Drawing.Point(165, 214);
			this.btnStartDomainsByDay.Name = "btnStartDomainsByDay";
			this.btnStartDomainsByDay.Size = new System.Drawing.Size(38, 23);
			this.btnStartDomainsByDay.TabIndex = 17;
			this.btnStartDomainsByDay.Text = "Start";
			this.btnStartDomainsByDay.UseVisualStyleBackColor = true;
			this.btnStartDomainsByDay.Click += new System.EventHandler(this.btnStartDomainsByDay_Click);
			// 
			// pgbDomainsByDay
			// 
			this.pgbDomainsByDay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pgbDomainsByDay.Location = new System.Drawing.Point(2, 165);
			this.pgbDomainsByDay.Name = "pgbDomainsByDay";
			this.pgbDomainsByDay.Size = new System.Drawing.Size(250, 13);
			this.pgbDomainsByDay.TabIndex = 14;
			// 
			// btnReadFileDomainChanges
			// 
			this.btnReadFileDomainChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadFileDomainChanges.Location = new System.Drawing.Point(112, 184);
			this.btnReadFileDomainChanges.Name = "btnReadFileDomainChanges";
			this.btnReadFileDomainChanges.Size = new System.Drawing.Size(66, 23);
			this.btnReadFileDomainChanges.TabIndex = 26;
			this.btnReadFileDomainChanges.Text = "Read File";
			this.btnReadFileDomainChanges.UseVisualStyleBackColor = true;
			this.btnReadFileDomainChanges.Click += new System.EventHandler(this.btnReadFileDomainChanges_Click);
			// 
			// rtxtDomainChanges
			// 
			this.rtxtDomainChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtDomainChanges.Location = new System.Drawing.Point(3, 19);
			this.rtxtDomainChanges.Name = "rtxtDomainChanges";
			this.rtxtDomainChanges.Size = new System.Drawing.Size(249, 140);
			this.rtxtDomainChanges.TabIndex = 23;
			this.rtxtDomainChanges.Text = "";
			// 
			// pgbDomainChanges
			// 
			this.pgbDomainChanges.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pgbDomainChanges.Location = new System.Drawing.Point(2, 165);
			this.pgbDomainChanges.Name = "pgbDomainChanges";
			this.pgbDomainChanges.Size = new System.Drawing.Size(250, 13);
			this.pgbDomainChanges.TabIndex = 24;
			// 
			// btnDownloadDomainChanges
			// 
			this.btnDownloadDomainChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnDownloadDomainChanges.Location = new System.Drawing.Point(6, 184);
			this.btnDownloadDomainChanges.Name = "btnDownloadDomainChanges";
			this.btnDownloadDomainChanges.Size = new System.Drawing.Size(100, 23);
			this.btnDownloadDomainChanges.TabIndex = 25;
			this.btnDownloadDomainChanges.Text = "Download file";
			this.btnDownloadDomainChanges.UseVisualStyleBackColor = true;
			this.btnDownloadDomainChanges.Click += new System.EventHandler(this.button2_Click);
			// 
			// btnStartDomainChanges
			// 
			this.btnStartDomainChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartDomainChanges.Location = new System.Drawing.Point(165, 214);
			this.btnStartDomainChanges.Name = "btnStartDomainChanges";
			this.btnStartDomainChanges.Size = new System.Drawing.Size(38, 23);
			this.btnStartDomainChanges.TabIndex = 27;
			this.btnStartDomainChanges.Text = "Start";
			this.btnStartDomainChanges.UseVisualStyleBackColor = true;
			this.btnStartDomainChanges.Click += new System.EventHandler(this.btnStartDomainChanges_Click);
			// 
			// label11
			// 
			this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(61, 219);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(43, 13);
			this.label11.TabIndex = 29;
			this.label11.Text = "minutes";
			// 
			// btnSaveDomainChanges
			// 
			this.btnSaveDomainChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveDomainChanges.Location = new System.Drawing.Point(110, 214);
			this.btnSaveDomainChanges.Name = "btnSaveDomainChanges";
			this.btnSaveDomainChanges.Size = new System.Drawing.Size(44, 23);
			this.btnSaveDomainChanges.TabIndex = 28;
			this.btnSaveDomainChanges.Text = "Save";
			this.btnSaveDomainChanges.UseVisualStyleBackColor = true;
			this.btnSaveDomainChanges.Click += new System.EventHandler(this.btnSaveDomainChanges_Click);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(3, 3);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(72, 13);
			this.label12.TabIndex = 32;
			this.label12.Text = "DailyChanges";
			// 
			// btnStopDomainChanges
			// 
			this.btnStopDomainChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStopDomainChanges.Location = new System.Drawing.Point(209, 214);
			this.btnStopDomainChanges.Name = "btnStopDomainChanges";
			this.btnStopDomainChanges.Size = new System.Drawing.Size(37, 23);
			this.btnStopDomainChanges.TabIndex = 30;
			this.btnStopDomainChanges.Text = "Stop";
			this.btnStopDomainChanges.UseVisualStyleBackColor = true;
			this.btnStopDomainChanges.Click += new System.EventHandler(this.btnStopDomainChanges_Click);
			// 
			// txtIntervalDomainChanges
			// 
			this.txtIntervalDomainChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtIntervalDomainChanges.Location = new System.Drawing.Point(6, 215);
			this.txtIntervalDomainChanges.Name = "txtIntervalDomainChanges";
			this.txtIntervalDomainChanges.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalDomainChanges.TabIndex = 31;
			this.txtIntervalDomainChanges.Text = "60";
			// 
			// bgwPre
			// 
			this.bgwPre.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwPre_DoWork);
			// 
			// bgwPending
			// 
			this.bgwPending.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwPending_DoWork);
			// 
			// bgwAuctions
			// 
			this.bgwAuctions.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwAuctions_DoWork);
			// 
			// timerPre
			// 
			this.timerPre.Tick += new System.EventHandler(this.timerPre_Tick);
			// 
			// timerPending
			// 
			this.timerPending.Tick += new System.EventHandler(this.timerPending_Tick);
			// 
			// timerAuction
			// 
			this.timerAuction.Tick += new System.EventHandler(this.timerAuction_Tick);
			// 
			// bgwGoDaddy
			// 
			this.bgwGoDaddy.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwGoDaddy_DoWork);
			// 
			// timerGoDaddy
			// 
			this.timerGoDaddy.Tick += new System.EventHandler(this.timerGoDaddy_Tick);
			// 
			// timerDomainsByDay
			// 
			this.timerDomainsByDay.Tick += new System.EventHandler(this.timerDomainsByDay_Tick);
			// 
			// bgwDomainsByDay
			// 
			this.bgwDomainsByDay.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwDomainsByDay_DoWork);
			// 
			// timerDomainChanges
			// 
			this.timerDomainChanges.Tick += new System.EventHandler(this.timerDomainChanges_Tick);
			// 
			// bgwDomainChanges
			// 
			this.bgwDomainChanges.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwDomainChanges_DoWork);
			// 
			// FrmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(773, 547);
			this.Controls.Add(this.splitContainerMain);
			this.Name = "FrmMain";
			this.Text = "Download Domain NameJet";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
			this.Load += new System.EventHandler(this.FrmMain_Load);
			this.splitContainerMain.Panel1.ResumeLayout(false);
			this.splitContainerMain.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
			this.splitContainerMain.ResumeLayout(false);
			this.splitContainerLeft.Panel1.ResumeLayout(false);
			this.splitContainerLeft.Panel1.PerformLayout();
			this.splitContainerLeft.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).EndInit();
			this.splitContainerLeft.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.splitContainerRight.Panel1.ResumeLayout(false);
			this.splitContainerRight.Panel1.PerformLayout();
			this.splitContainerRight.Panel2.ResumeLayout(false);
			this.splitContainerRight.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerRight)).EndInit();
			this.splitContainerRight.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel1.PerformLayout();
			this.splitContainer2.Panel2.ResumeLayout(false);
			this.splitContainer2.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnStartDownloadRelease;
		private System.Windows.Forms.RichTextBox rtxtViewPreRelease;
		private System.Windows.Forms.SplitContainer splitContainerMain;
		private System.Windows.Forms.SplitContainer splitContainerLeft;
		private System.Windows.Forms.SplitContainer splitContainerRight;
		private System.Windows.Forms.ProgressBar ppbPreRelease;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnStartDownloadPending;
		private System.Windows.Forms.RichTextBox rtxtViewPending;
		private System.Windows.Forms.ProgressBar ppbPending;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnStartDowloadAuctions;
		private System.Windows.Forms.RichTextBox rtxtViewAuctions;
		private System.Windows.Forms.ProgressBar ppbAuctions;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnReadTestPreRelease;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtIntervalPre;
		private System.Windows.Forms.Button btnStopPre;
		private System.Windows.Forms.Button btnStartPre;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtIntervalPending;
		private System.Windows.Forms.Button btnStopPending;
		private System.Windows.Forms.Button btnReadTestPending;
		private System.Windows.Forms.Button btnStartPending;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtIntervalAuction;
		private System.Windows.Forms.Button btnStopAuctions;
		private System.Windows.Forms.Button btnReadTestAuctions;
		private System.Windows.Forms.Button btnStartAuctions;
		private System.ComponentModel.BackgroundWorker bgwPre;
		private System.ComponentModel.BackgroundWorker bgwPending;
		private System.ComponentModel.BackgroundWorker bgwAuctions;
		private System.Windows.Forms.Timer timerPre;
		private System.Windows.Forms.Timer timerPending;
		private System.Windows.Forms.Timer timerAuction;
		private System.Windows.Forms.Button btnSavePre;
		private System.Windows.Forms.Button btnSavePending;
		private System.Windows.Forms.Button btnSaveAuctions;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Button btnStopGoDaddy;
		private System.Windows.Forms.Button btnStartGodaddy;
		private System.Windows.Forms.Button btnSaveGoDaddy;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtIntervalGoDaddy;
		private System.Windows.Forms.RichTextBox rtxtGoDaddy;
		private System.Windows.Forms.ProgressBar pgbGoDaddy;
		private System.ComponentModel.BackgroundWorker bgwGoDaddy;
		private System.Windows.Forms.Timer timerGoDaddy;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button btnReadFileGoddy;
		private System.Windows.Forms.Button btnDownloadFileGoDaddy;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.Button btnReadFileDomainsByDay;
		private System.Windows.Forms.RichTextBox rtxtDomainsByDay;
		private System.Windows.Forms.Button btnDownloadDomainsByDay;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtIntervalDomainsByDay;
		private System.Windows.Forms.Button btnStopDomainsByDay;
		private System.Windows.Forms.Button btnSaveDomainsByDay;
		private System.Windows.Forms.Button btnStartDomainsByDay;
		private System.Windows.Forms.ProgressBar pgbDomainsByDay;
		private System.Windows.Forms.Timer timerDomainsByDay;
		private System.ComponentModel.BackgroundWorker bgwDomainsByDay;
		private System.Windows.Forms.Button btnReadFileDomainChanges;
		private System.Windows.Forms.RichTextBox rtxtDomainChanges;
		private System.Windows.Forms.ProgressBar pgbDomainChanges;
		private System.Windows.Forms.Button btnDownloadDomainChanges;
		private System.Windows.Forms.Button btnStartDomainChanges;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Button btnSaveDomainChanges;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button btnStopDomainChanges;
		private System.Windows.Forms.TextBox txtIntervalDomainChanges;
		private System.Windows.Forms.Timer timerDomainChanges;
		private System.ComponentModel.BackgroundWorker bgwDomainChanges;
	}
}