﻿namespace ToolDownloadDomain
{
	partial class FrmSnapNames
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btnStartDownloadRelease = new System.Windows.Forms.Button();
			this.rtxtViewPreRelease = new System.Windows.Forms.RichTextBox();
			this.splitContainerMain = new System.Windows.Forms.SplitContainer();
			this.splitContainerLeft = new System.Windows.Forms.SplitContainer();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnSavePre = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.txtIntervalPre = new System.Windows.Forms.TextBox();
			this.btnStopPre = new System.Windows.Forms.Button();
			this.btnReadTestPreRelease = new System.Windows.Forms.Button();
			this.btnStartPre = new System.Windows.Forms.Button();
			this.ppbPreRelease = new System.Windows.Forms.ProgressBar();
			this.label1 = new System.Windows.Forms.Label();
			this.splitContainerRight = new System.Windows.Forms.SplitContainer();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.btnSavePending = new System.Windows.Forms.Button();
			this.btnStartDownloadPending = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.txtIntervalPending = new System.Windows.Forms.TextBox();
			this.btnStopPending = new System.Windows.Forms.Button();
			this.btnReadTestPending = new System.Windows.Forms.Button();
			this.btnStartPending = new System.Windows.Forms.Button();
			this.ppbPending = new System.Windows.Forms.ProgressBar();
			this.rtxtViewPending = new System.Windows.Forms.RichTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.btnSaveAuctions = new System.Windows.Forms.Button();
			this.btnStartDowloadAuctions = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.txtIntervalAuction = new System.Windows.Forms.TextBox();
			this.btnStopAuctions = new System.Windows.Forms.Button();
			this.btnReadTestAuctions = new System.Windows.Forms.Button();
			this.btnStartAuctions = new System.Windows.Forms.Button();
			this.rtxtViewAuctions = new System.Windows.Forms.RichTextBox();
			this.ppbAuctions = new System.Windows.Forms.ProgressBar();
			this.label3 = new System.Windows.Forms.Label();
			this.bgwPre = new System.ComponentModel.BackgroundWorker();
			this.bgwPending = new System.ComponentModel.BackgroundWorker();
			this.bgwAuctions = new System.ComponentModel.BackgroundWorker();
			this.timerPre = new System.Windows.Forms.Timer(this.components);
			this.timerPending = new System.Windows.Forms.Timer(this.components);
			this.timerAuction = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
			this.splitContainerMain.Panel1.SuspendLayout();
			this.splitContainerMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).BeginInit();
			this.splitContainerLeft.Panel1.SuspendLayout();
			this.splitContainerLeft.Panel2.SuspendLayout();
			this.splitContainerLeft.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerRight)).BeginInit();
			this.splitContainerRight.Panel1.SuspendLayout();
			this.splitContainerRight.Panel2.SuspendLayout();
			this.splitContainerRight.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnStartDownloadRelease
			// 
			this.btnStartDownloadRelease.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartDownloadRelease.Location = new System.Drawing.Point(3, 57);
			this.btnStartDownloadRelease.Name = "btnStartDownloadRelease";
			this.btnStartDownloadRelease.Size = new System.Drawing.Size(100, 23);
			this.btnStartDownloadRelease.TabIndex = 0;
			this.btnStartDownloadRelease.Text = "Download file";
			this.btnStartDownloadRelease.UseVisualStyleBackColor = true;
			this.btnStartDownloadRelease.Click += new System.EventHandler(this.btnDownloadHTML_Click);
			// 
			// rtxtViewPreRelease
			// 
			this.rtxtViewPreRelease.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtViewPreRelease.Location = new System.Drawing.Point(3, 25);
			this.rtxtViewPreRelease.Name = "rtxtViewPreRelease";
			this.rtxtViewPreRelease.Size = new System.Drawing.Size(249, 310);
			this.rtxtViewPreRelease.TabIndex = 1;
			this.rtxtViewPreRelease.Text = "";
			// 
			// splitContainerMain
			// 
			this.splitContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerMain.Location = new System.Drawing.Point(0, 0);
			this.splitContainerMain.Name = "splitContainerMain";
			this.splitContainerMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainerMain.Panel1
			// 
			this.splitContainerMain.Panel1.Controls.Add(this.splitContainerLeft);
			this.splitContainerMain.Size = new System.Drawing.Size(773, 498);
			this.splitContainerMain.SplitterDistance = 449;
			this.splitContainerMain.TabIndex = 2;
			// 
			// splitContainerLeft
			// 
			this.splitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerLeft.Location = new System.Drawing.Point(0, 0);
			this.splitContainerLeft.Name = "splitContainerLeft";
			// 
			// splitContainerLeft.Panel1
			// 
			this.splitContainerLeft.Panel1.Controls.Add(this.groupBox1);
			this.splitContainerLeft.Panel1.Controls.Add(this.ppbPreRelease);
			this.splitContainerLeft.Panel1.Controls.Add(this.label1);
			this.splitContainerLeft.Panel1.Controls.Add(this.rtxtViewPreRelease);
			// 
			// splitContainerLeft.Panel2
			// 
			this.splitContainerLeft.Panel2.Controls.Add(this.splitContainerRight);
			this.splitContainerLeft.Size = new System.Drawing.Size(773, 449);
			this.splitContainerLeft.SplitterDistance = 255;
			this.splitContainerLeft.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.btnSavePre);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.txtIntervalPre);
			this.groupBox1.Controls.Add(this.btnStopPre);
			this.groupBox1.Controls.Add(this.btnReadTestPreRelease);
			this.groupBox1.Controls.Add(this.btnStartPre);
			this.groupBox1.Controls.Add(this.btnStartDownloadRelease);
			this.groupBox1.Location = new System.Drawing.Point(3, 360);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(249, 86);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Options";
			// 
			// btnSavePre
			// 
			this.btnSavePre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSavePre.Location = new System.Drawing.Point(107, 18);
			this.btnSavePre.Name = "btnSavePre";
			this.btnSavePre.Size = new System.Drawing.Size(44, 23);
			this.btnSavePre.TabIndex = 8;
			this.btnSavePre.Text = "Save";
			this.btnSavePre.UseVisualStyleBackColor = true;
			this.btnSavePre.Click += new System.EventHandler(this.btnSavePre_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(58, 23);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(43, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "minutes";
			// 
			// txtIntervalPre
			// 
			this.txtIntervalPre.Location = new System.Drawing.Point(3, 19);
			this.txtIntervalPre.Name = "txtIntervalPre";
			this.txtIntervalPre.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalPre.TabIndex = 3;
			this.txtIntervalPre.Text = "1";
			// 
			// btnStopPre
			// 
			this.btnStopPre.Location = new System.Drawing.Point(206, 18);
			this.btnStopPre.Name = "btnStopPre";
			this.btnStopPre.Size = new System.Drawing.Size(37, 23);
			this.btnStopPre.TabIndex = 7;
			this.btnStopPre.Text = "Stop";
			this.btnStopPre.UseVisualStyleBackColor = true;
			this.btnStopPre.Click += new System.EventHandler(this.btnStopPre_Click);
			// 
			// btnReadTestPreRelease
			// 
			this.btnReadTestPreRelease.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadTestPreRelease.Location = new System.Drawing.Point(109, 57);
			this.btnReadTestPreRelease.Name = "btnReadTestPreRelease";
			this.btnReadTestPreRelease.Size = new System.Drawing.Size(66, 23);
			this.btnReadTestPreRelease.TabIndex = 4;
			this.btnReadTestPreRelease.Text = "Read File";
			this.btnReadTestPreRelease.UseVisualStyleBackColor = true;
			this.btnReadTestPreRelease.Click += new System.EventHandler(this.btnReadFilePreRelease_Click);
			// 
			// btnStartPre
			// 
			this.btnStartPre.Location = new System.Drawing.Point(162, 18);
			this.btnStartPre.Name = "btnStartPre";
			this.btnStartPre.Size = new System.Drawing.Size(38, 23);
			this.btnStartPre.TabIndex = 6;
			this.btnStartPre.Text = "Start";
			this.btnStartPre.UseVisualStyleBackColor = true;
			this.btnStartPre.Click += new System.EventHandler(this.btnStartPre_Click);
			// 
			// ppbPreRelease
			// 
			this.ppbPreRelease.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ppbPreRelease.Location = new System.Drawing.Point(2, 341);
			this.ppbPreRelease.Name = "ppbPreRelease";
			this.ppbPreRelease.Size = new System.Drawing.Size(250, 13);
			this.ppbPreRelease.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(78, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Available Soon";
			// 
			// splitContainerRight
			// 
			this.splitContainerRight.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerRight.Location = new System.Drawing.Point(0, 0);
			this.splitContainerRight.Name = "splitContainerRight";
			// 
			// splitContainerRight.Panel1
			// 
			this.splitContainerRight.Panel1.Controls.Add(this.groupBox2);
			this.splitContainerRight.Panel1.Controls.Add(this.ppbPending);
			this.splitContainerRight.Panel1.Controls.Add(this.rtxtViewPending);
			this.splitContainerRight.Panel1.Controls.Add(this.label2);
			// 
			// splitContainerRight.Panel2
			// 
			this.splitContainerRight.Panel2.Controls.Add(this.groupBox3);
			this.splitContainerRight.Panel2.Controls.Add(this.rtxtViewAuctions);
			this.splitContainerRight.Panel2.Controls.Add(this.ppbAuctions);
			this.splitContainerRight.Panel2.Controls.Add(this.label3);
			this.splitContainerRight.Size = new System.Drawing.Size(514, 449);
			this.splitContainerRight.SplitterDistance = 255;
			this.splitContainerRight.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.btnSavePending);
			this.groupBox2.Controls.Add(this.btnStartDownloadPending);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.txtIntervalPending);
			this.groupBox2.Controls.Add(this.btnStopPending);
			this.groupBox2.Controls.Add(this.btnReadTestPending);
			this.groupBox2.Controls.Add(this.btnStartPending);
			this.groupBox2.Location = new System.Drawing.Point(3, 360);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(249, 86);
			this.groupBox2.TabIndex = 8;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Options";
			// 
			// btnSavePending
			// 
			this.btnSavePending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSavePending.Location = new System.Drawing.Point(107, 18);
			this.btnSavePending.Name = "btnSavePending";
			this.btnSavePending.Size = new System.Drawing.Size(44, 23);
			this.btnSavePending.TabIndex = 9;
			this.btnSavePending.Text = "Save";
			this.btnSavePending.UseVisualStyleBackColor = true;
			this.btnSavePending.Click += new System.EventHandler(this.btnSavePending_Click);
			// 
			// btnStartDownloadPending
			// 
			this.btnStartDownloadPending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartDownloadPending.Location = new System.Drawing.Point(3, 57);
			this.btnStartDownloadPending.Name = "btnStartDownloadPending";
			this.btnStartDownloadPending.Size = new System.Drawing.Size(100, 23);
			this.btnStartDownloadPending.TabIndex = 4;
			this.btnStartDownloadPending.Text = "Download file";
			this.btnStartDownloadPending.UseVisualStyleBackColor = true;
			this.btnStartDownloadPending.Click += new System.EventHandler(this.btnStartDownloadPending_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(58, 23);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(43, 13);
			this.label5.TabIndex = 3;
			this.label5.Text = "minutes";
			// 
			// txtIntervalPending
			// 
			this.txtIntervalPending.Location = new System.Drawing.Point(3, 19);
			this.txtIntervalPending.Name = "txtIntervalPending";
			this.txtIntervalPending.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalPending.TabIndex = 3;
			this.txtIntervalPending.Text = "1";
			// 
			// btnStopPending
			// 
			this.btnStopPending.Location = new System.Drawing.Point(206, 18);
			this.btnStopPending.Name = "btnStopPending";
			this.btnStopPending.Size = new System.Drawing.Size(37, 23);
			this.btnStopPending.TabIndex = 7;
			this.btnStopPending.Text = "Stop";
			this.btnStopPending.UseVisualStyleBackColor = true;
			this.btnStopPending.Click += new System.EventHandler(this.btnStopPending_Click);
			// 
			// btnReadTestPending
			// 
			this.btnReadTestPending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadTestPending.Location = new System.Drawing.Point(109, 57);
			this.btnReadTestPending.Name = "btnReadTestPending";
			this.btnReadTestPending.Size = new System.Drawing.Size(66, 23);
			this.btnReadTestPending.TabIndex = 4;
			this.btnReadTestPending.Text = "Read File";
			this.btnReadTestPending.UseVisualStyleBackColor = true;
			this.btnReadTestPending.Click += new System.EventHandler(this.button4_Click);
			// 
			// btnStartPending
			// 
			this.btnStartPending.Location = new System.Drawing.Point(162, 18);
			this.btnStartPending.Name = "btnStartPending";
			this.btnStartPending.Size = new System.Drawing.Size(38, 23);
			this.btnStartPending.TabIndex = 6;
			this.btnStartPending.Text = "Start";
			this.btnStartPending.UseVisualStyleBackColor = true;
			this.btnStartPending.Click += new System.EventHandler(this.btnStartPending_Click);
			// 
			// ppbPending
			// 
			this.ppbPending.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ppbPending.Location = new System.Drawing.Point(2, 341);
			this.ppbPending.Name = "ppbPending";
			this.ppbPending.Size = new System.Drawing.Size(250, 13);
			this.ppbPending.TabIndex = 5;
			// 
			// rtxtViewPending
			// 
			this.rtxtViewPending.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtViewPending.Location = new System.Drawing.Point(3, 25);
			this.rtxtViewPending.Name = "rtxtViewPending";
			this.rtxtViewPending.Size = new System.Drawing.Size(249, 310);
			this.rtxtViewPending.TabIndex = 2;
			this.rtxtViewPending.Text = "";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(55, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "In Auction";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.btnSaveAuctions);
			this.groupBox3.Controls.Add(this.btnStartDowloadAuctions);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.txtIntervalAuction);
			this.groupBox3.Controls.Add(this.btnStopAuctions);
			this.groupBox3.Controls.Add(this.btnReadTestAuctions);
			this.groupBox3.Controls.Add(this.btnStartAuctions);
			this.groupBox3.Location = new System.Drawing.Point(2, 360);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(249, 86);
			this.groupBox3.TabIndex = 9;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Options";
			// 
			// btnSaveAuctions
			// 
			this.btnSaveAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnSaveAuctions.Location = new System.Drawing.Point(107, 18);
			this.btnSaveAuctions.Name = "btnSaveAuctions";
			this.btnSaveAuctions.Size = new System.Drawing.Size(44, 23);
			this.btnSaveAuctions.TabIndex = 10;
			this.btnSaveAuctions.Text = "Save";
			this.btnSaveAuctions.UseVisualStyleBackColor = true;
			this.btnSaveAuctions.Click += new System.EventHandler(this.btnSaveAuctions_Click);
			// 
			// btnStartDowloadAuctions
			// 
			this.btnStartDowloadAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnStartDowloadAuctions.Location = new System.Drawing.Point(4, 57);
			this.btnStartDowloadAuctions.Name = "btnStartDowloadAuctions";
			this.btnStartDowloadAuctions.Size = new System.Drawing.Size(100, 23);
			this.btnStartDowloadAuctions.TabIndex = 6;
			this.btnStartDowloadAuctions.Text = "Download file";
			this.btnStartDowloadAuctions.UseVisualStyleBackColor = true;
			this.btnStartDowloadAuctions.Click += new System.EventHandler(this.btnStartDowloadAuctions_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(58, 23);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(43, 13);
			this.label6.TabIndex = 3;
			this.label6.Text = "minutes";
			// 
			// txtIntervalAuction
			// 
			this.txtIntervalAuction.Location = new System.Drawing.Point(3, 19);
			this.txtIntervalAuction.Name = "txtIntervalAuction";
			this.txtIntervalAuction.Size = new System.Drawing.Size(49, 20);
			this.txtIntervalAuction.TabIndex = 3;
			this.txtIntervalAuction.Text = "1";
			// 
			// btnStopAuctions
			// 
			this.btnStopAuctions.Location = new System.Drawing.Point(206, 18);
			this.btnStopAuctions.Name = "btnStopAuctions";
			this.btnStopAuctions.Size = new System.Drawing.Size(37, 23);
			this.btnStopAuctions.TabIndex = 7;
			this.btnStopAuctions.Text = "Stop";
			this.btnStopAuctions.UseVisualStyleBackColor = true;
			this.btnStopAuctions.Click += new System.EventHandler(this.btnStopAuctions_Click);
			// 
			// btnReadTestAuctions
			// 
			this.btnReadTestAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnReadTestAuctions.Location = new System.Drawing.Point(109, 57);
			this.btnReadTestAuctions.Name = "btnReadTestAuctions";
			this.btnReadTestAuctions.Size = new System.Drawing.Size(66, 23);
			this.btnReadTestAuctions.TabIndex = 4;
			this.btnReadTestAuctions.Text = "Read File";
			this.btnReadTestAuctions.UseVisualStyleBackColor = true;
			this.btnReadTestAuctions.Click += new System.EventHandler(this.btnReadTestAuctions_Click);
			// 
			// btnStartAuctions
			// 
			this.btnStartAuctions.Location = new System.Drawing.Point(162, 19);
			this.btnStartAuctions.Name = "btnStartAuctions";
			this.btnStartAuctions.Size = new System.Drawing.Size(38, 23);
			this.btnStartAuctions.TabIndex = 6;
			this.btnStartAuctions.Text = "Start";
			this.btnStartAuctions.UseVisualStyleBackColor = true;
			this.btnStartAuctions.Click += new System.EventHandler(this.btnStartAuctions_Click);
			// 
			// rtxtViewAuctions
			// 
			this.rtxtViewAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rtxtViewAuctions.Location = new System.Drawing.Point(2, 25);
			this.rtxtViewAuctions.Name = "rtxtViewAuctions";
			this.rtxtViewAuctions.Size = new System.Drawing.Size(249, 310);
			this.rtxtViewAuctions.TabIndex = 3;
			this.rtxtViewAuctions.Text = "";
			// 
			// ppbAuctions
			// 
			this.ppbAuctions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ppbAuctions.Location = new System.Drawing.Point(3, 341);
			this.ppbAuctions.Name = "ppbAuctions";
			this.ppbAuctions.Size = new System.Drawing.Size(250, 13);
			this.ppbAuctions.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(44, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Expiring";
			// 
			// bgwPre
			// 
			this.bgwPre.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwPre_DoWork);
			// 
			// bgwPending
			// 
			this.bgwPending.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwPending_DoWork);
			// 
			// bgwAuctions
			// 
			this.bgwAuctions.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwAuctions_DoWork);
			// 
			// timerPre
			// 
			this.timerPre.Tick += new System.EventHandler(this.timerPre_Tick);
			// 
			// timerPending
			// 
			this.timerPending.Tick += new System.EventHandler(this.timerPending_Tick);
			// 
			// timerAuction
			// 
			this.timerAuction.Tick += new System.EventHandler(this.timerAuction_Tick);
			// 
			// FrmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(773, 498);
			this.Controls.Add(this.splitContainerMain);
			this.Name = "FrmMain";
			this.Text = "Download Domain From SnapNames";
			this.Load += new System.EventHandler(this.FrmMain_Load);
			this.splitContainerMain.Panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
			this.splitContainerMain.ResumeLayout(false);
			this.splitContainerLeft.Panel1.ResumeLayout(false);
			this.splitContainerLeft.Panel1.PerformLayout();
			this.splitContainerLeft.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerLeft)).EndInit();
			this.splitContainerLeft.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.splitContainerRight.Panel1.ResumeLayout(false);
			this.splitContainerRight.Panel1.PerformLayout();
			this.splitContainerRight.Panel2.ResumeLayout(false);
			this.splitContainerRight.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerRight)).EndInit();
			this.splitContainerRight.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnStartDownloadRelease;
		private System.Windows.Forms.RichTextBox rtxtViewPreRelease;
		private System.Windows.Forms.SplitContainer splitContainerMain;
		private System.Windows.Forms.SplitContainer splitContainerLeft;
		private System.Windows.Forms.SplitContainer splitContainerRight;
		private System.Windows.Forms.ProgressBar ppbPreRelease;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnStartDownloadPending;
		private System.Windows.Forms.RichTextBox rtxtViewPending;
		private System.Windows.Forms.ProgressBar ppbPending;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnStartDowloadAuctions;
		private System.Windows.Forms.RichTextBox rtxtViewAuctions;
		private System.Windows.Forms.ProgressBar ppbAuctions;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnReadTestPreRelease;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtIntervalPre;
		private System.Windows.Forms.Button btnStopPre;
		private System.Windows.Forms.Button btnStartPre;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtIntervalPending;
		private System.Windows.Forms.Button btnStopPending;
		private System.Windows.Forms.Button btnReadTestPending;
		private System.Windows.Forms.Button btnStartPending;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtIntervalAuction;
		private System.Windows.Forms.Button btnStopAuctions;
		private System.Windows.Forms.Button btnReadTestAuctions;
		private System.Windows.Forms.Button btnStartAuctions;
		private System.ComponentModel.BackgroundWorker bgwPre;
		private System.ComponentModel.BackgroundWorker bgwPending;
		private System.ComponentModel.BackgroundWorker bgwAuctions;
		private System.Windows.Forms.Timer timerPre;
		private System.Windows.Forms.Timer timerPending;
		private System.Windows.Forms.Timer timerAuction;
		private System.Windows.Forms.Button btnSavePre;
		private System.Windows.Forms.Button btnSavePending;
		private System.Windows.Forms.Button btnSaveAuctions;
	}
}