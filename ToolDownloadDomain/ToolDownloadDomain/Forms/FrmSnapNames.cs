﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Utility;

namespace ToolDownloadDomain
{
	public partial class FrmSnapNames : Form
	{
		private readonly SnapNamesAvailableSoon _SnapNamesAvailableSoon = new SnapNamesAvailableSoon("AvailableSoon");
		private readonly SnapNamesInAuction _SnapNamesInAuction = new SnapNamesInAuction("InAuction");
		private readonly SnapNamesExpiring _SnapNamesExpiring = new SnapNamesExpiring("Expiring");

		public delegate void WriteProgressBarDelegate(ProgressBar bar, int percent);
		private WriteProgressBarDelegate _WriteProgressBarDelegate;

		public delegate void WriteTextViewDelegate(RichTextBox rtxt, string text);
		private WriteTextViewDelegate _WriteTextViewDelegate;

		public FrmSnapNames()
		{
			InitializeComponent();
			_SnapNamesAvailableSoon.WriteLogEvent += WriteLogPreToView;
			_SnapNamesAvailableSoon.WritePropressEvent += WriteProgressBarPreRelease;
			_SnapNamesInAuction.WriteLogEvent += WriteLogPendingToView;
			_SnapNamesInAuction.WritePropressEvent += WriteProgressBarPending;
			_SnapNamesExpiring.WriteLogEvent += WriteLogAuctionToView;
			_SnapNamesExpiring.WritePropressEvent += WriteProgressBarAuctions;

			_WriteProgressBarDelegate = WriteProgressBar;
			_WriteTextViewDelegate = WriteTextView;

			//double minutes = 60;
			//double.TryParse(txtIntervalRefreshSEODomain.Text, out minutes);
			timerPre.Interval = (int)TimeSpan.FromMinutes(txtIntervalPre.Text.AsDouble(60)).TotalMilliseconds;
			timerPending.Interval = (int)TimeSpan.FromMinutes(txtIntervalPending.Text.AsDouble(60)).TotalMilliseconds;
			timerAuction.Interval = (int)TimeSpan.FromMinutes(txtIntervalAuction.Text.AsDouble(60)).TotalMilliseconds;

			timerAuction.Enabled = false;
			timerPending.Enabled = false;
			timerPre.Enabled = false;
		}

		private void WriteTextView(RichTextBox rtxt, string text)
		{
			rtxt.AppendText(text + "\n\n");
		}

		private void WriteProgressBar(ProgressBar bar, int percent)
		{
			bar.Value = percent;
		}

		private void WriteTextASync(RichTextBox rtxt, string message)
		{
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxt, message });
		}

		private void WriteLogPreToView(string Message)
		{
			//rtxtViewPreRelease.AppendText(Message+"\n");
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtViewPreRelease, Message });
			//_WriteTextViewDelegate.Invoke(rtxtViewPreRelease, Message);
		}
		private void WriteLogPendingToView(string Message)
		{
			//rtxtViewPending.AppendText(Message + "\n");
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtViewPending, Message });
			//_WriteTextViewDelegate.Invoke(rtxtViewPending, Message);
		}
		private void WriteLogAuctionToView(string Message)
		{
			//rtxtViewAuctions.AppendText(Message + "\n");
			this.BeginInvoke(_WriteTextViewDelegate, new object[] { rtxtViewAuctions, Message });
			//_WriteTextViewDelegate.Invoke(rtxtViewAuctions, Message);
		}
		private void WriteProgressBarPreRelease(int percent)
		{
			//ppbPreRelease.Value = percent;
			this.BeginInvoke(_WriteProgressBarDelegate, new object[] { ppbPreRelease, percent });
			//_WriteProgressBarDelegate.Invoke(ppbPreRelease, percent);
		}
		private void WriteProgressBarPending(int percent)
		{
			//ppbPending.Value = percent;
			this.BeginInvoke(_WriteProgressBarDelegate, new object[] { ppbPending, percent });
			//_WriteProgressBarDelegate.Invoke(ppbPending, percent);
		}
		private void WriteProgressBarAuctions(int percent)
		{
			//ppbAuctions.Value = percent;
			this.BeginInvoke(_WriteProgressBarDelegate, new object[] { ppbAuctions, percent });
			//_WriteProgressBarDelegate.Invoke(ppbAuctions, percent);
		}

		private void btnDownloadHTML_Click(object sender, EventArgs e)
		{
			//WriteProgressBarPreRelease(0);
			//_SnapNamesAvailableSoon.DownLoadHtml();

			if (!_SnapNamesAvailableSoon.WebIsBusy)
			{
				WriteProgressBarPreRelease(0);
				Task.Factory.StartNew(() => { _SnapNamesAvailableSoon.DownLoadHtml(); });
			}
		}

		private void btnStartDownloadPending_Click(object sender, EventArgs e)
		{
			//WriteProgressBarPending(0);
			//_SnapNamesInAuction.DownLoadHtml();

			if (!_SnapNamesInAuction.WebIsBusy)
			{
				WriteProgressBarPending(0);
				Task.Factory.StartNew(() => { _SnapNamesInAuction.DownLoadHtml(); });
			}

		}

		private void btnStartDowloadAuctions_Click(object sender, EventArgs e)
		{
			//WriteProgressBarAuctions(0);
			//_SnapNamesExpiring.DownLoadHtml();

			if (!_SnapNamesExpiring.WebIsBusy)
			{
				WriteProgressBarAuctions(0);
				Task.Factory.StartNew(() => { _SnapNamesExpiring.DownLoadHtml(); });
			}
		}

		private void FrmMain_Load(object sender, EventArgs e)
		{

		}

		private void btnReadFilePreRelease_Click(object sender, EventArgs e)
		{
			_SnapNamesAvailableSoon.ReadFileDownload();
		}

		private void bgwPre_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_SnapNamesAvailableSoon.WebIsBusy)
			{
				WriteTextASync(rtxtViewPreRelease, "Thực hiện download file");
				WriteProgressBarPreRelease(0);
				_SnapNamesAvailableSoon.DownLoadHtml();
			}
			else
			{
				WriteTextASync(rtxtViewPreRelease, "Downloading...");
			}
		}

		private void bgwPending_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_SnapNamesInAuction.WebIsBusy)
			{
				WriteTextASync(rtxtViewPending, "Thực hiện download file");
				WriteProgressBarPending(0);
				_SnapNamesInAuction.DownLoadHtml();
			}
			else
			{
				WriteTextASync(rtxtViewPending, "Downloading...");
			}
		}

		private void bgwAuctions_DoWork(object sender, DoWorkEventArgs e)
		{
			if (!_SnapNamesExpiring.WebIsBusy)
			{
				WriteTextASync(rtxtViewAuctions, "Thực hiện download file");
				WriteProgressBarAuctions(0);
				_SnapNamesExpiring.DownLoadHtml();
			}
			else
			{
				WriteTextASync(rtxtViewAuctions, "Downloading...");
			}
		}

		private void timerPre_Tick(object sender, EventArgs e)
		{
			if(!bgwPre.IsBusy)
			{
				bgwPre.RunWorkerAsync();
			}
		}

		private void timerPending_Tick(object sender, EventArgs e)
		{
			if (!bgwPending.IsBusy)
			{
				bgwPending.RunWorkerAsync();
			}
		}

		private void timerAuction_Tick(object sender, EventArgs e)
		{
			if (!bgwAuctions.IsBusy)
			{
				bgwAuctions.RunWorkerAsync();
			}
		}

		private void btnStartPre_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewPreRelease, "Start Download domain");
			timerPre.Enabled = true;
		}

		private void btnStopPre_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewPreRelease, "Stop Download domain");
			timerPre.Enabled = false;
		}

		private void btnStartPending_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewPending, "Start Download domain ");
			timerPending.Enabled = true;
		}

		private void btnStopPending_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewPending, "Stop Download domain ");
			timerPending.Enabled = false;
		}

		private void btnStartAuctions_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewAuctions, "Start Download domain ");
			timerAuction.Enabled = true;
		}

		private void btnStopAuctions_Click(object sender, EventArgs e)
		{
			_WriteTextViewDelegate.Invoke(rtxtViewAuctions, "Stop Download domain ");
			timerAuction.Enabled = false;
		}

		private void button4_Click(object sender, EventArgs e)
		{
			_SnapNamesInAuction.ReadFileDownload();
		}

		private void btnSavePre_Click(object sender, EventArgs e)
		{
			timerPre.Interval = (int)TimeSpan.FromMinutes(txtIntervalPre.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnSavePending_Click(object sender, EventArgs e)
		{
			timerPending.Interval = (int)TimeSpan.FromMinutes(txtIntervalPending.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnSaveAuctions_Click(object sender, EventArgs e)
		{
			timerAuction.Interval = (int)TimeSpan.FromMinutes(txtIntervalAuction.Text.AsDouble(60)).TotalMilliseconds;
		}

		private void btnReadTestAuctions_Click(object sender, EventArgs e)
		{
			_SnapNamesExpiring.ReadFileDownload();
		}
	}
}
