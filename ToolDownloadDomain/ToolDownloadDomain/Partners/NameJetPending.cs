﻿using DTO.Objects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Utility;

namespace ToolDownloadDomain
{
	public class NameJetPending:Partner
	{
		public NameJetPending(string id)
			: base(id)
		{
			
		}

		public override void ReadFileDownload()
		{
			WebIsBusy = true;
			WriteLogHandle("Thực hiện đọc file");
			Stopwatch watch = new Stopwatch();
			watch.Start();
			WriteProgressHandle(0);
			FileDownload fileDownload = GetFileDownload();
			try
			{
				if (fileDownload != null)
				{
					WriteLogHandle("Lấy được 1 file Pending chưa đọc: " + fileDownload.LocalPath);
					string filecontent = "";
					using (StreamReader reader = new StreamReader(fileDownload.LocalPath))
					{
						filecontent = reader.ReadToEnd();
					}
					List<string> rows = Regex.Split(filecontent, @"[\r\n]+").ToList();

					WriteLogHandle("File có tất cả: " + rows.Count + " records");
					//WriteLogHandle("Đang refresh domain...");
					//CancellationTokenSource cancellationSource = new CancellationTokenSource();
					//ParallelOptions options = new ParallelOptions();
					//options.CancellationToken = cancellationSource.Token;
					////options.MaxDegreeOfParallelism=3;
					//int count = 0;
					//int percent = 0;
					//object sync = new Object();
					//var task = Task.Factory.StartNew(() =>
					//{

					//	Parallel.For(0, rows.Count, options, (i) =>
					//	{
					//		//lock (sync)
					//		//{
					//		//	count += 1;
					//		//	percent = (count / rows.Count) * 100;
					//		//}
					//		DomainNotBelongVN domain = new DomainNotBelongVN();
					//		domain.AuctionsDate = null;
					//		domain.DomainName = rows[i];
					//		domain.DomainType = _xPath.Id;
					//		domain.Origin = _xPath.Name;
					//		domain.ForSale = true;
					//		domain.ID = -1;
					//		domain.Note = "";
					//		if (InsertDomain(domain))
					//		{

					//		}
					//		else
					//		{

					//		}
					//		//WriteProgressHandle(percent);
					//	});
					//});
					//task.ContinueWith(antecedent =>
					//{
					//	WebIsBusy = false;
					//	fileDownload.Status = FileDownloadStatus.Close;
					//	if(UpdateFileDownload(fileDownload))
					//	{
					//		WriteLogHandle("Update status file success");
					//	}
					//	else
					//	{
					//		WriteLogHandle("Update status file fail");
					//	}
					//	watch.Stop();
					//	WriteLogHandle("Kết thúc đọc file, Total: " + watch.Elapsed.TotalMinutes + " minutes");
					//});
				}
				else
				{
					WriteLogHandle("Không có file Pending nào chưa được đọc.");
				}
			}
			catch(Exception ex)
			{
				WriteLogHandle("Đọc file bị lỗi. "+ex.Message);
				fileDownload.Status = FileDownloadStatus.Read_Error;
				if (UpdateFileDownload(fileDownload))
				{
					WriteLogHandle("Update status file success");
				}
				else
				{
					WriteLogHandle("Update status file fail");
				}
				WebIsBusy = false;
				watch.Stop();
				WriteLogHandle("Kết thúc đọc file, Total: " + watch.Elapsed.TotalMinutes + " minutes");
			}
		}
		public override string GetContent()
		{
			DateTime now = DateTime.Now;
			//string filetoday = string.Format("{0}, {1} {2}, {3}", now.DayOfWeek.ToString(), now.DayOfYear, now.Day, now.Year);
			string filetoday = now.ToString("dddd, MMMM dd, yyyy");
			if (PartnerHtmlDocument != null)
			{
				var node = PartnerHtmlDocument.DocumentNode.SelectNodes(_xPath.Value.Replace("{1}", filetoday));
				if (node != null)
				{
					string date = node.FirstOrDefault().InnerText;
					return date.TrimEnd().TrimStart();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		public override string GetUrl()
		{
			DateTime now=DateTime.Now;
			string filetoday = now.ToString("dddd, MMMM dd, yyyy");
			if (PartnerHtmlDocument != null)
			{
				var node = PartnerHtmlDocument.DocumentNode.SelectNodes(_xPath.Value.Replace("{1}", filetoday));
				if (node != null)
				{
					string link = node.FirstOrDefault().Attributes["href"].Value;
					return _xPath.HomePage + link.TrimEnd().TrimStart();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
	}
}
