﻿using DTO.Objects;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Utility;

namespace ToolDownloadDomain
{
	public class DomainChanges_v2 : Partner
	{
		private ChromeDriver _chromeDriver { get; set; }
		public DomainChanges_v2(string id)
			: base(id)
		{
			///Khởi tạo
			//var chromeOptions = new ChromeOptions();
			//chromeOptions.AddUserProfilePreference("download.default_directory", Directory.GetCurrentDirectory());
			//chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
			//chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");

			String myDownloadFolder = Directory.GetCurrentDirectory() + @"\Download\DomainChanges"; 
			var options = new ChromeOptions(); 
			options.AddUserProfilePreference("download.default_directory", myDownloadFolder); 

			_chromeDriver = new ChromeDriver(Directory.GetCurrentDirectory(),options);
		}

		public override void DownLoadHtml()
		{
			try
			{
				//_webClient.DownloadStringAsync(new Uri(_xPath.LinkDownload));
				if(_xPath.DNS!=null)
				{
					string date=DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd");
					foreach (var dns in _xPath.DNS)
					{
						string url = string.Format(_xPath.LinkDownload, dns, date);
						//var source = _webClient.DownloadString(new Uri(url));
						_chromeDriver.Navigate().GoToUrl(url);

						WebDriverWait wait = new WebDriverWait(_chromeDriver, new TimeSpan(0, 0, 10));
						wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_xPath.Value)));

						DownLoadHtmlCompleted(dns, null);
						Thread.Sleep(10000);
					}
					
				}
				else
				{
					WriteLogHandle("Không tìm thấy DNS nào cần download.");
				}
				
			}
			catch (Exception ex)
			{
				_autoResetEvent.Set();
				WriteLogHandle("Download HTML lỗi: " + ex.Message);
			}
		}

		public override void DownLoadHtmlCompleted(object dns, System.Net.DownloadStringCompletedEventArgs e)
		{
			file = new FileDownload();
			file.Type = dns.ToString();

			DownloadFiles();

			if(PrepareFileAfterDownload(Directory.GetCurrentDirectory() + @"\Download\DomainChanges", 2))
			{
				if (InsertFileDownload(file))
				{
					WriteLogHandle("Insert file thành công");
				}
				else
				{
					WriteLogHandle("Insert file thất bại");
				}
			}
		}

		public override void DownloadFiles()
		{
			try
			{
				/*Kiểm tra folder ngày xem có chưa*/
				string folderToday = DateTime.Now.ToString("yyyyMMdd");
				string pathFolderToday = Directory.GetCurrentDirectory() + @"\Download\DomainChanges";
				
				if (!Directory.Exists(pathFolderToday))
				{
					Directory.CreateDirectory(pathFolderToday);
				}
				file.Title = GetContent();
				//Url
				file.LinkDownload = GetUrl();
				file.Origin = _xPath.Name;
				file.HomePage = _xPath.HomePage;
				file.Query = _xPath.Value;
				file.CreatedDate = DateTime.Now;
				//file.Type = _xPath.Id;
				//file name saved
				Regex regex = new Regex(@"/([\w.-]{1,})$");
				var match = regex.Match(file.LinkDownload);
				if (match.Success)
				{
					file.FileName = match.Groups[1].Value;
					//file.LocalPath = pathFolderToday + @"\" + DateTime.Now.ToString("ddMMHHmmss_") + file.FileName;
					WriteLogHandle("Start download: " + file.LinkDownload);
					Click(_xPath.Value);
					Thread.Sleep(10000);
					
					WriteLogHandle("Download completed");
				}
			}
			catch(Exception ex)
			{
				WriteLogHandle("Không tìm thấy file để download: "+ex.Message);
			}
		}
		public override bool PrepareFileAfterDownload(string path, int rowstart)
		{
			try
			{
				WriteLogHandle("Format lại file.");
				/*Lấy tên file đã giải nén*/
				DirectoryInfo info = new DirectoryInfo(path);
				FileInfo[] files = info.GetFiles().Where(p=>p.CreationTime>file.CreatedDate).OrderByDescending(p => p.CreationTime).ToArray();
				FileInfo lastFile;
				if (files != null)
				{
					lastFile = files.FirstOrDefault();
					file.LocalPath = lastFile.FullName;
					file.FileName = lastFile.Name;

					string text = File.ReadAllText(file.LocalPath);
					var mathches = Regex.Matches(text, @"\r\n");
					string main = text.Substring(mathches[rowstart].Index + 2);
					File.WriteAllText(file.LocalPath, main);
					WriteLogHandle("File save as: " + lastFile.FullName);
					WriteLogHandle("Kết thúc format lại file.");
					return true;
				}
				else
				{
					WriteLogHandle("Không tim thấy file mới download");
					return false;
				}
			}
			catch (Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi format file. " + ex.Message);
				return false;
			}
		}
		

		public override void DownLoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e, DTO.Objects.FileDownload ob)
		{
			try
			{
				
				if (!e.Cancelled && e.Error == null)
				{
					WriteLogHandle(ob.Type + " Download Completed: " + ob.LocalPath);


					//if (PrepareFileAfterDownload(ob.LocalPath, 2))
					//{
					//	if (InsertFileDownload(ob))
					//	{
					//		WriteLogHandle("Insert file thành công");
					//	}
					//	else
					//	{
					//		WriteLogHandle("Insert file thất bại");
					//	}
					//}
					//else
					//{
					//	WriteLogHandle("Prepare file thất bại, không thực hiện insert");
					//}
					
				}
				else
				{
					WriteLogHandle("Download file đã bị stop. "+e.Error);
				}
				WriteLogHandle("Kết thúc download file");
				///Sau khi insert file xong thì thực hiện đọc file luôn
				//ReadFileDownload();
				
			}
			catch (Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi chuẩn bị insert file. " + ex.Message);
			}
			
			_autoResetEvent.Set();
		}
		

		public override void ReadFileDownload()
		{
			WebIsBusy = true;
			WriteLogHandle("Thực hiện đọc file");
			Stopwatch watch = new Stopwatch();
			watch.Start();
			WriteProgressHandle(0);
			FileDownload fileDownload = GetFileDownload();
			try
			{
				if (fileDownload != null)
				{
					WriteLogHandle("Lấy được 1 file Pending chưa đọc: " + fileDownload.LocalPath);
					string filecontent = "";
					using (StreamReader reader = new StreamReader(fileDownload.LocalPath))
					{
						filecontent = reader.ReadToEnd();
					}
					List<string> rows = Regex.Split(filecontent, @"[\r\n]+").ToList();

					WriteLogHandle("File có tất cả: " + rows.Count + " records");
				}
				else
				{
					WriteLogHandle("Không có file Pending nào chưa được đọc.");
				}
			}
			catch (Exception ex)
			{
				WriteLogHandle("Đọc file bị lỗi. " + ex.Message);
				fileDownload.Status = FileDownloadStatus.Read_Error;
				if (UpdateFileDownload(fileDownload))
				{
					WriteLogHandle("Update status file success");
				}
				else
				{
					WriteLogHandle("Update status file fail");
				}
				WebIsBusy = false;
				watch.Stop();
				WriteLogHandle("Kết thúc đọc file, Total: " + watch.Elapsed.TotalMinutes + " minutes");
			}
		}
		public void Close()
		{
			_chromeDriver.Close();
			_chromeDriver.Quit();
			_chromeDriver.Dispose();
		}

		public override string GetContent()
		{
			var element = _chromeDriver.FindElementByXPath(_xPath.Value);
			return element.Text;
		}

		public override string GetUrl()
		{
			var element = _chromeDriver.FindElementByXPath(_xPath.Value);
			return element.GetAttribute("href");
		}

		public bool Click(string xpath)
		{
			try
			{
				//IWebElement element = WebDriver.FindElementByXPath(xpath);
				//if (element != null)
				//{
				//	element.Click();
				//}

				WebDriverWait wait = new WebDriverWait(_chromeDriver, new TimeSpan(0, 0, 10));
				wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));

				IWebElement element = _chromeDriver.FindElementByXPath(xpath);
				element.Click();

				WriteLogHandle("click success: " + xpath);
				return true;
			}
			catch
			{
				WriteLogHandle("click fail: " + xpath);
				return false;
			}
		}
	}
}
