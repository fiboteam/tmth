﻿using DTO.Objects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Utility;

namespace ToolDownloadDomain
{
	public class SnapNamesExpiring:Partner
	{
		public SnapNamesExpiring(string id)
			: base(id)
		{
			
		}

		public override void DownloadFiles()
		{
			try
			{
				/*Kiểm tra folder ngày xem có chưa*/
				string folderToday = DateTime.Now.ToString("yyyyMMdd");
				string pathFolderToday = Directory.GetCurrentDirectory() + @"\Download\" + folderToday;
				if (!Directory.Exists(pathFolderToday))
				{
					Directory.CreateDirectory(pathFolderToday);
				}
				file = new FileDownload();
				file.Title = GetContent();
				//Url
				file.LinkDownload = GetUrl();
				file.Origin = _xPath.Name;
				file.HomePage = _xPath.HomePage;
				file.Query = _xPath.Value;
				file.Type = _xPath.Id;
				//file name saved
				Regex regex = new Regex(@"file=([\w.-]{1,})$");
				var match = regex.Match(file.LinkDownload);
				if (match.Success)
				{
					file.FileName = match.Groups[1].Value;
					file.LocalPath = pathFolderToday + @"\" + DateTime.Now.ToString("ddMMHHmmss_") + file.FileName;
					WriteLogHandle("Start download: " + file.LinkDownload);
					//using (WebClient wc = new WebClient())
					//{
					//_webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler((sender, e) => DownLoading(sender, e, file));
					//_webClient.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler((sender, e) => DownLoadCompleted(sender, e, file));
					_webClient.DownloadFileAsync(new System.Uri(file.LinkDownload), file.LocalPath);
					//wc.DownloadFile(new System.Uri(file.LinkDownload), file.LocalPath);
					//}
				}
			}
			catch
			{
				WriteLogHandle("Không tìm thấy file để download");
			}
		}

		public override void ReadFileDownload()
		{
			WebIsBusy = true;
			WriteLogHandle("Thực hiện đọc file");
			Stopwatch watch = new Stopwatch();
			watch.Start();
			WriteProgressHandle(0);
			FileDownload fileDownload = GetFileDownload();
			try
			{
				if (fileDownload != null)
				{
					WriteLogHandle("Lấy được 1 file Pending chưa đọc: " + fileDownload.LocalPath);
					string filecontent = "";
					using (StreamReader reader = new StreamReader(fileDownload.LocalPath))
					{
						filecontent = reader.ReadToEnd();
					}
					List<string> rows = Regex.Split(filecontent, @"[\r\n]+").ToList();

					WriteLogHandle("File có tất cả: " + rows.Count + " records");
				}
				else
				{
					WriteLogHandle("Không có file Pending nào chưa được đọc.");
				}
			}
			catch(Exception ex)
			{
				WriteLogHandle("Đọc file bị lỗi. "+ex.Message);
				fileDownload.Status = FileDownloadStatus.Read_Error;
				if (UpdateFileDownload(fileDownload))
				{
					WriteLogHandle("Update status file success");
				}
				else
				{
					WriteLogHandle("Update status file fail");
				}
				WebIsBusy = false;
				watch.Stop();
				WriteLogHandle("Kết thúc đọc file, Total: " + watch.Elapsed.TotalMinutes + " minutes");
			}
		}
		public override string GetContent()
		{
			if (PartnerHtmlDocument != null)
			{
				var node = PartnerHtmlDocument.DocumentNode.SelectNodes(_xPath.Value);
				if (node != null)
				{
					string date = node.FirstOrDefault().Attributes["title"].Value;
					return date.TrimEnd().TrimStart();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
		public override void DownLoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e, DTO.Objects.FileDownload ob)
		{
			try
			{
				if (!e.Cancelled && e.Error == null)
				{
					WriteLogHandle(ob.Type + " Download Completed: " + ob.LocalPath);
					string pathFinish = "";
					if (UnFileRar(ob.LocalPath, out pathFinish))
					{
						/*Lấy tên file đã giải nén*/
						string[] fileArray = Directory.GetFiles(pathFinish);


						ob.LocalPath = fileArray[0];

						if (PrepareFileAfterDownload(ob.LocalPath, 2))
						{
							if (InsertFileDownload(ob))
							{
								WriteLogHandle("Insert file thành công");
							}
							else
							{
								WriteLogHandle("Insert file thất bại");
							}
						}
						else
						{
							WriteLogHandle("Prepare file thất bại, không thực hiện insert");
						}
					}
					else
					{
						WriteLogHandle("Không insert file do giải nén thất bại");
					}
				}
				else
				{
					WriteLogHandle("Download file bị dừng." + e.Error.Message);
				}
				WriteLogHandle("Kết thúc download file");
				///Sau khi insert file xong thì thực hiện đọc file luôn
				//ReadFileDownload();
			}
			catch (Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi chuẩn bị insert file. " + ex.Message);
			}
		}
		public override bool PrepareFileAfterDownload(string path, int rowstart)
		{
			try
			{
				WriteLogHandle("Format lại file.");
				/*Đọc file*/
				string text = File.ReadAllText(path);
				var mathches = Regex.Matches(text, @"\r\n");
				string main = text.Substring(mathches[rowstart].Index + 2);
				main = Regex.Replace(main, @"[ \t]+", "\t");
				main = Regex.Replace(main, @"[,]+", "");
				File.WriteAllText(path, main);
				WriteLogHandle("Kết thúc format lại file.");
				return true;
			}
			catch(Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi format file. " + ex.Message);
				return false;
			}
		} 
	}
}
