﻿using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ToolDownloadDomain
{
	public interface IPartner
	{
		void DownLoadHtml();
		void DownLoadHtmlCompleted(object sender, DownloadStringCompletedEventArgs e);
		void DownloadFiles();
		string GetContent();
		string GetUrl();
		void DownLoading(object sender, DownloadProgressChangedEventArgs e, FileDownload ob);
		void DownLoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e, FileDownload ob);
		void ReadFileDownload();
		bool InsertFileDownload(FileDownload file);
		bool UpdateFileDownload(FileDownload file);
		bool InsertDomain(DomainNotBelongVN domain);
		bool UnFileRar(string pathFileRar, out string destinationFolder);
		bool PrepareFileAfterDownload(string path, int rowstart);
		FileDownload GetFileDownload();
	}
}
