﻿using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utility;

namespace ToolDownloadDomain
{
	public class NameJetPreRelease:Partner
	{
		public NameJetPreRelease(string id):base(id)
		{
			
		}

		public override void ReadFileDownload()
		{

			WebIsBusy = true;
			WriteLogHandle("Thực hiện đọc file");
			Stopwatch watch = new Stopwatch();
			watch.Start();
			WriteProgressHandle(0);
			FileDownload fileDownload = GetFileDownload();
			try
			{
				if (fileDownload != null)
				{
					WriteLogHandle("Lấy được 1 file Pre-Release chưa đọc: " + fileDownload.LocalPath);
					string filecontent = "";
					using (StreamReader reader = new StreamReader(fileDownload.LocalPath))
					{
						filecontent = reader.ReadToEnd();
					}
					List<string> rows = Regex.Split(filecontent, @"[\r\n]+").ToList();

					WriteLogHandle("File có tất cả: " + rows.Count + " records");
					//WriteLogHandle("Đang refresh domain...");
					//var task = Task.Factory.StartNew(() =>
					//{
					//	Parallel.For(0, rows.Count, (i) =>
					//	{
					//		try
					//		{
					//			List<string> info = Regex.Split(rows[i], @"[,]+").ToList();
					//			if (info.Count >= 3)
					//			{
					//				DomainNotBelongVN domain = new DomainNotBelongVN();
					//				//domain.AuctionsDate = DateTime.ParseExact(info[1], GFunction.GuessPattern(info[1], CultureInfo.InvariantCulture), System.Globalization.CultureInfo.InvariantCulture);
					//				string[] format = { "yyyy-MM-dd" };
					//				DateTime date;

					//				if (DateTime.TryParseExact(info[1],
					//										   format,
					//										   System.Globalization.CultureInfo.InvariantCulture,
					//										   System.Globalization.DateTimeStyles.None,
					//										   out date))
					//				{
					//					domain.AuctionsDate = date;
					//				}
					//				else
					//				{
					//					domain.AuctionsDate = null;
					//				}
					//				domain.DomainName = info[0];
					//				domain.DomainType = _xPath.Id;
					//				domain.Origin = _xPath.Name;
					//				domain.ForSale = true;
					//				domain.ID = -1;
					//				domain.Note = info[2];
					//				if (InsertDomain(domain))
					//				{

					//				}
					//				else
					//				{

					//				}
					//			}
					//		}
					//		catch(Exception ex)
					//		{
					//			WriteLogHandle("Xữ lý data bị lỗi. "+ex.Message);
					//		}
					//		//WriteProgressHandle(percent);
					//	});
					//});
					//task.ContinueWith(antecedent =>
					//{
					//	WebIsBusy = false;
					//	fileDownload.Status = FileDownloadStatus.Close;
					//	if (UpdateFileDownload(fileDownload))
					//	{
					//		WriteLogHandle("Update status file success");
					//	}
					//	else
					//	{
					//		WriteLogHandle("Update status file fail");
					//	}
					//	watch.Stop();
					//	WriteLogHandle("Kết thúc đọc file, Total: " + watch.Elapsed.TotalMinutes + " minutes");
					//});
				}
				else
				{
					WriteLogHandle("Không có file Pre-Release nào chưa được đọc.");
				}
			}
			catch (Exception ex)
			{
				WriteLogHandle("Đọc file bị lỗi. " + ex.Message);
				fileDownload.Status = FileDownloadStatus.Read_Error;
				if (UpdateFileDownload(fileDownload))
				{
					WriteLogHandle("Update status file success");
				}
				else
				{
					WriteLogHandle("Update status file fail");
				}
				WebIsBusy = false;
				watch.Stop();
				WriteLogHandle("Kết thúc đọc file, Total: " + watch.Elapsed.TotalMinutes + " minutes");
			}
		}
	}
}
