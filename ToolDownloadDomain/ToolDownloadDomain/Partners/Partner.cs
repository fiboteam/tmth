﻿using DAO;
using DAO.Interface;
using DTO.Objects;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Utility;
using System.Configuration;

using System.IO.Compression;

namespace ToolDownloadDomain
{
	public class Partner : IPartner
	{
		private readonly IFileDownload _fileDownloadDown = DataAccess.FileDownload;
		private readonly IDomainNotBelongVN _domainNotBelongVN = DataAccess.DomainNotBelongVN;
		protected MyWebClient _webClient;
		protected AutoResetEvent _autoResetEvent;
		protected FileDownload file;
		private bool _webIsBusy = false;
		protected HtmlDocument PartnerHtmlDocument { get; set; }
		protected Xpath _xPath { get; set; }
		private string _id { get; set; }

		public delegate void WriteLog(string Message);
		public event WriteLog WriteLogEvent;

		public delegate void WritePropress(int percent);
		public event WritePropress WritePropressEvent;

		public delegate void WriteMaxPropress(int max);
		public event WriteMaxPropress WriteMaxPropressEvent;

		public delegate void WriteInsertPropress();
		public event WriteInsertPropress WriteInsertPropressEvent;

		public bool WebIsBusy 
		{
			get { return _webClient.IsBusy; }
			set { _webIsBusy = value; }
		}

		public Partner(string id)
		{
			_webClient = new MyWebClient();
			_webClient.DownloadStringCompleted += DownLoadHtmlCompleted;
			_webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler((sender, e) => DownLoading(sender, e, file));
			_webClient.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler((sender, e) => DownLoadCompleted(sender, e, file));

			
			_id = id;
			PartnerHtmlDocument = new HtmlDocument();
			
			var jOb = JObject.Parse(File.ReadAllText(Directory.GetCurrentDirectory() + @"\Xpath\Xpath.json"));
			string namePartner = ConfigurationManager.AppSettings["Partner"].ToString();
			JArray xpaths = (JArray)jOb[namePartner];
			var _xpaths = xpaths.ToObject<List<Xpath>>();
			_xPath = _xpaths.Where(p => p.Id == _id).Select(p => p).First();

			_autoResetEvent = new AutoResetEvent(false);
			//_webClient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");
			//_webClient.Headers[HttpRequestHeader.UserAgent] = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)";
			//_webClient.Proxy = new WebProxy(new Uri("http://proxy.oursite.com:8080"), true);
			ServicePointManager.Expect100Continue = true;
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
			ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
		}
		protected void WriteMaxProgressHandle(int value)
		{
			if (WriteMaxPropressEvent != null)
				WriteMaxPropressEvent(value);
		}
		protected void WriteInsertPropressHandle()
		{
			if (WriteInsertPropressEvent != null)
				WriteInsertPropressEvent();
		}
		/// <summary>
		/// Viết log ra view
		/// </summary>
		/// <param name="Message"></param>
		protected void WriteLogHandle(string Message)
		{
			if (WriteLogEvent != null)
				WriteLogEvent(Message);
		}
		/// <summary>
		/// % bar
		/// </summary>
		/// <param name="percent"></param>
		protected void WriteProgressHandle(int percent)
		{
			if (WritePropressEvent != null)
				WritePropressEvent(percent);
		}
		/// <summary>
		/// Download source code của web partner
		/// </summary>
		public virtual void DownLoadHtml()
		{
			try
			{
				//_webClient.DownloadStringAsync(new Uri(_xPath.LinkDownload));
				//ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
				
				var source = _webClient.DownloadString(new Uri(_xPath.LinkDownload));
				if (!string.IsNullOrEmpty(source))
				{
					PartnerHtmlDocument.LoadHtml(source);
					DownloadFiles();
				}
				else
				{
					WriteLogHandle("Không vào được web của partner");
				}
			}
			catch(Exception ex)
			{
				WriteLogHandle("Download HTML lỗi: " + ex.Message);
			}
		}
		/// <summary>
		/// Download source hoàn tất
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public virtual void DownLoadHtmlCompleted(object sender, DownloadStringCompletedEventArgs e)
		{
			try
			{
				if (e.Error == null && !e.Cancelled)
				{
					PartnerHtmlDocument.LoadHtml(e.Result);
					DownloadFiles();
				}
				else
				{
					WriteLogHandle("Không vào được web của partner");
				}
			}
			catch (Exception ex)
			{
				WriteLogHandle("Download HTML lỗi: "+ex.Message);
			}
		}
		/// <summary>
		/// Lấy đường dẫn file đã download nhưng chưa đọc từ server
		/// </summary>
		/// <returns></returns>
		public FileDownload GetFileDownload()
		{
			return _fileDownloadDown.GetFileBy((short)FileDownloadStatus.New, _xPath.Name, _xPath.Id);
		}
		/// <summary>
		/// Read file đã download nhưng chưa đọc
		/// </summary>
		public virtual void ReadFileDownload()
		{
			WriteLogHandle("Thực hiện đọc file");
			FileDownload fileDownload = GetFileDownload();
			if(fileDownload!=null)
			{
				WriteLogHandle("Lấy được 1 file chưa đọc: " + fileDownload.FileName);
			}
			else
			{
				WriteLogHandle("Tất cả file đã được đọc.");
			}
			WriteLogHandle("Kết thúc đọc file");
		}
		/// <summary>
		/// Lưu thông tin file mới download lên server
		/// </summary>
		/// <param name="file"></param>
		/// <returns></returns>
		public bool InsertFileDownload(FileDownload file)
		{
			string remark = "";
			var result = _fileDownloadDown.Insert(file, ref remark);
			//if(remark!=string.Empty)
			//{
			//	WriteLogHandle(remark);
			//}
			WriteLogHandle(remark);
			return result;
		}
		/// <summary>
		/// Thực hiện download file 
		/// </summary>
		public virtual void DownloadFiles()
		{
			try
			{
				/*Kiểm tra folder ngày xem có chưa*/
				string folderToday = DateTime.Now.ToString("yyyyMMdd");
				string pathFolderToday = Directory.GetCurrentDirectory() + @"\Download\" + folderToday;
				if (!Directory.Exists(pathFolderToday))
				{
					Directory.CreateDirectory(pathFolderToday);
				}
				file = new FileDownload();
				file.Title = GetContent();
				//Url
				file.LinkDownload = GetUrl();
				file.Origin = _xPath.Name;
				file.HomePage = _xPath.HomePage;
				file.Query = _xPath.Value;
				file.Type = _xPath.Id;
				//file name saved
				Regex regex = new Regex(@"[^/][\w.-]{1,}$");
				var match = regex.Match(file.LinkDownload);
				if (match.Success)
				{
					file.FileName = match.Value;
					file.LocalPath = pathFolderToday + @"\" +DateTime.Now.ToString("ddMMHHmmss_")+ file.FileName;
					WriteLogHandle("Start download: " + file.LinkDownload);
					//using (WebClient wc = new WebClient())
					//{
					//_webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler((sender, e) => DownLoading(sender, e, file));
					//_webClient.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler((sender, e) => DownLoadCompleted(sender, e, file));
					_webClient.DownloadFileAsync(new System.Uri(file.LinkDownload), file.LocalPath);
						//wc.DownloadFile(new System.Uri(file.LinkDownload), file.LocalPath);
					//}
				}
			}
			catch
			{
				WriteLogHandle("Không tìm thấy file để download");
			}
		}
		/// <summary>
		/// Get tên file sắp download
		/// </summary>
		/// <returns></returns>
		public virtual string GetContent()
		{
			if (PartnerHtmlDocument != null)
			{
				var node = PartnerHtmlDocument.DocumentNode.SelectNodes(_xPath.Value);
				if (node != null)
				{
					string date = node.FirstOrDefault().InnerText;
					return date.TrimEnd().TrimStart();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// Get url dùng để download
		/// </summary>
		/// <returns></returns>
		public virtual string GetUrl()
		{
			if (PartnerHtmlDocument != null)
			{
				var node = PartnerHtmlDocument.DocumentNode.SelectNodes(_xPath.Value);
				if (node != null)
				{
					string link = node.FirstOrDefault().Attributes["href"].Value;
					return _xPath.HomePage + link.TrimEnd().TrimStart();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// Đang download
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <param name="ob"></param>
		public virtual void DownLoading(object sender, DownloadProgressChangedEventArgs e, FileDownload ob)
		{
			WriteProgressHandle(e.ProgressPercentage);
		}
		/// <summary>
		/// Download thành công
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		/// <param name="ob"></param>
		public virtual void DownLoadCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e, FileDownload ob)
		{
			if (!e.Cancelled && e.Error == null)
			{
				WriteLogHandle(ob.Type + " Download Completed: " + ob.LocalPath);
				//string contentFile;
				//using (StreamReader reader = new StreamReader(ob.LocalPath))
				//{
				//	contentFile = reader.ReadToEnd();
				//}
				//Regex regex = new Regex(@"[\w.-][\n\r]+");
				//var matchs = regex.Matches(contentFile);
				//WriteLogHandle(matchs.Count + " domains");
				/*Insert record len6 db*/
				if (InsertFileDownload(ob))
				{
					WriteLogHandle("Insert file thành công");
				}
				else
				{
					WriteLogHandle("Insert file thất bại");
				}
			}
			WriteLogHandle("Kết thúc download file");
			///Sau khi insert file xong thì thực hiện đọc file luôn
			//ReadFileDownload();
		}


		public bool InsertDomain(DomainNotBelongVN domain)
		{
			return _domainNotBelongVN.Insert(domain);
		}


		public bool UpdateFileDownload(FileDownload file)
		{
			return _fileDownloadDown.Update(file);
		}


		public bool UnFileRar(string pathFileRar, out string destinationFolder)
		{
			try
			{
				WriteLogHandle("Thực hiện giải nến file Rar");
				destinationFolder = pathFileRar.Remove(pathFileRar.LastIndexOf('.'));
				System.Diagnostics.Process p = new System.Diagnostics.Process();
				p.StartInfo.CreateNoWindow = true;
				p.StartInfo.UseShellExecute = false;
				p.StartInfo.FileName = ConfigurationManager.AppSettings["PathWinrar"].ToString();
				p.StartInfo.Arguments = string.Format(@"x -s ""{0}"" *.* ""{1}\""", pathFileRar, destinationFolder);
				p.Start();
				p.WaitForExit();
				WriteLogHandle("Thực hiện giải nến file thành công: " + destinationFolder);
				File.Delete(pathFileRar);
				return true;
			}
			catch
			{
				WriteLogHandle("Thực hiện giải nến file thất bại");
				destinationFolder = "";
				return false;
			}
		}

		public bool UnFileZip(string pathFileRar, out string destinationFolder)
		{
			try
			{
				WriteLogHandle("Thực hiện giải nến file Zip");
				destinationFolder = pathFileRar.Remove(pathFileRar.LastIndexOf('.'));
				System.Diagnostics.Process p = new System.Diagnostics.Process();
				p.StartInfo.CreateNoWindow = true;
				p.StartInfo.UseShellExecute = false;
				p.StartInfo.FileName = ConfigurationManager.AppSettings["PathWinZip"].ToString();
				p.StartInfo.Arguments = string.Format(@"x -s ""{0}"" *.* ""{1}\""", pathFileRar, destinationFolder);
				p.Start();
				p.WaitForExit();
				WriteLogHandle("Thực hiện giải nến file thành công: " + destinationFolder);
				File.Delete(pathFileRar);
				return true;
			}
			catch
			{
				WriteLogHandle("Thực hiện giải nến file thất bại");
				destinationFolder = "";
				return false;
			}
		}


		public virtual bool PrepareFileAfterDownload(string path, int rowstart)
		{
			return false;
		}
	}
}
