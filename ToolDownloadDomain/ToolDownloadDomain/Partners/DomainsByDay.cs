﻿using DTO.Objects;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Utility;

namespace ToolDownloadDomain
{
	public class DomainsByDay:Partner
	{
		private List<string> _domainNames=new List<string>();
		public DomainsByDay(string id)
			: base(id)
		{
			//_webClient.Credentials = new NetworkCredential(_xPath.UserName, _xPath.Password);
			//_webClient.Encoding = Encoding.UTF8;
		}
		/// <summary>
		/// 
		/// </summary>
		public override void DownloadFiles()
		{
			WriteLogHandle("Thực hiện get domains từ DomainsByDay");
			_domainNames.Clear();
			try
			{
				/*Kiểm tra folder ngày xem có chưa*/
				string folderToday = DateTime.Now.ToString("yyyyMMdd");
				string pathFolderToday = Directory.GetCurrentDirectory() + @"\Download\" + folderToday;
				if (!Directory.Exists(pathFolderToday))
				{
					Directory.CreateDirectory(pathFolderToday);
				}
				file = new FileDownload();
				file.Title = GetContent();
				//Url
				file.LinkDownload = GetUrl();
				file.Origin = _xPath.Name;
				file.HomePage = _xPath.HomePage;
				file.Query = _xPath.Value;
				file.Type = _xPath.Id;
				file.FileName = file.Type+"_"+file.Title+".txt";
				file.LocalPath = pathFolderToday + @"\" +DateTime.Now.ToString("ddMMHHmmss_")+ file.FileName;

				var paging=GetPaging(file.LinkDownload);
				if(paging!=null)
				{
					paging.NextLinkTemplate = file.HomePage + @"/" + file.Title + @"/domains-{0}.html";
				}

				//List<Task> tasks = new List<Task>();

				WriteMaxProgressHandle(paging.TotalPage+1);
				WriteLogHandle("Thực hiện get domain từng trang...");
				Parallel.For(0, paging.TotalPage+1,
					new ParallelOptions() { MaxDegreeOfParallelism = 3 }, 
					i => 
				{
						string url = "";
						if (i == 0)
						{
							url = paging.From;
						}
						else
						{
							url = paging.NextLinkTemplate.Replace("{0}", i.ToString());
						}
						//tasks.Add(Task.Factory.StartNew(() =>
						//{
							string content = "";
							using (MyWebClient webClient = new MyWebClient())
							{
								//webClient.DownloadStringCompleted += DownloadStringCompleted;
								content = webClient.DownloadString(new Uri(url));
							}
							DownloadStringCompleted(content);
							//
						//}));
				});

				//Task.WaitAll(tasks.ToArray());
				WriteLogHandle("Get được tổng cộng " + _domainNames.Count + " domains .com");
				if (_domainNames.Count > 0)
				{
					File.WriteAllLines(file.LocalPath, _domainNames);

					if (InsertFileDownload(file))
					{
						WriteLogHandle("File Save as: " + file.LocalPath);
					}
					else
					{
						WriteLogHandle("Insert file thất bại");
					}
				}
				WriteLogHandle("Kết thúc get domain.");
			}
			catch(Exception ex)
			{
				WriteLogHandle("Phát sinh lỗi khi get domain: "+ex.Message);
			}
		}
		/// <summary>
		/// Lấy tất cả domain có trong trang
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DownloadStringCompleted(string content)
		{
			try
			{
				//if (e.Cancelled == false && e.Error == null)
				//{
				HtmlDocument html = new HtmlDocument();
				html.LoadHtml(content);

				var nodes = html.DocumentNode.SelectNodes(@"//div[@class='small-12 large-12 columns']/table[@class='responsive']/tbody/tr/td[1]/a[1]");
				//int count = nodes.Count;
				//Parallel.ForEach(nodes, domain =>
				//{
					
				//});
				foreach (var domain in nodes)
				{
					string name = domain.InnerText.TrimEnd().TrimStart();

					if (name.ToLower().EndsWith(".com") && name.Length > 4)
					{
						_domainNames.Add(name);

					}
				}
				//}
				//WriteLogHandle("Get được " + count + " domains từ page");
				WriteInsertPropressHandle();
			}
			catch (Exception ex)
			{
				WriteLogHandle("phát sinh lỗi từ trang: " + ex.Message);
			}
		}

		/// <summary>
		/// Lấy phân trang
		/// </summary>
		/// <param name="Url"></param>
		/// <returns></returns>
		private PagingDomainsByDay GetPaging(string Url)
		{
			try
			{
				PagingDomainsByDay paging = new PagingDomainsByDay(Url);
				WriteLogHandle("Bắt đầu tính phân trang domain từ link: " + Url);
				HtmlDocument html = new HtmlDocument();
				using (MyWebClient webClient = new MyWebClient())
				{
					string s = webClient.DownloadString(file.LinkDownload);
					html.LoadHtml(s);
				}

				var nodePageEnd = html.DocumentNode.SelectNodes(@"//div[@class='pagination-centered']/ul[@class='pagination']/li[last()]/a[1]");
				paging.End = nodePageEnd.FirstOrDefault().Attributes["href"].Value;
				paging.TotalPage=int.Parse(Regex.Match(paging.End, @"-([\d]+).html$").Groups[1].Value);
				WriteLogHandle(string.Format("Tìm được tổng cộng:{0} trang.",paging.TotalPage));
				return paging;
			}
			catch
			{
				return null;
			}
		}
	}

	public class PagingDomainsByDay
	{
		public string NextLinkTemplate { get; set; }
		public string From { get; set; }
		public string End { get; set; }
		public int TotalPage { get; set; }
		public PagingDomainsByDay(string from)
		{
			From = from;
		}
	}
}
