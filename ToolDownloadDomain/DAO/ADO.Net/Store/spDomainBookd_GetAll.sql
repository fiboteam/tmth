use TMTH
go
alter procedure [dbo].[spDomainBooked_GetAll]
as
begin
	(
		select booked.DomainBookedId,booked.DomainBookedName,booked.SaleManId,DepositPrice,CustomerInfomation,SaleName,DomainBookedStatus,
		case when tblDomainNotBelongVN.AuctionEndDate IS Not null then tblDomainNotBelongVN.AuctionEndDate else tblDomainNotBelongVN.ReleaseDate end as CaptureDate,
		null as [ExpireDate],
		tblDomainNotBelongVN.UpdatedDate as UpdatedDate
		from tblDomainBooked as booked
		inner join tblDomainNotBelongVN on booked.DomainIdRelated=tblDomainNotBelongVN.DomainID and booked.TableRelated='tblDomainNotBelongVN'
		inner join Salesman on booked.SaleManId=Salesman.SaleId
	)
	union
	(
		
		select booked.DomainBookedId,booked.DomainBookedName,booked.SaleManId,DepositPrice,CustomerInfomation,SaleName,DomainBookedStatus,
		DATEADD(DAY,20,tblDomainVN.ExpireDate) as CaptureDate, 
		case when tblDomainVN.[ExpireDate] = '1900-01-01 00:00:00' then null else tblDomainVN.[ExpireDate] end as [ExpireDate],
		tblDomainVN.UpdatedDate as UpdatedDate
		from tblDomainBooked as booked
		inner join tblDomainVN on booked.DomainIdRelated=tblDomainVN.DomainID and booked.TableRelated='tblDomainVN'
		inner join Salesman on booked.SaleManId=Salesman.SaleId
	)
	order by CaptureDate,ExpireDate 
end