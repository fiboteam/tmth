use TMHT
go
alter procedure [dbo].[spDomainNotBelongVN_BulkInsertDaily]
as
begin
	declare @DAteTimeBegin datetime =getdate()
	declare @count int
	/*table chứa log*/
	declare @tbl_log table( ChangeType VARCHAR(100),ID bigint)
	/*Lấy 1 file chưa download nằm trên cùng*/
	declare @temp_fileNew table (FileDownloadId bigint,Origin nvarchar(255),Type nvarchar(255),LocalPath nvarchar(255))
	
	insert into @temp_fileNew
	select top 1 FileDownloadId,Origin,Type,LocalPath from tblFileDownload
	where Status=1
	order by FileDownloadId asc
	
	/*Lấy thông tin file download ra variable để dể thực hiện*/
	declare @id bigint = (select FileDownloadId from @temp_fileNew)
	declare @origin nvarchar(255)=(select Origin from @temp_fileNew)
	declare @type nvarchar(255)=(select Type from @temp_fileNew)
	declare @path nvarchar(255) =(select LocalPath from @temp_fileNew)
	declare @sql varchar(max)
	
	/*xoá table tạm*/
	truncate table tbldomainnotbelongVN_Temp
	
	if(@id>0)
	begin
		/*update status file là đang đọc*/
		update tblFileDownload set Status=2,UpdatedDate=GETDATE()
		where FileDownloadId in
		(
			select FileDownloadId from @temp_fileNew
		)
		
		/*Bulk insert từ file vào table tạm*/
		if(@origin='NameJet' and @type='Pre-Release')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_PreRelease_NameJet FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with (DATAFILETYPE = ''char'', FIELDTERMINATOR = '','' ,  ROWTERMINATOR = ''\n'' )';
			
			begin try
			
				exec(@sql)
				
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.ReleaseDate = Source.ReleaseDate,
				--Target.Bid = ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) ,
				Target.Bid =case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,ReleaseDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.ReleaseDate,case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null);
				
				
				merge into tblDomainNotBelongVN_Compare as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.ReleaseDate = Source.ReleaseDate,
				Target.Bid =case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end
				when not matched by target and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,ReleaseDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note,HaveDomainVn) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.ReleaseDate,case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null,3)
				output $action,Inserted.DomainId into @tbl_log;
				
				
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='NameJet' and @type='Pending-Delete')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_PendingDelete_NameJet FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with ( ROWTERMINATOR = ''\n'' )';
			
			begin try
			
				exec(@sql)
				
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched and Target.Origin=@origin then 
				update set Target.UpdatedDate=getdate(),DomainType=@type
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,getdate(),getdate(),convert(varchar(8),getdate(),112),0,null);
				
				merge into [tblDomainNotBelongVN_Compare] as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched and Target.Origin=@origin then 
				update set Target.UpdatedDate=getdate(),DomainType=@type
				when not matched by target and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note,HaveDomainVn) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,getdate(),getdate(),convert(varchar(8),getdate(),112),0,null,3)
				output $action,Inserted.DomainId into @tbl_log;
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='NameJet' and @type='Auctions-Listings')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_Auctions_Listings_NameJet FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with (DATAFILETYPE = ''char'',FIRSTROW = 1, FIELDTERMINATOR = '','' ,  ROWTERMINATOR = ''\n'' )';
			
			begin try
			
				exec(@sql)
				
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.AuctionEndDate=Source.AuctionEndDate
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,AuctionEndDate,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.AuctionEndDate,getdate(),getdate(),convert(varchar(8),getdate(),112),1,null);
				
				merge into [tblDomainNotBelongVN_Compare] as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.AuctionEndDate=Source.AuctionEndDate
				when not matched by target and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,AuctionEndDate,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note,HaveDomainVn) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.AuctionEndDate,getdate(),getdate(),convert(varchar(8),getdate(),112),1,null,3)
				output $action,Inserted.DomainId into @tbl_log;
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='SnapNames' and @type='InAuction')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_InAuction_SnapNames FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with ( FORMATFILE = ''F:\Apps\SnapNames\Format\InAuctionSnapNames.Fmt'')';
			
			begin try
			
				exec(@sql)
				
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.AuctionEndDate = Source.AuctionEndDate,
				Target.Bid = case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,AuctionEndDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.AuctionEndDate,case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null);
				
				merge into [tblDomainNotBelongVN_Compare] as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.AuctionEndDate = Source.AuctionEndDate,
				Target.Bid = case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end
				when not matched by target and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,AuctionEndDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note,HaveDomainVn) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.AuctionEndDate,case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null,3)
				output $action,Inserted.DomainId into @tbl_log;
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='SnapNames' and @type='AvailableSoon')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_AvailableSoon_SnapNames FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with ( FORMATFILE = ''F:\Apps\SnapNames\Format\AvailableSoonSnapNames.Fmt'')';
			
			begin try
			
				exec(@sql)
				
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.ReleaseDate = Source.Origin+' '+Source.DomainType,
				Target.Bid = case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,ReleaseDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.Origin+' '+Source.DomainType,case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null);
				
				merge into [tblDomainNotBelongVN_Compare] as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.ReleaseDate = Source.Origin+' '+Source.DomainType,
				Target.Bid = case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end
				when not matched by target and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,ReleaseDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note,HaveDomainVn) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.Origin+' '+Source.DomainType,case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null,3)
				output $action,Inserted.DomainId into @tbl_log;
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='SnapNames' and @type='Expiring')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_Expiring_SnapNames FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with ( FORMATFILE = ''F:\Apps\SnapNames\Format\ExpiringSnapNames.Fmt'')';
			
			begin try
			
				exec(@sql)
				
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName)) 
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.AuctionEndDate = Source.AuctionEndDate,
				Target.Bid = case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,AuctionEndDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.AuctionEndDate,case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null);
				
				merge into [tblDomainNotBelongVN_Compare] as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName)) 
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.AuctionEndDate = Source.AuctionEndDate,
				Target.Bid = case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end
				when not matched by target and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,AuctionEndDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note,HaveDomainVn) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.AuctionEndDate,case when ISNUMERIC(replace(LTRIM(RTRIM(Source.Bid)),'$',''))=1 and Source.Bid not like '%,%' then ROUND(CAST(replace(LTRIM(RTRIM(Source.Bid)),'$','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null,3)
				output $action,Inserted.DomainId into @tbl_log;
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='GoDaddy' and @type='AllListing')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_AllListing_GoDaddy FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with ( FIRSTROW=2,FORMATFILE = ''F:\Apps\NamJet\Format\AllListingGoDaddy.Fmt'')';
			
			begin try
			
				exec(@sql)
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.AuctionType=Source.AuctionType,
				--Target.TimeLeft = replace(replace(Source.TimeLeft,'(PST)',''),'(PDT)','') ,
				Target.TimeLeft=case when Source.TimeLeft like '%(PST)' then DATEADD(Hour,15, CONVERT(datetime,replace(Source.TimeLeft,'(PST)','')))  when  Source.TimeLeft like '%(PDT)' then DATEADD(Hour,14, CONVERT(datetime,replace(Source.TimeLeft,'(PDT)',''))) else CONVERT(datetime,Source.TimeLeft) end,
				Target.Price=case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.Price)),'$',''),',',''))=1 and Source.Price not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.Price)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				Target.Bids = case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.Bids)),'$',''),',',''))=1 and Source.Bids not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.Bids)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				Target.DomainAge=Source.DomainAge,
				Target.Traffic=Source.Traffic,
				Target.ValuationPrice=case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.ValuationPrice)),'$',''),',',''))=1 and Source.ValuationPrice not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.ValuationPrice)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,AuctionType,TimeLeft,Price,Bids,DomainAge,Traffic,ValuationPrice,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.AuctionType,case when Source.TimeLeft like '%(PST)' then DATEADD(Hour,15, CONVERT(datetime,replace(Source.TimeLeft,'(PST)','')))  when  Source.TimeLeft like '%(PDT)' then DATEADD(Hour,14, CONVERT(datetime,replace(Source.TimeLeft,'(PDT)',''))) else CONVERT(datetime,Source.TimeLeft) end,case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.Price)),'$',''),',',''))=1 and Source.Price not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.Price)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.Bids)),'$',''),',',''))=1 and Source.Bids not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.Bids)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				Source.DomainAge,
				Source.Traffic,
				case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.ValuationPrice)),'$',''),',',''))=1 and Source.ValuationPrice not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.ValuationPrice)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null);
				
				merge into [tblDomainNotBelongVN_Compare] as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Origin=@origin,DomainType=@type,
				Target.AuctionType=Source.AuctionType,
				--Target.TimeLeft = replace(replace(Source.TimeLeft,'(PST)',''),'(PDT)','') ,
				Target.TimeLeft=case when Source.TimeLeft like '%(PST)' then DATEADD(Hour,15, CONVERT(datetime,replace(Source.TimeLeft,'(PST)','')))  when  Source.TimeLeft like '%(PDT)' then DATEADD(Hour,14, CONVERT(datetime,replace(Source.TimeLeft,'(PDT)',''))) else CONVERT(datetime,Source.TimeLeft) end,
				Target.Price=case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.Price)),'$',''),',',''))=1 and Source.Price not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.Price)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				Target.Bids = case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.Bids)),'$',''),',',''))=1 and Source.Bids not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.Bids)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				Target.DomainAge=Source.DomainAge,
				Target.Traffic=Source.Traffic,
				Target.ValuationPrice=case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.ValuationPrice)),'$',''),',',''))=1 and Source.ValuationPrice not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.ValuationPrice)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end
				when not matched by target and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,AuctionType,TimeLeft,Price,Bids,DomainAge,Traffic,ValuationPrice,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note,HaveDomainVn) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,Source.AuctionType,case when Source.TimeLeft like '%(PST)' then DATEADD(Hour,15, CONVERT(datetime,replace(Source.TimeLeft,'(PST)','')))  when  Source.TimeLeft like '%(PDT)' then DATEADD(Hour,14, CONVERT(datetime,replace(Source.TimeLeft,'(PDT)',''))) else CONVERT(datetime,Source.TimeLeft) end,case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.Price)),'$',''),',',''))=1 and Source.Price not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.Price)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.Bids)),'$',''),',',''))=1 and Source.Bids not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.Bids)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				Source.DomainAge,
				Source.Traffic,
				case when ISNUMERIC(replace(replace(LTRIM(RTRIM(Source.ValuationPrice)),'$',''),',',''))=1 and Source.ValuationPrice not like '%,%' then ROUND(CAST(replace(replace(LTRIM(RTRIM(Source.ValuationPrice)),'$',''),',','') AS decimal(10,2)), 2) else -1.00 end,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null,3)
				output $action,Inserted.DomainId into @tbl_log;
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='DomainsByDay' and @type='DomainsByDay')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_DomainsByDay_DomainsByDay FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with ( FORMATFILE = ''F:\Apps\NamJet\Format\DomainsByDay.Fmt'')';
			
			begin try
			
				exec(@sql)
				
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate()
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null)
				output $action,Inserted.DomainId into @tbl_log;
				
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='DomainChanges')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_DomainChanges_DomainChanges FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with ( FORMATFILE = ''F:\Apps\NamJet\Format\DomainChanges.Fmt'')';
			
			begin try
			
				exec(@sql)
				
				merge into tbldomainnotbelongVN as Target
				using tbldomainnotbelongVN_Temp as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate(),Target.Server=Source.Server,Target.Note=Source.Note
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note,Server) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,
				getdate(),getdate(),convert(varchar(8),getdate(),112),0,Source.Note,Source.Server)
				output $action,Inserted.DomainId into @tbl_log;
							
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		if(@origin='DomainsByDay' and @type='DownloadAll')
		begin
			set @sql='BULK INSERT vw_bulk_insert_DomainNotBelongVN_DomainsByDay_DomainsByDay FROM ''';
			set @sql=@sql+ @path ;
			set @sql=@sql+''' with ( FORMATFILE = ''F:\Apps\DownloadAllDomain\FileFormat\DomainsByDay.Fmt'')';
			
			begin try
			
				exec(@sql)
				
				merge into [tblDomainNotBelongVN_AllInPartner] as Target
				using (select distinct DomainName from tblDomainNotBelongVN_Temp) as Source
				on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				when matched then 
				update set Target.UpdatedDate=getdate()
				when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				insert (DomainName,Origin,DomainType,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,
				getdate(),getdate(),convert(varchar(8),getdate(),112),1,null)
				output $action,Inserted.DomainId into @tbl_log;
				
				--merge into tbldomainnotbelongVN as Target
				--using (select distinct DomainName from tblDomainNotBelongVN_Temp) as Source
				--on Target.DomainName=LTRIM(RTRIM(Source.DomainName))
				--when matched then 
				--update set Target.UpdatedDate=getdate()
				--when not matched and (Len(LTRIM(RTRIM(Source.DomainName)))>0) then
				--insert (DomainName,Origin,DomainType,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note) 
				--values (LTRIM(RTRIM(Source.DomainName)),@origin,@type,
				--getdate(),getdate(),convert(varchar(8),getdate(),112),1,null);
				
				/*update status file là đã đọc xong*/
				update tblFileDownload set Status=3,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
			end try
			begin catch
				/*update status file là lỗi*/
				update tblFileDownload set Status=4,UpdatedDate=GETDATE()
				where FileDownloadId in
				(
					select FileDownloadId from @temp_fileNew
				)
				print('Bulk bi loi: '+error_message())
			end catch
		end
		--------------------------------------------------------------------
		/*Thêm log*/
		insert into tblLogDownloadAndCompare (LogType,[Origin],[Group],TotalDomain,ToltalInserted,ToltalUpdated,ToltalDeleted,TimeElapsed,IdRelated,note)
		select 1,@origin,@type,(select COUNT(DomainID) from tblDomainNotBelongVN_Temp),[INSERT],[UPDATE],[DELETE],cast((GETDATE() - @DAteTimeBegin) as time),@id,error_message()
		from @tbl_log as tbll
		pivot
		(
			count(ID)
			for ChangeType in ([INSERT],[UPDATE],[DELETE])
		) as tblpivot
	end

end