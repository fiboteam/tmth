USE [TMTH]
GO
/****** Object:  StoredProcedure [dbo].[spDomainBooked_Add]    Script Date: 04/11/2016 16:39:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spDomainBooked_Add] --'1kho.net',12700,'dat thu viet name vn',1
	@Domain nvarchar(255),
	@DepositPrice decimal(12,2),
	@Infomation nvarchar(max),
	@SaleId bigint,
	@DomainBookedStatus tinyint,
	@ExpireDate datetime,
	@CaptureDate datetime
as
begin
	declare @idRelated bigint = 0
	declare @tableRelated nvarchar(255) = ''
	
	if exists (select 1 from tblDomainBooked where DomainBookedName=@Domain)
	begin
		select 0
		return
	end
	-------------------------------------------------xác định là domain QT hay vietnam
	If(@Domain like '%.vn')
	begin
		-----------------------------------là vietnam thì search xem có chưa,  
		set @tableRelated='tblDomainVN'
		------------------------------
		if exists (select 1 from tblDomainVN where DomainName=@Domain)
		begin
			---------------------có rồi thì lấy ID luôn
			select @idRelated=DomainID from tblDomainVN where DomainName=@Domain
		end
		else
		begin
			---------------------chua thì insert
			insert into tblDomainVN (DomainName, CreatedDate, UpdatedDate,[ExpireDate])
			values (@Domain,GETDATE(),GETDATE(),case when @ExpireDate IS null then '' else @ExpireDate end)
			------------------------------------
			set @idRelated=@@IDENTITY
		end
	end
	else if(LEN(@Domain)>1)
	begin
		--------------------------------------là QT thì search xem có chưa, chua thì insert, có rồi thì lấy ID luôn
		set @tableRelated='tblDomainNotBelongVN'
		------------------------------
		if exists (select 1 from tblDomainNotBelongVN where DomainName=@Domain)
		begin
			---------------------có rồi thì lấy ID luôn
			select @idRelated=DomainID from tblDomainNotBelongVN where DomainName=@Domain
			
			if(@CaptureDate is not null and @idRelated>0)
			begin
				update tblDomainNotBelongVN set ReleaseDate=@CaptureDate where DomainID=@idRelated
			end
		end
		else
		begin
			---------------------chua thì insert
			insert into tblDomainNotBelongVN (DomainName,Origin,DomainType,ReleaseDate,Bid,CreatedDate,UpdatedDate,ShortCreatedDate,ForSale,Note)
			values (@Domain,'DomainBooked','DomainBooked',@CaptureDate,null,GETDATE(),GETDATE(),CONVERT(varchar(8),GETDATE(),112),1,null)
			------------------------------------
			set @idRelated=@@IDENTITY
		end
	end
	---------------------------------------------------------Có id rồi thì insert vào table domainbooked
	if(@idRelated>0)
	begin
		insert into tblDomainBooked (DomainBookedName,DepositPrice,SaleManId,CustomerInfomation,DomainIdRelated,TableRelated,DomainBookedStatus,[ExpireDate],CaptureDate)
		values (@Domain,@DepositPrice,@SaleId,@Infomation,@idRelated,@tableRelated,@DomainBookedStatus,@ExpireDate,@CaptureDate)
		-----------------
		select @@IDENTITY
	end
	else
	 select 0
end