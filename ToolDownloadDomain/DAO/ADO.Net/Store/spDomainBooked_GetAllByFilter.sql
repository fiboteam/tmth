USE [TMTH]
GO
/****** Object:  StoredProcedure [dbo].[spDomainBooked_GetAllByFilter]    Script Date: 04/11/2016 16:43:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spDomainBooked_GetAllByFilter]
	@domain nvarchar(255),
	@shortCaptureDate int,
	@shortExpireDate int,
	@SaleId bigint,
	@Infomation nvarchar(max),
	@Price decimal(12,2)
as
begin
	select * from
	(
		(
			select booked.DomainBookedId, booked.DomainBookedName,booked.SaleManId,DepositPrice,CustomerInfomation,SaleName,DomainBookedStatus,
			case when tblDomainNotBelongVN.AuctionEndDate IS Not null then tblDomainNotBelongVN.AuctionEndDate when tblDomainNotBelongVN.ReleaseDate IS Not null then tblDomainNotBelongVN.ReleaseDate else booked.CaptureDate end as CaptureDate,
			case when booked.[ExpireDate] = '1900-01-01 00:00:00' then null else booked.[ExpireDate] end as [ExpireDate],
			tblDomainNotBelongVN.UpdatedDate as UpdatedDate
			from tblDomainBooked as booked
			inner join tblDomainNotBelongVN on booked.DomainIdRelated=tblDomainNotBelongVN.DomainID and booked.TableRelated='tblDomainNotBelongVN'
			inner join Salesman on booked.SaleManId=Salesman.SaleId
		)
		union
		(
			select booked.DomainBookedId, booked.DomainBookedName,booked.SaleManId,DepositPrice,CustomerInfomation,SaleName,DomainBookedStatus,
			case when tblDomainVN.[ExpireDate] = '1900-01-01 00:00:00' and booked.[ExpireDate] IS null then booked.CaptureDate when booked.[ExpireDate] IS Not null then DATEADD(DAY,20,tblDomainVN.ExpireDate) when booked.[ExpireDate] IS Not null then DATEADD(DAY,20,booked.ExpireDate) else booked.CaptureDate end as CaptureDate,
			case when tblDomainVN.[ExpireDate] = '1900-01-01 00:00:00' and booked.[ExpireDate] IS null then null when tblDomainVN.[ExpireDate] IS Not null then tblDomainVN.[ExpireDate] when booked.[ExpireDate] IS Not null then booked.[ExpireDate] else null end as [ExpireDate],
			tblDomainVN.UpdatedDate as UpdatedDate
			from tblDomainBooked as booked
			inner join tblDomainVN on booked.DomainIdRelated=tblDomainVN.DomainID and booked.TableRelated='tblDomainVN'
			inner join Salesman on booked.SaleManId=Salesman.SaleId
		)
	) as tblsource
	where (convert(varchar(8),CaptureDate,112)=@shortCaptureDate or @shortCaptureDate=0)
	and (convert(varchar(8),ExpireDate,112)=@shortExpireDate or @shortExpireDate=0)
	and (SaleManId=@SaleId or @SaleId=0)
	and (CustomerInfomation like '%'+@Infomation+'%' or @Infomation='')
	and (DepositPrice=@Price or @Price=0)
	order by CaptureDate,ExpireDate 
end