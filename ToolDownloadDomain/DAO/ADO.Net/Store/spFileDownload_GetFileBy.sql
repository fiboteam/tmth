USE [TMHT]
GO
/****** Object:  StoredProcedure [dbo].[spFileDownload_GetFileBy]    Script Date: 03/07/2016 22:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER  proc [dbo].[spFileDownload_GetFileBy] --"NameJet","Pre-Release",1
	@name nvarchar(255),
	@type nvarchar(255),
	@status tinyint
as
begin
	declare @id bigint

	select top 1 @id=FileDownloadId from tblFileDownload
	where tblFileDownload.Origin=@name
	and Type=@type
	and Status=@status
	order by FileDownloadId asc
	if(@id>0)
	begin
		/*set status file đó là đang đọc*/
		update tblFileDownload set Status=2,UpdatedDate=getdate()
		where FileDownloadId=@id

		select * from tblFileDownload
		where FileDownloadId=@id
	end
end

--select top 100 * from tblDomainNotBelongVN
--order by domainid desc

