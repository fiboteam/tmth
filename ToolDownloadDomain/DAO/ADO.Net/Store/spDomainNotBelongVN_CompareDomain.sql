USE [TMTH]
GO
/****** Object:  StoredProcedure [dbo].[spDomainNotBelongVN_CompareDomain]    Script Date: 03/25/2016 11:54:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spDomainNotBelongVN_CompareDomain] --'NameJet','Pre-Release'
	@origin nvarchar(255),
	@type nvarchar(255),
	@shortdate int
as
begin
	/*
	@origin='' lấy hết
	@type='' lấy hết
	@shortdate=-1 lấy hết
	@shortdate=0 hôm nay
	*/
	if(@shortdate=0)
	begin
		set @shortdate=CONVERT(varchar(8),GETDATE(),112)
	end
	---------------------------------------------------------------
	declare @tbl_result table (DomainID bigint,DomainName nvarchar(255),Type nvarchar(255),DomainNameVN nvarchar(255))
	declare @tbl_resultPivot table (DomainID bigint,DomainName nvarchar(255),ComVn nvarchar(255),Vn nvarchar(255))
	declare @DAteTimeBegin datetime =getdate()
	----------------------------------------------------------------
	insert into @tbl_result
	select tblSource.DomainID,tblSource.DomainName,tblCross.Type,tblCross.DomainName VN
	from
	(
	select SUBSTRING(DomainName,0,CHARINDEX('.',DomainName,0)) as Prefix, tblCompare.DomainID,tblCompare.DomainName ,tblCompare.HaveDomainVn
	from tblDomainNotBelongVN_Compare as tblCompare
	where (CONVERT(varchar(8),tblCompare.UpdatedDate,112)= @shortdate or @shortdate=-1)
	and tblCompare.HaveDomainVn not in (2)
	and (tblCompare.Origin=@origin or @origin='')
	and (tblCompare.DomainType=@type or @type='')
	) as tblSource
	cross apply
	(
		select case when DomainName like '%.com.vn' then 'ComVn' else 'Vn' end as [Type] ,DomainName from tblDomainVN as tblvn
		where SUBSTRING(tblvn.DomainName,0,CHARINDEX('.',tblvn.DomainName,0))=tblSource.Prefix
		and DomainName like '%.vn'
	) as tblCross
	where HaveDomainVn=3
	or (HaveDomainVn=0 and [Type]='ComVn')
	or (HaveDomainVn=1 and [Type]='Vn')
	-------------------------------------
	insert into @tbl_resultPivot
	select DomainID,DomainName,ComVn,Vn 
	from @tbl_result as tblResult
	pivot
	(
		Max(tblResult.DomainNameVN)
		for [Type] in ([ComVn],[Vn])
	) as tblPivot
	-------------------------------------
	update tblDomainNotBelongVN_Compare
	set DomainDotVn= case when tblResult.Vn is not null then 1 else tblCompare.DomainDotVn end,
	DomainDotComDotVn = case when tblResult.ComVn is not null then 1 else tblCompare.DomainDotComDotVn end,
	HaveDomainVn = case when tblResult.Vn is not null and tblResult.ComVn is not null then 2 when tblResult.Vn is not null then 0  when tblResult.ComVn is not null then 1 else HaveDomainVn end,
	UpdatedDate=GETDATE()
	from tblDomainNotBelongVN_Compare as tblCompare
	inner join @tbl_resultPivot as tblResult on tblResult.DomainID=tblCompare.DomainId
	---------------------------------------
	declare @count int = @@ROWCOUNT
	---------------------------------------
	insert into tblLogDownloadAndCompare (LogType,Origin,[Group],TotalDomain,ToltalInserted,ToltalUpdated,ToltalDeleted,TimeElapsed,note,shortcreateddate)
	select 2,'spDomainNotBelongVN_CompareDomain',Type,COUNT(*) as TotalDomainVN,0,@count,0, cast((GETDATE() - @DAteTimeBegin) as time),error_message(),@shortdate
	from @tbl_result
	group by Type
end
