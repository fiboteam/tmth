USE [TMHT]
GO
/****** Object:  StoredProcedure [dbo].[spDomainNotBelongVN_AddUpdate]    Script Date: 03/07/2016 22:35:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spDomainNotBelongVN_AddUpdate]  
    -- Add the parameters for the stored procedure here 
    @id bigint = 0, 
    @domainname nvarchar(255), 
    @origin nvarchar(255), 
    @domaintype nvarchar(255), 
    @auctionsdate datetime, 
    @forsale bit, 
    @note nvarchar(1024) 
AS 
BEGIN 
    -- SET NOCOUNT ON added to prevent extra result sets from 
    -- interfering with SELECT statements. 
SET NOCOUNT ON; 
    if (@id <= 0) 
    begin 
		if((select COUNT(*) from tblDomainNotBelongVN where DomainName=@domainname and Origin=@origin and DomainType=@domaintype)>0)
		begin
			update tblDomainNotBelongVN set UpdatedDate=GETDATE() where DomainName=@domainname and Origin=@origin and DomainType=@domaintype
		end
		else
		begin
			insert into tblDomainNotBelongVN( 
					DomainName, 
					Origin, 
					DomainType, 
					AuctionsDate, 
					CreatedDate, 
					UpdatedDate, 
					ShortCreatedDate, 
					ForSale, 
					Note) 
			values(
					@domainname, 
					@origin, 
					@domaintype, 
					@auctionsdate, 
					GETDATE(), 
					GETDATE(), 
					CONVERT(varchar(8),GETDATE(),112), 
					@forsale, 
					@note
						) 
			set @id = @@identity 
        end
    end 
    else 
    begin 
        update tblDomainNotBelongVN set 
        DomainName = @domainname, 
        Origin = @origin, 
        DomainType = @domaintype, 
        AuctionsDate = @auctionsdate, 
        UpdatedDate = GETDATE(), 
        ForSale = @forsale, 
        Note = @note
        where DomainID = @id 
        
		set @id=@@ROWCOUNT
    end 
    -- Insert statements for procedure here 
    SELECT @id as DomainID 
END