use TMHT

USE [TMHT]
GO
/****** Object:  Table [dbo].[tblFileDownload]    Script Date: 03/21/2016 19:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFileDownload](
	[FileDownloadId] [bigint] IDENTITY(1,1) NOT NULL,
	[Origin] [nvarchar](255) NOT NULL,
	[Type] [nvarchar](255) NOT NULL,
	[HomePage] [nvarchar](255) NOT NULL,
	[LinkDownload] [nvarchar](255) NOT NULL,
	[Query] [varchar](1025) NOT NULL,
	[Title] [nvarchar](1024) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[LocalPath] [nvarchar](1024) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[ShortCreatedDate] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[FileDownloadId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO


drop table [tblDomainNotBelongVN_AllInPartner]
create table [dbo].[tblDomainNotBelongVN_AllInPartner]
(
	DomainID bigint primary key identity,
	DomainName nvarchar(255) not null,
	Origin nvarchar(255),
	DomainType nvarchar(255),
	AuctionEndDate datetime,
	ReleaseDate datetime,
	Bid decimal(10,2),
	CreatedDate datetime,
	UpdatedDate datetime,
	ShortCreatedDate int,
	ForSale bit,
	Note nvarchar(1024),
	AuctionType nvarchar(255),
	TimeLeft datetime,
	Price decimal(10,2),
	Bids decimal(10,2),
	DomainAge int,
	Traffic int,
	ValuationPrice decimal(10,2),
	[Server] nvarchar(255)
)

drop table [tblDomainNotBelongVN]
create table [dbo].[tblDomainNotBelongVN]
(
	DomainID bigint primary key identity,
	DomainName nvarchar(255) not null,
	Origin nvarchar(255),
	DomainType nvarchar(255),
	AuctionEndDate datetime,
	ReleaseDate datetime,
	Bid decimal(10,2),
	CreatedDate datetime,
	UpdatedDate datetime,
	ShortCreatedDate int,
	ForSale bit,
	Note nvarchar(1024),
)

alter table [tblDomainNotBelongVN]
add AuctionType nvarchar(255)

alter table [tblDomainNotBelongVN]
add TimeLeft datetime

alter table [tblDomainNotBelongVN]
add Price decimal(10,2)

alter table [tblDomainNotBelongVN]
add Bids decimal(10,2)

alter table [tblDomainNotBelongVN]
add DomainAge int

alter table [tblDomainNotBelongVN]
add Traffic int

alter table [tblDomainNotBelongVN]
add ValuationPrice decimal(10,2)

alter table [tblDomainNotBelongVN]
add [Server] nvarchar(255)

drop table [tblDomainNotBelongVN_Compare]
create table [dbo].[tblDomainNotBelongVN_Compare]
(
	DomainID bigint primary key identity,
	DomainName nvarchar(255),
	Origin nvarchar(255),
	DomainType nvarchar(255),
	AuctionEndDate datetime,
	ReleaseDate datetime,
	Bid nvarchar(255),
	CreatedDate datetime,
	UpdatedDate datetime,
	ShortCreatedDate int,
	ForSale bit,
	Note nvarchar(1024),
	AuctionType nvarchar(255),
	TimeLeft nvarchar(255),
	Price nvarchar(255),
	Bids nvarchar(255),
	DomainAge int,
	Traffic int,
	ValuationPrice nvarchar(255),
	[Server] nvarchar(255),
)

alter table [tblDomainNotBelongVN_Compare]
add HaveDomainVn smallint

alter table [tblDomainNotBelongVN_Compare]
add DomainDotVn smallint

alter table [tblDomainNotBelongVN_Compare]
add DomainDotComDotVn smallint

alter table [tblDomainNotBelongVN_Compare]
add Sales nvarchar(255)

CREATE INDEX index_DomainName
ON [tblDomainNotBelongVN_Compare] (DomainName)



drop table [tblDomainNotBelongVN_Temp]
create table [dbo].[tblDomainNotBelongVN_Temp]
(
	DomainID bigint primary key identity,
	DomainName nvarchar(255),
	Origin nvarchar(255),
	DomainType nvarchar(255),
	AuctionEndDate datetime,
	ReleaseDate datetime,
	Bid nvarchar(255),
	CreatedDate datetime,
	UpdatedDate datetime,
	ShortCreatedDate int,
	ForSale bit,
	Note nvarchar(1024),
	AuctionType nvarchar(255),
	TimeLeft nvarchar(255),
	Price nvarchar(255),
	Bids nvarchar(255),
	DomainAge int,
	Traffic int,
	ValuationPrice nvarchar(255),
	[Server] nvarchar(255)
)

go
create view vw_bulk_insert_DomainNotBelongVN_PreRelease_NameJet
as 
select DomainName,ReleaseDate,Bid from tblDomainNotBelongVN_Temp

go
create view vw_bulk_insert_DomainNotBelongVN_PendingDelete_NameJet
as 
select DomainName from tblDomainNotBelongVN_Temp

go
create view vw_bulk_insert_DomainNotBelongVN_Auctions_Listings_NameJet
as 
select DomainName,AuctionEndDate from tblDomainNotBelongVN_Temp

go
create view vw_bulk_insert_DomainNotBelongVN_InAuction_SnapNames
as 
select DomainName,Bid,AuctionEndDate from tblDomainNotBelongVN_Temp

go
create view vw_bulk_insert_DomainNotBelongVN_AvailableSoon_SnapNames
as 
select DomainName,Bid,Origin,DomainType from tblDomainNotBelongVN_Temp

go
create view vw_bulk_insert_DomainNotBelongVN_Expiring_SnapNames
as 
select DomainName,Bid,AuctionEndDate from tblDomainNotBelongVN_Temp

go
create view vw_bulk_insert_DomainNotBelongVN_AllListing_GoDaddy
as 
select DomainName,Note,AuctionType,TimeLeft,Price,Bids,DomainAge,Traffic,ValuationPrice from tblDomainNotBelongVN_Temp

go
create view vw_bulk_insert_DomainNotBelongVN_DomainsByDay_DomainsByDay
as 
select DomainName from tblDomainNotBelongVN_Temp

go
create view vw_bulk_insert_DomainNotBelongVN_DomainChanges_DomainChanges
as 
select DomainName,Note,[Server] from tblDomainNotBelongVN_Temp

go

/*Lưu log khi download domain và compare*/
drop table [tblLogDownloadAndCompare]
create table [dbo].[tblLogDownloadAndCompare]
(
	Id bigint primary key identity,
	IdRelated bigint,
	LogType int not null,
	Origin nvarchar(255) ,
	[Group] nvarchar(255) ,
	TotalDomain int,
	ToltalInserted int,
	ToltalUpdated int,
	ToltalDeleted int,
	LogStatus tinyint default 1,
	CreatedDate datetime default getdate(),
	ShortCreatedDate int default convert(varchar(8),getdate(),112),
	TimeElapsed time,
	Note nvarchar(255)
)


select * from vw_bulk_insert_DomainNotBelongVN_AllListing_GoDaddy

delete vw_bulk_insert_DomainNotBelongVN_InAuction_SnapNames


truncate table tblFileDownload
truncate table tblDomainNotBelongVN
truncate table tblDomainNotBelongVN_Temp
truncate table [tblLogDownloadAndCompare]

select * from tblFileDownload
order by FileDownloadId desc

select DomainName from tblDomainNotBelongVN
group by DomainName
having COUNT(*)>1

select DomainName from tblDomainNotBelongVN_Compare
group by DomainName
having COUNT(*)>1

select DomainType,COUNT(*) from tblDomainNotBelongVN
where ShortCreatedDate=20160324
group by DomainType

select DomainType,COUNT(*) from tblDomainNotBelongVN_Compare
where ShortCreatedDate=20160324
group by DomainType


--457
/*
AllListing	1502978
Auctions-Listings	1777
AvailableSoon	984362
DomainsByDay	672021
dotvndns.vn	217
DownloadAll	23799267
Expiring	615601
InAuction	8578
pavietnam.net	148
pavietnam.vn	775
Pending-Delete	80217
Pre-Release	522480
*/
select distinct DomainName from tblDomainNotBelongVN_Temp

select DomainName,COUNT(*) from tblDomainNotBelongVN_Temp
group by DomainName 
having COUNT(*)>1

select  * from tblDomainNotBelongVN
where DomainType='Auctions-Listings'
and Bid<0
and Origin='NameJet'

select * from tblDomainNotBelongVN_AllInPartner
select DomainName,COUNT(*) from tblDomainNotBelongVN_AllInPartner
group by DomainName
having COUNT(*)>1

select * from tblFileDownload
order by FileDownloadId desc

update tblFileDownload set Status=1
where FileDownloadId in (
644,
643,
642,
641,
640,
639,
638,
637,
636,
635,
634
)

exec [spDomainNotBelongVN_BulkInsertDaily]

update tblDomainNotBelongVN_Compare set HaveDomainVn=3
where ShortCreatedDate=20160323

exec [dbo].[spDomainNotBelongVN_CompareDomain] 'DomainChanges','DomainChanges'

exec [dbo].[spDomainNotBelongVN_CompareDomain] '','',20160324
--select CONVERT(varchar(8),GETDATE(),112)

exec [spDomainNotBelongVN_GetDomainHaveVn] 20160323,3,NameJet

exec [spDomainNotBelongVN_ReportDownloadDaily] 20160324

exec [spDomainNotBelongVN_Compare_ReportDownloadDaily] 20160324

select * from tblLogDownloadAndCompare

select * from tblDomainNotBelongVN_Compare
where havedomainvn <> 3
and Origin='NameJet' 
and DomainType='Pending-Delete'



select case when ISNUMERIC('10,200.00')=1 and '10,200.00' not like '%,%' then ROUND(CAST('10,200.00' AS decimal(10,2)), 2) else -1.00 end

select cast('03/10/2016 07:00 AM ' as datetime)


select * from tblDomainNotBelongVN
where DomainName in
(
	select DomainName from tblDomainNotBelongVN_Temp
)
and Origin='SnapNames' and DomainType='InAuction'

select * from tblDomainNotBelongVN_Temp
where DomainName not in
(
	select DomainName from tblDomainNotBelongVN
)

select * from tblDomainNotBelongVN
inner join tblDomainNotBelongVN_Temp on tblDomainNotBelongVN.DomainName=tblDomainNotBelongVN_Temp.DomainName

select DomainName,COUNT(*) from tblDomainNotBelongVN_Temp
group by DomainName
having COUNT(*)>1


DECLARE @utcdate DATETIME 
SET @utcdate = GetUTCDate()

select @utcdate




SELECT SYSDATETIME() AS SYSDATETIME
    ,SYSDATETIMEOFFSET() AS SYSDATETIMEOFFSET
    ,SYSUTCDATETIME() AS SYSUTCDATETIME
    --,CURRENT_TIMESTAMP AS CURRENT_TIMESTAMP
    ,GETDATE() AS GETDATE
    ,GETUTCDATE() AS GETUTCDATE;

SELECT CONVERT (date, SYSDATETIME())
    ,CONVERT (date, SYSDATETIMEOFFSET())
    ,CONVERT (date, SYSUTCDATETIME())
    ,CONVERT (date, CURRENT_TIMESTAMP)
    ,CONVERT (date, GETDATE())
    ,CONVERT (date, GETUTCDATE());
    
   
declare @InputUtcDateTime datetime2 = getdate()

declare @LocalDateTime datetime2 = dateadd(minute, datepart(TZoffset, sysdatetimeoffset()), @InputUtcDateTime)
print @LocalDateTime


--declare @dt datetime set @dt = '2011-11-06 07:11:22.000' select CAST(SWITCHOFFSET(CAST(@dt as varchar),'-08:00') AS DATETIME) AS PST
 
--declare @dt datetime set @dt = '2016-03-14 10:30:00' 
--declare @dt datetime set @dt = GETDATE() 
--select CAST(SWITCHOFFSET(CAST(@dt as varchar),'-14:00') AS DATETIME) AS PST      

--select CASE WHEN @dt BETWEEN  '11-mar-2007' and '4-nov-2007'    THEN DATEADD(hh, -7, @dt)
--     WHEN @dt BETWEEN  '9-mar-2008' and '2-nov-2008'       THEN DATEADD(hh, -7, @dt)
--     WHEN @dt BETWEEN  '8-mar-2009' and '1-nov-2009'     THEN DATEADD(hh, -7, @dt)
--     WHEN @dt BETWEEN '14-mar-2010' and '7-nov-2010'     THEN DATEADD(hh, -7, @dt)
--ELSE
--    DATEADD(hh, -8, @dt)
--End      

declare @dt nvarchar(50) = '2016-03-18 14:20:00.000 (PDT)'
--select DATEADD(Hour,15, CONVERT(datetime,replace(@dt,'(PST)','')))

select case when @dt like '%(PST)' then DATEADD(Hour,15, CONVERT(datetime,replace(@dt,'(PST)','')))  when  @dt like '%(PDT)' then DATEADD(Hour,14, CONVERT(datetime,replace(@dt,'(PDT)',''))) else CONVERT(datetime,@dt) end


select top 1000 * from tblDomainVN
order by DomainID desc


select top 1000 * from tblDomainNotBelongVN_Compare
where HaveDomainVn not in (2)



select DomainID,tblSource.DomainName,tblCross.DomainName as VN from
(
select top 1000 SUBSTRING(DomainName,0,CHARINDEX('.',DomainName,0)) as Prefix, tblCompare.DomainID,tblCompare.DomainName from tblDomainNotBelongVN_Compare as tblCompare
where CONVERT(varchar(8),UpdatedDate,112)=CONVERT(varchar(8),GETDATE(),112)
) as tblSource
cross apply
(
	select DomainName from tblDomainVN as tblvn
	where SUBSTRING(tblvn.DomainName,0,CHARINDEX('.',tblvn.DomainName,0))=tblSource.Prefix
	and DomainName like '%.vn'
) as tblCross

select DomainID,tblSource.DomainName,tblCross.DomainName as VN from
(
select top 1000 SUBSTRING(DomainName,0,CHARINDEX('.',DomainName,0)) as Prefix, tblCompare.DomainID,tblCompare.DomainName from tblDomainNotBelongVN_Compare as tblCompare
where CONVERT(varchar(8),UpdatedDate,112)=CONVERT(varchar(8),GETDATE(),112)
) as tblSource
cross apply
(
	select DomainName from tblDomainVN as tblvn
	where SUBSTRING(tblvn.DomainName,0,CHARINDEX('.',tblvn.DomainName,0))=tblSource.Prefix
	and DomainName like '%.vn'
) as tblCross


select tblCompare.DomainID,tblCompare.DomainName,tblVn.DomainName from tblDomainNotBelongVN_Compare as tblCompare
inner join tblDomainVN as tblVn on SUBSTRING(tblCompare.DomainName,0,CHARINDEX('.',tblCompare.DomainName,0))=SUBSTRING(tblVn.DomainName,0,CHARINDEX('.',tblVn.DomainName,0))
where tblVn.DomainName like '%.vn'
and  tblCompare.HaveDomainVn not in (2)


--select tblSource.DomainID,tblSource.DomainName,tblCross.[ComVn],tblCross.[Vn] as VN from

select cast ((DATEADD(SECOND,10,GETDATE())- GETDATE()) as time)







