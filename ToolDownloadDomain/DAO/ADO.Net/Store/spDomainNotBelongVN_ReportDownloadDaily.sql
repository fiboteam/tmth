use TMHT
go
alter procedure [dbo].[spDomainNotBelongVN_ReportDownloadDaily]
	@shortdate int
as
begin
	select Origin,[GROUP],TotalDomain ,
	ToltalInserted ,
	ToltalUpdated ,
	ToltalDeleted,
	isnull(note,'') as note
	from tblLogDownloadAndCompare
	where shortcreateddate=@shortdate
	and LogType=1
	order by Origin,[GROUP],createddate
end