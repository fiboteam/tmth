use TMHT
go
alter procedure [dbo].[spDomainNotBelongVN_GetDomainHaveVn] --'NameJet','Pre-Release'
	@shortdate int,
	@status smallint
as
begin
	/*
	@status 3: chi lay domain co vn
	@status -1: lấy tất cả domain
	*/
	select DomainID,DomainName,Origin,DomainType, case when AuctionEndDate is not null then AuctionEndDate when ReleaseDate is not null then ReleaseDate else null end as [Date],
	UpdatedDate , DomainDotVn,DomainDotComDotVn,Sales
	from tblDomainNotBelongVN_Compare
	where havedomainvn <> @status
	and CONVERT(varchar(8),UpdatedDate,112)=@shortdate
end