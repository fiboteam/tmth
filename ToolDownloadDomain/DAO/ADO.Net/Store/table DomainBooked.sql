use TMTH

drop table [tblDomainBooked]
create table [dbo].[tblDomainBooked]
(
	DomainBookedId bigint primary key identity,
	DomainBookedName nvarchar(255) not null,
	SaleManId bigint not null,
	DepositPrice decimal(12,2),
	CustomerInfomation nvarchar(max),
	Note nvarchar(max),
	Remark nvarchar(1024),
	DomainIdRelated bigint not null,
	TableRelated nvarchar(255) not null,
	CaptureDate Datetime, --lấy từ table relate  Nếu là domain VN: CaptureDay = Ngày hết hạn + 20, Nếu là domain QT: lúc import có ngày Capture Day thì update lại.
	[ExpireDate] datetime, --lấy tu table relate, whois
	DomainBookedStatus tinyint,
	CreatedDate datetime default getdate(),
	UpdatedDate datetime, --lấy từ table relate
	ShortCreatedDate int default convert(varchar(8),getdate(),112)
)

CREATE UNIQUE INDEX index_DomainBookedName
ON [tblDomainBooked] (DomainBookedName)

CREATE INDEX index_DomainIdRelated
ON [tblDomainBooked] (DomainIdRelated)

truncate table [tblDomainBooked]


--------------------------------
select top 1000 * from  tbldomainnotbelongvn

select * from tbldomainvn
where domainid in (53796,1366505)

select * from tbldomainnotbelongvn_compare
where havedomainvn=2

select * from tbldomainbooked

select * from Salesman


select top 100 * from tbldomainvn
where domainId in (5424881)

select * from tbldomainvn
where domainname='1kho.vn'

exec [spDomainBooked_GetAll]

exec [dbo].[spDomainBooked_GetAllByFilter] '',0,0,2,'',0

select * from tblDomainNotBelongVN
where DomainName='nibe.tv'