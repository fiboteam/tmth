﻿using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DAO.AdoNet.SqlServer
{
	public class SqlAlertClientDao:SqlDaoBase<AlertClient>
	{
		public SqlAlertClientDao()
        {
			TableName = "tblAlertClient";
            EntityIDName = "ID";
			StoreProcedurePrefix = "spAlertClient_";
        }

		public SqlAlertClientDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public List<AlertClient> GetWithFilter(long pagesize, long pagenum, string clientno, short status)
		{
			try
			{
				string sql = StoreProcedurePrefix + "v2_GetWithFilter";
				object[] parms = { "@pagesize", pagesize, "@pagenum", pagenum, "@clientno", clientno, "@status", status };
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}

		public override bool Insert(AlertClient ob)
		{
			string sql = StoreProcedurePrefix + "v2_AddUpdate";
			object[] parms = { "@id", ob.ID, "@clientNo", ob.ExtenstionProperties["ClientNo"].ToString(), "@minvalue", ob.MinValue, "@maxvalue", ob.MaxValue, "@listphone", ob.ListPhoneNumber, "@status",1, "@servicetypeid", ob.ServiceTypeID };
			var id = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			if (id > 0)
				return true;
			else
				return false;
		}
		public override bool Update(AlertClient ob)
		{
			string sql = StoreProcedurePrefix + "v2_AddUpdate";
			object[] parms = { "@id", ob.ID, "@clientNo", ob.ExtenstionProperties["ClientNo"].ToString(), "@minvalue", ob.MinValue, "@maxvalue", ob.MaxValue, "@listphone", ob.ListPhoneNumber, "@status",1, "@servicetypeid", ob.ServiceTypeID };
			var id = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			if (id > 0)
				return true;
			else
				return false;
		}

		public override AlertClient GetSingle(long entityID)
		{
			try
			{
				string sql = StoreProcedurePrefix + "v2_GetSingle";
				object[] parms = { "@id", entityID};
				return DbAdapter1.Read(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}


		public bool CheckExistAlert(string clientno, int servicetype)
		{
			string sql = StoreProcedurePrefix + "v2_CheckExistAlert";
			object[] parms = { "@clientno", clientno, "@servicetype", servicetype };
			var id = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			if (id > 0)
				return true;
			else
				return false;
		}
	}
}
