using DAO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.AdoNet.SqlServer
{
    public class SqlServerDaoFactory : IDaoFactory
    {
		public IFileDownload FileDownloadDao
		{
			get { return new SqlFileDownloadDao(); }
		}

		public IDomainNotBelongVN DomainNotBelongVNDao
		{
			get { return new SqlDomainNotBelongVNDao(); }
		}

		public IDomainBooked DomainBookedDao
		{
			get { return new SqlDomainBookedDao(); }
		}
	}
}