using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace DAO.AdoNet.SqlServer
{
    public class SqlDomainNotBelongVNDao : SqlDaoBase<DomainNotBelongVN>, IDomainNotBelongVN
    {
        public SqlDomainNotBelongVNDao()
        {
            TableName = "tblDomainNotBelongVN";
            EntityIDName = "DomainNotBelongVNId";
            StoreProcedurePrefix = "spDomainNotBelongVN_";
        }
        public SqlDomainNotBelongVNDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public IList<DomainNotBelongVN> GetList(long pageSize, long pageNum, string domainName,string origin,string domainType,int shortCreatedDate,bool forSale,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetList";
				object[] parms = { "@pagesize", pageSize, "@pagenum", pageNum, "@domainname",domainName,"@origin",origin,"@domaintype",domainType,"@shortcreateddate",shortCreatedDate,"@forsale",forSale,"@note",note};
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
            catch (Exception)
            {
                return null;
            }
        }

        public int GetTotalPage(long pageSize, string domainName,string origin,string domainType,int shortCreatedDate,bool forSale,string note)
        {
			try
			{
				string sql = StoreProcedurePrefix + "GetTotalPage";
				object[] parms = { "@pagesize", pageSize, "@domainname",domainName,"@origin",origin,"@domaintype",domainType,"@shortcreateddate",shortCreatedDate,"@forsale",forSale,"@note",note };
				return DbAdapter1.GetCount(sql, true, parms);
			}
            catch (Exception)
            {
                return -1;
            }

        }
    }
}
