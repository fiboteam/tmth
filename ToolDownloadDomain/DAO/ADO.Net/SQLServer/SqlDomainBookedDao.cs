using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DAO.AdoNet.SqlServer
{
    public class SqlDomainBookedDao : SqlDaoBase<DomainBooked>, IDomainBooked
    {
        public SqlDomainBookedDao()
        {
            TableName = "tblDomainBooked";
			EntityIDName = "DomainBookedId";
            StoreProcedurePrefix = "spDomainBooked_";
        }
        public SqlDomainBookedDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

		public override bool Insert(DomainBooked businessObject)
		{
			string sql = StoreProcedurePrefix + "Add";
			object[] parms = { "@Domain", businessObject.DomainBookedName, "@DepositPrice", businessObject.DepositPrice, "@Infomation", businessObject.CustomerInfomation, "@SaleId", businessObject.SaleManId, "@DomainBookedStatus", businessObject.DomainBookedStatus, "@ExpireDate", businessObject.ExpireDate, "@CaptureDate", businessObject.CaptureDate };
			businessObject.ID = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			if (businessObject.ID > 0)
				return true;
			else
				return false;
		}

		public List<DomainBooked> GetAllWithFilter(DomainBooked businessObject)
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetAllByFilter";
				object[] parms = { "@domain", businessObject.DomainBookedName, "@shortCaptureDate", businessObject.CaptureDate == null ? 0 : int.Parse(((DateTime)businessObject.CaptureDate).ToString("yyyyMMdd")), "@shortExpireDate", businessObject.ExpireDate == null ? 0 : int.Parse(((DateTime)businessObject.ExpireDate).ToString("yyyyMMdd")), "@SaleId", businessObject.SaleManId, "@Infomation", businessObject.CustomerInfomation, "@Price", businessObject.DepositPrice };
				return DbAdapter1.ReadList(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}

		public override bool Update(DomainBooked businessObject)
		{
			string sql = StoreProcedurePrefix + "Update";
			object[] parms = { "@id", businessObject.ID, "@price", businessObject.DepositPrice, "@info", businessObject.CustomerInfomation};
			businessObject.ID = DbAdapter1.ExcecuteScalar(sql, true, parms).AsLong();

			if (businessObject.ID > 0)
				return true;
			else
				return false;
		}
	}
}
