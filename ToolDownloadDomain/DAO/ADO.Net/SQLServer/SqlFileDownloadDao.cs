﻿using DAO.Interface;
using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace DAO.AdoNet.SqlServer
{
	public class SqlFileDownloadDao : SqlDaoBase<FileDownload>, IFileDownload
	{
		public SqlFileDownloadDao()
		{
			TableName = "tblFileDownload";
			EntityIDName = "FileDownloadId";
			StoreProcedurePrefix = "spFileDownload_";
		}

		public SqlFileDownloadDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }


		public FileDownload GetFileBy(short status,string origin,string type)
		{
			try
			{
				string sql = StoreProcedurePrefix + "GetFileBy";
				object[] parms = { "@name", origin, "@type", type, "@status", status };
				return DbAdapter1.Read(sql, Make, true, parms);
			}
			catch (Exception)
			{
				return null;
			}
		}
		public bool Insert(DTO.Objects.FileDownload businessObject,ref string remark)
		{
			try
			{
				string sql = StoreProcedurePrefix + "AddUpdate";
				var id = DbAdapter1.ExcecuteScalar(sql, true, Take(businessObject)).AsLong();

				if (id > 0)
					return true;
				else
					return false;
			}
			catch(Exception ex)
			{
				remark = ex.Message;
				return false;
			}
		}
	}
}
