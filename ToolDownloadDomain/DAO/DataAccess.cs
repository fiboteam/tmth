using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using DAO.Interface;


namespace DAO
{
    public static class DataAccess
    {
		//private static readonly string connectionStringName = ConfigurationManager.AppSettings.Get("ConnectionStringName");
		private static readonly string connectionStringName = Properties.Settings.Default.ConnectionStringName;
		/*
		 * Không cần ép kiểu nữa
		 * private static readonly IDaoFactory factory = (IDaoFactory)DaoFactories.GetFactory(connectionStringName);
		 */
		//private static readonly IDaoFactory factory = DaoFactories.GetFactory(connectionStringName);
		private static readonly IDaoFactory factory = DaoFactories.GetFactory(connectionStringName);


		public static IFileDownload FileDownload { get { return factory.FileDownloadDao; } }
		public static IDomainNotBelongVN DomainNotBelongVN { get{ return factory.DomainNotBelongVNDao; }  }

		public static IDomainBooked DomainBooked { get { return factory.DomainBookedDao; } }

    }
}