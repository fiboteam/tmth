using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IDomainNotBelongVN : IDao<DomainNotBelongVN>
    {
		IList<DomainNotBelongVN> GetList(long pageSize, long pageNum, string domainName,string origin,string domainType,int shortCreatedDate,bool forSale,string note);
        int GetTotalPage(long pageSize, string domainName,string origin,string domainType,int shortCreatedDate,bool forSale,string note);
    }
}
