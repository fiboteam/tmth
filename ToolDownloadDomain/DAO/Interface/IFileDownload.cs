﻿using DTO.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
	public interface IFileDownload:IDao<FileDownload>
	{
		FileDownload GetFileBy(short status, string origin, string type);
		bool Insert(FileDownload businessObject, ref string remark);
	}
}
