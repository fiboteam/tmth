using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO.Objects;

namespace DAO.Interface
{
    public interface IDomainBooked : IDao<DomainBooked>
    {
		List<DomainBooked> GetAllWithFilter(DomainBooked domain);
	}
}
