using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAO.Interface
{
    public interface IDaoFactory
    {
		IFileDownload FileDownloadDao { get; }
		IDomainNotBelongVN DomainNotBelongVNDao { get; }

		IDomainBooked DomainBookedDao { get; }
    }
}