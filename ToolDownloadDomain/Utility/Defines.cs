﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    public enum FileDownloadStatus:short
	{
		New =1,
		Reading=2,
		Close=3,
		Read_Error=4
	}

	public enum LogDownloadAndCompareType : short
	{
		DownloadDomain=1,
		CompareDomain
	}
	public enum LogDownloadAndCompareStatus : short
	{
		Success = 1,
		Error
	}

	public enum DomainBookedStatus:short
	{
		New=1
	}

	public enum StatusWhois : short
	{
		Registered = 1,
		NotFound = 2,
	}
}
